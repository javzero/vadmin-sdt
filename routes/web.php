<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/vadmin/convertToMysql', 'Site\SiteController@mysql');
Route::get('/vadmin/convertToMysql/{id}', 'Site\SiteController@mysql');

Route::get('/vadmin/langs', 'LangsController@showLangs');
Route::get('/desarrollo', 'Site\SiteController@dev');

// WebSite
Route::get('/', 'Site\SiteController@index');

// Posts / Noticias
Route::get('noticias', 'Site\PostController@index');
Route::get('noticias/{tag}', 'Site\PostController@index');

Route::get('noticia', 'Site\PostController@index');

Route::get('noticias/{search}', 'Site\PostController@index');
Route::get('noticia/{id}', 'Site\PostController@show');

Route::get('/contacto', 'Site\SiteController@contactForm');
Route::get('/contato', 'Site\SiteController@contactForm');

Route::get('/vende-tu-negocio', 'Site\FormsController@mainForm');
Route::get('/vende-tu-negocio/{id}/{customerEmail}', 'Site\FormsController@mainForm');

Route::get('/venda-seu-negocio', 'Site\FormsController@mainForm');
Route::get('/venda-seu-negocio/{id}/{customerEmail}', 'Site\FormsController@mainForm');

Route::get('/cotiza-tu-negocio', 'Site\FormsController@mainForm');
Route::get('/cotiza-tu-negocio/{id}/{customerEmail}', 'Site\FormsController@mainForm');

Route::get('/precifique-seu-negocio', 'Site\FormsController@mainForm');
Route::get('/precifique-seu-negocio/{id}/{customerEmail}', 'Site\FormsController@mainForm');
Route::get('/avalie-valor-do-negocio', 'Site\FormsController@mainForm');

Route::get('/quienes-somos', function () {  return view('site/sections/about-us'); });
Route::get('/quem-somos', function () {  return view('site/sections/about-us'); });

Route::get('/terminos-y-condiciones', function () {  return view('site/sections/terms-and-conditions'); });
Route::get('/preguntas-frecuentes', function () {  return view('site/sections/faq'); });
Route::get('/como-funciona', function () {  return view('site/sections/how-it-works'); });

// Route::get('/catalogo', 'Site\SiteController@catalog');
Route::get('/listado', 'Site\SiteController@list');
Route::get('/lista', 'Site\SiteController@list');
Route::get('/negocio/{id}', 'Site\SiteController@show');

// Main Form
Route::post('submitMainForm', 'Site\FormsController@processMainForm');
Route::post('validateMainFormStepOne', 'Site\FormsController@validateMainFormStepOne');
Route::post('validateMainFormStepTwo', 'Site\FormsController@validateMainFormStepTwo');


Route::get('getCountries', 'Site\SiteController@getCountries');
Route::get('getStates/{id}', 'Site\SiteController@getStates');

Route::post('submitContactForm', 'Site\FormsController@submitContactForm');
// Route::post('validateBusinessFormStepOne', 'Site\FormsController@validateBusinessFormStepOne');
// Route::post('validatebusinessSellFormStepTwo', 'Site\FormsController@validateBusinessSellFormStepTwo');
// Route::post('validatebusinessValueFormStepTwo', 'Site\FormsController@validateBusinessValueFormStepTwo');
Route::get('businessValueTest/{type}', 'Site\FormsController@valuateBusiness');

Route::get('getCategories', 'Site\SiteController@getCategories');
Route::get('getCategoriesByCountry/{country_id}', 'Site\SiteController@getCategoriesByCountry');
Route::get('getSocialNetworks', 'Site\SiteController@getSocialNetworks');
Route::get('getDigitalCapitals', 'Site\SiteController@getDigitalCapitals');


// Auth
Auth::routes();
Route::get('/vadmin/logout', 'Auth\LoginController@logout');

// Vadmin
Route::post('setSetting', 'Core\SettingsController@setSetting');
Route::post('setSettings', 'Core\SettingsController@setSettings');
Route::get('getSettings', 'Core\SettingsController@getSettings');



Route::group(['prefix' => 'vadmin', 'middleware' => 'auth'], function(){
    
    Route::get('/', 'Core\VadminController@dashboard')->name('vadmin');
    Route::get('getAuthUser', 'Core\VadminController@getAuthUser');

    // Users
    Route::put('save_users', 'Core\UserController@save');
    Route::resource('users', 'Core\UserController');
    Route::get('usuarios', ['as' => 'users', 'uses' => 'Core\UserController@index']);
    Route::delete('users/{ids}', 'Core\UserController@destroy');    

    // Menus
    Route::put('save_menus', 'Core\MenuController@save');
    Route::resource('menus', 'Core\MenuController');
    Route::get('buildMenu', 'Core\VadminController@dashboard');
    Route::get('getMenuPermission', 'Core\MenuController@getMenuPermission');
    Route::get('getMenu', 'Core\MenuController@getMenu');

    // Roles
    Route::put('save_roles', 'Core\RoleController@save');
    Route::get('getRoles', 'Core\RoleController@getRoles');
    Route::post('setRoles', 'Core\RoleController@updateRoles');
    Route::post('deleteRoles', 'Core\RoleController@deleteRoles');
    Route::resource('roles', 'Core\RoleController');
    
    // Articles
    Route::resource('articles', 'ArticleController');
    Route::get('articulos', 'ArticleController@index');
    Route::get('crear-articulo', 'ArticleController@create');
    Route::get('editar-articulo/{id}', 'ArticleController@edit');
    Route::get('ver-articulo/{id}', 'ArticleController@show');
    Route::put('saveArticle', 'ArticleController@save');
    Route::get('getArticle/{id}', 'ArticleController@getArticle');
    Route::get('exportar-articulos/{from}/{to}', 'ArticleController@exportArticles');
    Route::post('checkArticlesExistence', 'ArticleController@checkArticlesExistence');
    Route::post('updateArticlePosition/{id}/{position}', 'ArticleController@updatePosition');
    Route::post('updateArticlePosition', 'ArticleController@updatePosition');

    
    Route::post('export-articles', 'ArticleController@exportArticles');


    // Article Images
    Route::resource('images', 'ArticleImageController');

    // Digital Capital
    Route::get('capital-digital', 'DigitalCapitalController@index');
    Route::resource('digitalcapital', 'DigitalCapitalController');
    Route::put('save_digitalcapital', 'DigitalCapitalController@save');

    // Social Networks
    Route::get('redes-sociales', 'SocialNetworkController@index');
    Route::resource('social', 'SocialNetworkController');
    Route::put('save_social', 'SocialNetworkController@save');
    

    // Categories
    Route::put('save_article_categories', 'ArticleCategoriesController@save');
    Route::resource('article_categories', 'ArticleCategoriesController');
    Route::get('categorias', 'ArticleCategoriesController@index');
    Route::get('rubros', 'ArticleCategoriesController@index');
    Route::get('getCategories', 'ArticleCategoriesController@getCategories');
    
    Route::put('save_currency', 'CurrencyController@save');
    Route::resource('currency', 'CurrencyController');
    Route::get('monedas', 'CurrencyController@index');

    // -------------------------------------------------------
    // POST / BLOG
    // -------------------------------------------------------
    // 

    Route::resource('posts', 'Post\PostController');
    Route::get('posts', 'Post\PostController@index');
    Route::get('crear-post', 'Post\PostController@create');

    Route::post('savePost', 'Post\PostController@save');
    Route::get('editar-post/{id}', 'Post\PostController@edit');
    // Route::get('ver-post/{id}', 'PostController@show');
    
    Route::get('tags', 'Post\PostTagController@index');
    Route::put('save_tags', 'Post\PostTagController@save');
    Route::resource('tags', 'Post\PostTagController');

    // -------------------------------------------------------
    // LOCATIONS
    // -------------------------------------------------------

    // Countries
    Route::resource('countries', 'Core\CountryController');
    Route::get('paises', ['as' => 'states', 'uses' => 'Core\CountryController@index']);
    // Route::get('countries/{country}', 'Core\StateController@getStates');
    Route::put('save_countries', 'Core\CountryController@save');
    Route::get('getCountries', 'Core\CountryController@getCountries');
    
    Route::resource('countries', 'Core\CountryController');

    // States
    Route::resource('states', 'Core\StateController');
    Route::get('estados', ['as' => 'states', 'uses' => 'Core\StateController@index']);
    Route::get('getStates/{country}', 'Core\StateController@getStates');
    Route::put('save_states', 'Core\StateController@save');
    
    // Cities
    Route::resource('cities', 'Core\CityController');
    Route::get('ciudades', ['as' => 'cities', 'uses' => 'Core\CityController@index']);
    Route::get('getCities/{state}', 'Core\CityController@getCities');
    Route::put('save_cities', 'Core\CityController@save');

    // -------------------------------------------------------
    // ADS
    // -------------------------------------------------------
    // 
    Route::get('anuncios', 'AdsController@index');    
    Route::get('getCtesad', 'AdsController@getCtesad');   
    Route::post('saveAd', 'AdsController@save');
    // Route::post('changeStatus', 'AdsController@changeStatus');
    Route::post('destroyAd', 'AdsController@destroy');

    // -------------------------------------------------------
    // COMMON
    // -------------------------------------------------------
    // 

    Route::post('toggleActive', 'Core\CommonController@toggleActive');

    // -------------------------------------------------------
    // SETTINGS & UTILITIES
    // -------------------------------------------------------
    // 
    Route::get('configuraciones-generales', ['as' => 'settings', 'uses' => 'Core\SettingsController@index']);
    // Route::get('tareas-de-desarrollo', 'Core');
    // Tools
    Route::get('tools', 'Core\ToolController@ExtractDataFromDB');

    // DevZone
    Route::get('iconos', function () {  return view('vadmin/core/dev-icons'); });
    Route::get('tareas-de-desarrollo', function () {  return view('vadmin/core/dev-tasks'); });
    Route::get('inputs', function () {  return view('vadmin/core/dev-inputs'); });

    // -------------------------------------------------------
    // ALGORITHM
    // -------------------------------------------------------
    // 
    Route::get('config-algoritmo', function() {  return view('vadmin/articles/algorithm_values'); });
    Route::get('getAlgorithmValues', 'AlgorithmController@getValues');
    Route::post('updateAlgorithm', 'AlgorithmController@updateValues');

    // -------------------------------------------------------
    // CONTACTS
    // -------------------------------------------------------
    // 

    Route::resource('contacts', 'ContactController');
    Route::get('exportar-mensajes/{from}/{to}', 'ContactController@exportContacts');
    Route::post('checkContactsExistence', 'ContactController@checkContactsExistence');

    Route::resource('mfcontacts', 'MfContactController');
    Route::get('exportar-registros/{from}/{to}', 'MfContactController@exportRecords');
    Route::post('checkRecordsExistence', 'MfContactController@checkRecordsExistence');
    
    // -------------------------------------------------------
    // TESTS
    // -------------------------------------------------------
    // 
    Route::get('/TestProcessMainFormForClient/{id}', 'Core\TestController@TestProcessMainFormForClient');
    Route::get('/TestProcessMainFormForManager/{id}', 'Core\TestController@TestProcessMainFormForManager');
    
    Route::get('/TestEmailProcessMainForm/{articleId}/{country}', 'Core\TestController@TestEmailProcessMainForm');
    Route::get('/TestEmailContactFormForClient/{country}', 'Core\TestController@TestEmailContactFormForClient');
    Route::get('/TestEmailMFContacts/{customerId}', 'Core\TestController@TestEmailMFContacts');

});