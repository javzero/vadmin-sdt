// I18N Localization
// -------------------------------------
import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

import es from './locales/es'
import br from './locales/br'

const messages = {
    es,
    br
}


let initialLocale = 'es';

if(localStorage.getItem('locale')) {
    initialLocale = localStorage.getItem('locale')
} else {
    // Check origin URL
    if(window.location.origin == 'https://seuproximonegocio.com.br' || window.location.origin == 'https://www.seuproximonegocio.com.br')
        initialLocale = 'br'

    localStorage.setItem('locale', initialLocale)
}

// Check localStorage to see if usar change language manually




export default new VueI18n({
    locale: initialLocale,
    messages
})