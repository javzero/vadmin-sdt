require('./bootstrap')
window.Vue = require('vue')

// --------------------------------------------
// HELPERS
// --------------------------------------------
import customHelpers from '../../vadmin/js/helpers'

const helpers = {
    install() {
        Vue.helpers = customHelpers
        Vue.prototype.$helpers = customHelpers
    }
}

Vue.use(helpers)

// --------------------------------------------
// STORE
// --------------------------------------------
import store from "./store"

// --------------------------------------------
// VENDORS
// --------------------------------------------

// Vuex
import Vuex from 'vuex'
Vue.use(Vuex)

import i18n from "./i18n.js"


// Bootstrap vue (https://bootstrap-vue.js.org/)
// https://bootstrap-vue.org/docs/icons <<< ----- ICON LIST
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)


// Vue toast (https://www.npmjs.com/package/vue-toast-notification)
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css'
import 'vue-toast-notification/dist/theme-sugar.css'
Vue.use(VueToast)
// Vue alerts shortcuts are in helpers.js
// Use example 
// this.$helpers.alertSmall({message: 'Hay errores en el formulario', type: 'error', duration: 999999});

// https://binarcode.github.io/vue-form-wizard/#/
// Vue-form-wizard use themify icons
// import VueFormWizard from 'vue-form-wizard'
// import 'vue-form-wizard/dist/vue-form-wizard.min.css'
// Vue.use(VueFormWizard)


// Vue Font-Awesome ICONS
// https://github.com/FortAwesome/vue-fontawesome#installation
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret)
Vue.component('font-awesome-icon', FontAwesomeIcon)


import components from "./sections"

Vue.config.productionTip = false;

// if (process.env.MIX_ENV_MODE === 'production') {
//     Vue.config.devtools = false;
//     Vue.config.debug = false;
//     Vue.config.silent = true;
// }




const app = new Vue({
    el: '#app',
    i18n,
    components,
    // router,
    store
});
