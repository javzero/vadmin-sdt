import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import i18n from './i18n.js'

Vue.use(Vuex)
Vue.use(i18n)

function getResultsPerPage() {

    if (localStorage.resultsPerPageWeb) {
        return localStorage.resultsPerPageWeb;
    }
    return 10;
}

function buildQuery() {
    let newQuery = '';
    for (const query in state.query) {
        if (state.query[query] != '') {
            newQuery += '&' + query + '=' + state.query[query];
        }
    }
    return newQuery;
}

const state = {
    modelData: [],
    paginationData: [],
    latestArticles: [],
    articles: [],
    countries: [],
    query: {
        orderBy: 'id',
        order: 'asc',
        searchTerm: ''
    },
    resultsPerPageWeb: getResultsPerPage(),
    listProps: {
        errors: [],
        noResults: false
    },
    activeSearch: false,
    loader: false,
    mainLoader: false,
    // Posts
    postProps: {
        errors: [],
        noResults: false
    },
    posts: [],
    postsPaginationData: [],
    i18n: i18n
}

const actions = {
    async getCountries({ commit }) {

        let url = '/getCountries';
        const response = await axios.get(url);
        commit('setCountries', response.data);
    },

    async getArticles({ commit }, { page = '1', query }) {

        state.mainLoader = true;

        let url = '/listado?';

        if (query != '' && query != undefined) {
            // Query from browser url
            url += query;
        } else {
            // Query from search
            //url += buildQuery() + '&page=' + page + '&resultsPerPage=' + state.resultsPerPageWeb;	
            url += buildQuery() + '&page=' + page;
        }

        const response = await axios.get(url);
        localStorage.setItem('previous_query', url);

        try {
            commit('setArticles', response.data.items.data);
            commit('setPagination', response.data.pagination);
        } catch (error) {
            console.log('Error getting data in fetchData()', error);
        }
        finally {
            state.mainLoader = false;
        }
    },


    async setQuery({ commit }, query) {
        commit('setQuery', query);
    },

    setResultsPerPage({ commit }, resultsPerPage) {
        commit('setResultsPerPageWeb', resultsPerPage);
    },


    sendPost({ commit }, data) {
        return new Promise((resolve, reject) => {
            axios.post(data.route, data)
                .then(res => {
                    if (res.data.status == 'success') {
                        this.dispatch('fetchData');
                        resolve(res.data.message);
                    } else {
                        reject(res.data.message);
                    }
                })
                .catch(error => {
                    console.error("Error en setRoles", error);
                })
                .finally(() => { });
        });
    },

    async getPosts({ commit }, { page = '1', query }) {

        console.log(page);

        state.mainLoader = true;

        let url = '/noticias?';

        if (query != '' && query != undefined) {
            // Query from browser url
            url += query;
        } else {
            // Query from search
            url += buildQuery() + '&page=' + page + '&resultsPerPage=' + state.resultsPerPageWeb;
        }

        url += '&lang=' + localStorage.getItem('locale')

        const response = await axios.get(url);

        try {
            commit('setPosts', response.data.items.data);
            commit('setPostsPagination', response.data.pagination);
        } catch (error) {
            console.log('Error getting data in getPosts()', error);
        }
        finally {
            state.mainLoader = false;
        }
    },
}

// GETTERS and SETTERS
// --------------------------------------------------

const getters = {
    searchTerm: state => state.searchTerm,
    latestArticles: state => state.latestArticles,
    articles: state => state.articles,
    countries: state => state.countries,
    items: state => state.modelData,
    pagination: state => state.paginationData,
    postsPagination: state => state.postsPaginationData,
    noResults: state => state.listProps.noResults,
    loader: state => state.loader,
    mainLoader: state => state.mainLoader,
    errors: state => state.listProps.errors,
    resultsPerPageWeb: state => state.resultsPerPageWeb,
    posts: state => state.posts,
    postResults: state => state.postProps.noResults
};


const mutations = {
    setData: (state, modelData) => (state.modelData = modelData),
    setPagination: (state, paginationData) => (state.paginationData = paginationData),
    setLatestArticles: (state, latestArticles) => (state.latestArticles = latestArticles),
    setArticles: (state, articles) => (state.articles = articles),
    setPosts: (state, posts) => (state.posts = posts),
    setPostsPagination: (state, postsPaginationData) => (state.postsPaginationData = postsPaginationData),

    setCountries: (state, countries) => (state.countries = countries),
    setQuery: (state, query) => (state.query = query),
    setOrder(state, query) {
        state.query.orderBy = query;

        if (state.query.order == 'asc')
            state.query.order = 'desc';
        else if (state.query.order == 'desc')
            state.query.order = 'asc';
    },

    setSearchTerm(state, searchQuery) {
        state.query.searchTerm = searchQuery;
    },

    toggleLoader(state, action) {
        state.loader = action;
    },

    toggleMainLoader(state, action) {
        state.mainLoader = action;
    },

    setResultsPerPageWeb(state, resultsPerPage) {
        localStorage.resultsPerPageWeb = resultsPerPage
        state.resultsPerPageWeb = localStorage.resultsPerPageWeb;
    },

};

export default new Vuex.Store({
    state,
    mutations,
    getters,
    actions,
});
