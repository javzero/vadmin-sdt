// --------------------------------------------
// COMPONENTS AND SECTIONS
// --------------------------------------------

// Sections
import Navigation from './components/Navigation'
import Footer from './components/Footer'
import MainLoader from './components/MainLoader'
import Home from './sections/Home'
import Contact from './sections/Contact'
import List from './sections/List'
import ShowArticle from './sections/ShowArticle'
import AboutUs from './sections/AboutUs'
import TermsAndConditions from './sections/TermsAndConditions'
import Faq from './sections/Faq'
import HowItWorks from './sections/HowItWorks'
import Posts from './sections/Posts'
import PostShow from './sections/PostShow'
import MainForm from './sections/MainForm'
import WhatsappCta from './components/WhatsappCta'

const components = {
    'home-section': Home,
    'navigation-component': Navigation,
    'main-loader': MainLoader,
    'footer-component': Footer,
    'contact-section': Contact,
    'list-section': List,
    'show-article-section': ShowArticle,
    'about-us-section': AboutUs,
    'terms-and-conditions-section': TermsAndConditions,
    'faq-section': Faq,
    'how-it-works-section': HowItWorks,
    'posts-section': Posts,
    'post-show-section': PostShow,
    'main-form-section': MainForm,
    'whatsapp-cta-component': WhatsappCta
};

export default components;