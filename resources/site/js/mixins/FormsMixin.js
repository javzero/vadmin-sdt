import { mapGetters, mapActions, mapMutations } from "vuex";
import EasyInputs from '../components/EasyInputs.vue'
import InputExtras from '../components/InputExtras'

export default {
	components: {
		'easy-input': EasyInputs,
		'input-extras': InputExtras
	},
	data() {
		return {
			businessForm: {
				country_id: 'AR',
				state_id: null,
				customer_phone: '',
				category: 0,   
				logistic_id: 0,
				socialNetworkDefault: null,
				social: [],
				digital_capital: [],
			},
			errors: [],
			showForm: true,
			successMessage: false,
			errorMessage: false,
			countries: [],
			states: [],
		};
	},
	created() {
		this.getCountries();
	},
	computed: {
		phonePrefixByCountry() {
			switch (this.businessForm.country_id) {
				case 'AR':
					return '+54';
					break;
				case 'CO':
					return '+57';
					break;
				case 'PE':
					return '+51';
					break; 
				case 'MX':
					return '+52';
					break; 
				case 'CL':
					return '+56';
					break; 
				case 'UY':
					return '+598';
					break; 
				default:
					return '---';
					break;
			}
		}
	},
	methods: {
		...mapMutations(['toggleMainLoader']),

		submitForm() {
			let route = 'submitBusinessForm';
			this.toggleMainLoader(true);

			axios.post(route, this.businessForm)
				.then(res => {
					// console.log(res);
					if(res.data.status == 'success') {
						// this.$helpers.alertSmall({message: res.data.message});
						this.showForm = false;
						this.successMessage = true;

					} else if(res.data.status == 'error') {
						// this.$helpers.alertSmall({type: 'error', message: res.data.message});
						this.showForm = true;
						this.successMessage = false;
						this.errors = res.data.message;
					}
				})
				.catch(error => {
					console.error("Error on submitBusinessForm()");
					console.log(error);
				})
				.finally(() => {
					this.toggleMainLoader(false);
				});
		},

		validateAndSetStepOne() {

			let route = 'validateBusinessFormStepOne';
			let data = this.businessForm;

			this.businessForm.customer_phone = this.phonePrefixByCountry + " (" + this.businessForm.customer_phone_area + ") " + this.businessForm.customer_phone_number;

			return new Promise((resolve, reject) => {

				axios.post(route, data)
					.then(res => {
						if(res.data.status == 'success') {
							resolve(true);
						}
						else {
							this.errors = res.data.message;
							reject('No');
						}
					})
					.catch(error => {
						return false;
						console.error("Error on validateFormStep()");
						console.log(error);
					});
				
			});
		},

		
		getCountries() {
			// console.log("Searching for countries...");
			axios.get('getCountries')
				.then(res => {
					
					this.countries = res.data;
					this.getStates();
				})
				.catch(error => {
					console.error("Error on getCountries()");
					console.log(error);
				});
		},

		getStates(countryId = undefined){
			let country = 1;
			this.businessForm.state_id = null;

			if(countryId) {
				country = countryId;
			} else {
				country = this.businessForm.country_id;
			}

			let route = 'getStates/'+ country;
			axios.get(route)
				.then(res => {
					this.states = res.data;
				})
				.catch(error => {
					console.error("Error on getStates()");
					console.log(error);
				});
		},
		
		clearError(value) {
			this.errors[value] = undefined;
		},
	}
};
