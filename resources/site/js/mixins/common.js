export default {
	data() {
		return {
		};
	},
	created() {
	},
	computed: {
	},
	filters: {
	},
	methods: {
        vuelog(data) {
            console.log(data);
		},
		
		strippedText(text, chars){
			if(text.length > chars) {
				return text.slice(0,chars);
			}
			return text + '...';
		},
	}
};
