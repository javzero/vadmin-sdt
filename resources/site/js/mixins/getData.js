import axios from "axios";

export default {
	data() {
		return {
            countries: []
		};
	},
	methods: {


		async getCountries() 
		{
			axios.get('/getCountries') 
                .then((res) => {
                    this.countries = res.data
                })
                .catch((error) => {
                    console.log(error);
                }).finally(() => {
                });
        
		}
	}
};
