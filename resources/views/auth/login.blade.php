@extends('vadmin.core.layouts.login')

@section('auth-component')
<div class="auth-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-7 col-left">
                <div class="content">
                    <img class="brand-image" src="/images/site/brand/user-main-logo.png"/>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-5 col-right">
                <form class="auth-form" method="POST" action="{{ route('login') }}">
                    <div class="form-header">
                        <div class="isologo"><img src="/images/vadmin/vadmin-isologo.png"></div>
                        <h1>INGRESO AL SISTEMA</h1>
                    </div>
                    <div class="form-content">
                        @csrf
                        <div class="form-group">
                            <label>Email / Usuario</label>
                            <input  type="username" class="form-control @error('username') is-invalid @enderror" 
                                name="username" value="{{ old('username') }}" required  autofocus placeholder="Ingrese nombre de usuario">
                            @if($errors->any())
                                <h4>{{$errors->first()}}</h4>
                            @endif

                            
                            
                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $error }}</strong>
                            </span>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" 
                            name="password" required autocomplete="current-password"  placeholder="Ingrese nombre contraseña">
                            
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        
                        @if (session('error'))
                            <div class="alert alert-success">
                                {{ session('error') }}
                            </div>
                        @endif

                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember"> Recordarme </label>
                            </div>
                        </div>  
                        <div class="form-group">
                            <button type="submit" class="btn btn-block submit-btn">Ingresar</button>
                            
                            <a class="btn text-btn btn-block register-btn" href="{{ route('register') }}">
                                <div class="register-text">No tengo cuenta</div>
                                <span class="text">Registrarme</span>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>                   
    </div>
    
@endsection