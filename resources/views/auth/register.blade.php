@extends('vadmin.core.layouts.login')

@section('auth-component')
<div class="auth-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-7 col-left">
                <div class="content">
                    <img class="brand-image" src="/images/site/brand/user-main-logo.png"/>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-5 col-right">
                <form class="auth-form" method="POST" action="{{ route('register') }}">
                    <div class="form-header">
                        <div class="isologo"><img src="/images/vadmin/vadmin-isologo.png"></div>
                        <h1>Registro de Usuari@</h1>
                    </div>
                    <div class="form-content">
                        @csrf

                        <div class="form-group">
                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="Nombre de usuario">

                            @error('username')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail">

                            @error('email')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="first_name" type="first_name" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" placeholder="Nombre">

                            @error('first_name')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="last_name" type="last_name" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" placeholder="Apellido">

                            @error('last_name')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Contraseña">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Repetir Contraseña">
                        </div>

                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-main btn-block submit-btn">Registrarme</button>
                            <a class="btn text-btn btn-block register-btn" href="{{ route('login') }}">
                                <span class="text">Ya tengo cuenta</span>
                                <div class="register-text">Ir al login</div>
                            </a>
 
                        </div>
                    </div>                    
                </form>
            </div>
        </div>                   
    </div>
    
</div>
@endsection
