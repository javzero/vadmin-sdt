@extends('vadmin.core.layouts.main')

@section('content')
    
    <post-create-section :tags="{{ json_encode($tags) }}" ></post-create-section>
  
@endsection
