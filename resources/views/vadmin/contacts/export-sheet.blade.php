@extends('vadmin.core.layouts.sheet')

@section('title', 'Mensajes recibidos')

@section('table-titles')
    <th>Nombre</th>
    <th>Teléfono</th>
    <th>E-Mail</th>
    <th>Mensaje</th>
    <th>Cód. de negocio consultado</th>
    <th>País</th>
    <th>Fecha</th>

@endsection

@section('table-content')
    @foreach($items as $item)
        <tr>
            <td>{{ $item->name }}</td>
            <td>{{ $item->phone }}</td>
            <td>{{ $item->email }}</td>
            <td>{{ $item->message }}</td>
            <td>{{ $item->article_id }}</td>
            @if($item->country)
            <td>{{ $item->country->name }}</td>
            @else
            <td></td>
            @endif
            <td>
                @php
                    $date = date_create($item->created_at);
                @endphp
                {{ date_format($date, 'm-d-Y H:i') }}
            </td>
        </tr>
    @endforeach
@endsection