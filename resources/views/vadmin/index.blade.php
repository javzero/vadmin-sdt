@extends('vadmin.core.layouts.main')

@section('content')

	<div class="container-fluid">	
		<div class="row">
			<div class="col-12">
			
				@if(Session::has('message'))
					<b-alert show dismissible variant="success">
						{{ Session::get('message') }} <b>&rArr;</b>
					</b-alert>
					{{-- <div class="alert alert-success"> 
						{{ Session::get('message') }}
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
					</div>  --}}
				@endif

				@if(Session::has('error'))
					<b-alert show dismissible variant="danger">
						{{ Session::get('error') }}
					</b-alert>
				@endif
			</div> 
		</div> 
	</div> 
	
	<dashboard-section></dashboard-section>

@endsection
