@extends('vadmin.core.layouts.sheet')

@section('title', 'Mensajes recibidos')

@section('table-titles')
    <th>Nombre y Apellido</th>
    <th>Teléfono</th>
    <th>E-Mail</th>
    <th>País</th>
    <th>Fecha</th>

@endsection

@section('table-content')
    @foreach($items as $item)
        <tr>
            <td>{{ $item->customer_firstname }}  {{ $item->customer_lastname }}</td>
            <td>({{ $item->customer_phone_area }}) {{ $item->customer_phone_number }}</td>
            <td>{{ $item->customer_email }}</td>
            @if($item->country)
            <td>{{ $item->country->name }}</td>
            @else
            <td></td>
            @endif
            <td>
                @php
                    $date = date_create($item->created_at);
                @endphp
                {{ date_format($date, 'm-d-Y H:i') }}
            </td>
        </tr>
    @endforeach
@endsection