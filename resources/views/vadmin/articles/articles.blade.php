@extends('vadmin.core.layouts.main')

@section('content')
   <articles-section :countries="{{ $countries }}"></articles-section>
@endsection
