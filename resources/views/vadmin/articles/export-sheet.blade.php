@extends('vadmin.core.layouts.sheet')

@section('title', 'Items del Catálogo')

@section('table-titles')
    <th>Cód.</th>
    <th>Fecha</th>
    <th>Nombre y Apellido</th>
    <th>Teléfono</th>
    <th>Email</th>
    
    <th>País</th>
    <th>Provincia / Estado</th>
    <th>Ciudad</th>

    <th>Rubro</th>
    <th>Nombre del proyecto | Titulo</th>
    <th>Nombre de dominio</th>

    <th>Visitas por mes</th>
    <th>% Orgánico</th>
    <th>Total de seguidores</th>
    <th>Total de suscriptores</th>

    <th>Tráfico por público femenino</th>
    <th>Tráfico por público masculino</th>
    <th>Horas trabajadas</th>
    <th>Antigüedad</th>
    <th>Ingreso neto actual Moneda local</th>
    <th>Ingreso neto anterior Moneda local</th>

    <th>Valor del negocio Usd</th>
    <th>Valor del negocio Moneda local</th>
    <th>Valor manual del negocio</th>

    <th>Logistica</th>
    <th>Capital Digital</th>
    <th>Estado</th>
@endsection

@section('table-content')
    @foreach($items as $item)
        {{-- {{ dd($item) }} --}}
        
        <tr>
            <td class="w-50">{{ $item->id}} </td>
            <td>{{ $item->created_at->format('d/m/Y') }}</td>
            <td class="max-text">{{ $item->customer_firstname }}, {{ $item->customer_lastname }}</td>
            <td>{{ $item->customer_phone }}</td>
            <td>{{ $item->customer_email }}</td>
            
            <td>@if($item->country) 
                {{ $item->country->name }} 
                @else 
                - 
                @endif
            </td>
            <td>@if($item->state) 
                {{ $item->state->name }} 
                @else 
                - 
                @endif
            </td>
            <td>@if($item->city) 
                {{ $item->city->name }} 
                @else 
                - 
                @endif
            </td>

            <td>@if($item->category_id) {{ $item->category->name }} @else - @endif</td>
            <td>{{ $item->name }} @if($item->title) | {{ $item->title}} @endif<br></td>
            <td>{{ $item->domain_name }}</td>

            <td> {{ $item->views_per_month }} </td>
            <td> {{ $item->organic_views_per_month }} </td>
            <td> {{ $item->total_followers }} </td>
            <td> {{ $item->total_subscriptors }} </td>            
            
            <td> {{ $item->female_traffic }} </td>
            <td> {{ $item->male_traffic }} </td>
            <td> {{ $item->working_hours }} </td>
            <td> {{ $item->business_age }} </td>
            <td> {{ $item->net_income_actual_year }} </td>
            <td> {{ $item->net_income_last_year }} </td>

            <td> 
                @if($item->business_value_manual > 0)
                    {{ $item->currencyLocalToUss($item->country_id, $item->business_value_manual) }}
                @else
                    {{ $item->business_value }} 
                @endif
            </td>

            <td> {{ $item->business_value_local }} </td>
            <td> $ {{ $item->business_value_manual }} | u$S {{ $item->currencyLocalToUss($item->country_id, $item->business_value_manual) }}</td>
            <td> 
                @if($item->logistic) 
                    {{ $item->logistic->name }}
                @endif
            </td>
            <td> 
                @foreach($item->digitalCapital as $dc)
                    @if($dc)
                        {{ $dc->name }} 
                    @endif
                    @if(!$loop->last) - @endif
                @endforeach
            </td>
            <td> 
         
                @switch($item->status)
                    @case('0')
                        Disponible 
                        @break
                    @case('1')
                        Reservado
                        @break
                    @case('2')
                        Vendido
                        @break
                    @case(null)
                        Disponible *
                        @break
                    @default
                        return '-'
                @endswitch
                | @if($item->active == 1) Activo @else Pausado @endif

            </td>
                    
        </tr>
    @endforeach
@endsection