@extends('vadmin.core.layouts.main')

@section('content')
    
    <articles-create-section 
        :socialnetworks="{{ json_encode($socialNetworks) }}" 
        :logistics="{{ $logistics }}" 
        :digitalcapitals="{{ $digitalCapitals }}"
        :categories="{{ $categories }}"
        :business_types="{{ $businessTypes }}"
        :showmode="{{ $showMode ?? 0 }}">
    </articles-create-section>
  
@endsection
