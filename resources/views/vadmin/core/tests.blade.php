@extends('vadmin.core.layouts.main')

@section('content')
    <div class="table-responsive">
        <table class="table" style="width:100%" >

            <tr>
                <th>Cód.</th>
                <th>Nombre y Apellido</th>
                <th>Teléfono</th>
                <th>Email</th>
                
                <th>País</th>
                <th>Provincia / Estado</th>
                <th>Ciudad</th>

                <th>Rubro</th>
                <th>Nombre del proyecto | Titulo</th>
                <th>Nombre de dominio</th>

                <th>Vistas x mes</th>
                <th>Vistas orgánicas x mes</th>
                <th>Total de seguidores</th>
                <th>Total de suscriptores</th>
                <th>Suscriptores mensuales</th>

                <th>Tráfico Fem.</th>
                <th>Tráfico Masc.</th>
                <th>Horas trabajadas</th>
                <th>Antigüedad</th>
                <th>Fc Año actual</th>
                <th>Fc Año anterior</th>
                <th>Ingreso neto año act.</th>
                <th>Ingreso neto año ant.</th>

                <th>Valor del negocio</th>
                <th>Valor del negocio (Mon. loc.)</th>
                <th>Valor manual del negocio</th>

                <th>Logistica</th>
                <th>Capital Digital</th>
                <th>Estado</th>

            </tr>
            @foreach($items as $item)


                <tr>
                    <td class="w-50">{{ $item->id}} </td>
                    <td class="max-text">{{ $item->customer_firstname }}, {{ $item->customer_lastname }}</td>
                    <td>{{ $item->customer_phone }}</td>
                    <td>{{ $item->customer_email }}</td>
                    
                    <td>@if($item->country) 
                        {{ $item->country->name }} 
                        @else 
                        - 
                        @endif
                    </td>
                    <td>@if($item->state) 
                        {{ $item->state->name }} 
                        @else 
                        - 
                        @endif
                    </td>
                    <td>@if($item->city) 
                        {{ $item->city->name }} 
                        @else 
                        - 
                        @endif
                    </td>

                    <td>@if($item->category_id) {{ $item->category->name }} @else - @endif</td>
                    <td>{{ $item->domain_name }}</td>
                    <td>{{ $item->name }} | {{ $item->title}}<br></td>
	
			        <td> {{ $item->views_per_month }} </td>
			        <td> {{ $item->organic_views_per_month }} </td>
			        <td> {{ $item->total_followers }} </td>
			        <td> {{ $item->total_subscriptors }} </td>
			        <td> {{ $item->montly_subscriptors }} </td>
			        
                    
			        <td> {{ $item->female_traffic }} </td>
                    <td> {{ $item->male_traffic }} </td>
                    <td> {{ $item->working_hours }} </td>
                    <td> {{ $item->business_age }} </td>
                    <td> {{ $item->fc_actual_year }} </td>
                    <td> {{ $item->fc_last_year }} </td>
                    <td> {{ $item->net_income_actual_year }} </td>
                    <td> {{ $item->net_income_last_year }} </td>
            
                    <td> {{ $item->business_value }} </td>
                    <td> {{ $item->business_value_local }} </td>
                    <td> {{ $item->business_value_manual }} </td>
                    <td> 
                        @if($item->logistic) 
                            {{ $item->logistic->name }}
                        @endif
                    </td>
                    <td> 
                        @foreach($item->digitalCapital as $dc)
                            @if($dc)
                                {{ $dc->name }}
                            @endif
                        @endforeach
                    </td>
                    <td> 
                        @switch($item->status)
                            @case('1')
                                Reservado
                                @break
                            @case('2')
                                Vendido
                                @break
                        
                            @default
                        @endswitch

                    </td>

                    
                            
                </tr>
            @endforeach
        </table>
    </div>



  
@endsection
