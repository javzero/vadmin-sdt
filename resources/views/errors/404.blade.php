@extends('vadmin.core.layouts.full')

@section('title', 'La página no existe | 404')

@section('content')

    <div class="container-fluid main-gradient-background full-screen-section">
        <div class="row">
            <div class="full-centered justify-content-center error-page text-center">
                {{-- <img src="{{ asset('images/vadmin/logo.png') }}" alt="" style="max-width: 200px"> --}}
                <hr>
                <h1>Lo sentimos</h1>
                <h2>La página solicitada no existe</h2>
                <p>(Error 404)</p>
                <hr>
                <a href="{{ url('/') }}"> Volver al Inicio</a>
            </div>
        </div>
    </div>
   
  
@endsection
