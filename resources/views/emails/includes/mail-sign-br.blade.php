@component('emails.components.mail-sign')
    @slot('imageSrc') {{ ENV('APP_URL') }}/images/site/thais-pic.png @endslot
    @slot('signText') 
        <b>Thais Louzada</b> <br>
        Customer Engagemente <br>
        Marketplace para a venda de negócios <br>
        Digitais na América latina <br>
    @endslot
    @slot('brandSrc') {{ ENV('APP_URL') }}/images/site/brand-logo-br.png @endslot    
@endcomponent
