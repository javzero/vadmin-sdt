@component('emails.components.mail-sign')
@slot('imageSrc') {{ ENV('APP_URL') }}/images/site/andrea-bertone-pic.png @endslot
@slot('signText') 
    <b>Andrea Bertone</b> <br>
    Founder & CEO <br>
    1er plataforma de compra-venta de <br>
    negocios online <br>
@endslot
@slot('linkedinHref') https://www.linkedin.com/in/andrea-bertone-47155715 @endslot
@slot('brandSrc') {{ ENV('APP_URL') }}/images/site/brand-logo.png @endslot
@endcomponent