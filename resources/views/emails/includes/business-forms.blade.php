@if($data->lang == 'br')

<h2>Recebemos um pedido de orçamento</h2>
{{-- Portuguese --}}
<p>
    <b>Nome:</b> {{ $data->customer_firstname }} {{ $data->customer_lastname }} <br>
    <b>Telefon:</b> {{ $data->customer_phone }} <br>
    <b>Email:</b> {{ $data->customer_email }} <br>
    <b>Localização</b> 
    @if($data->country) {{ $data->country->name }} @if($data->state) - {{ $data->state->name }} @endif @endif
</p>
<br>

<b>Domínio:</b> {{ $data->domain_name }}  <br>
<b>Idade empresarial: </b>{{ $data->business_age }} años <br>
<b>Horas trabalhadas por dia: </b>{{ $data->working_hours }} hs. <br>
<b>Lucro líquido do ano atual: </b>${{ formatMoney($data->net_income_actual_year) }} <br>
<b>Lucro líquido do período anterior: </b>${{ formatMoney($data->net_income_last_year) }} <br>
<b>Visualizações por mês: </b>{{ $data->views_per_month }} <br>
<b>Porcentagem de visualizações orgânicas: </b>%{{ $data->organic_views_per_month }} <br>
<b>Total de seguidores: </b>{{ $data->total_followers }} <br>
<b>Total de assinantes: </b>{{ $data->total_subscriptors }} <br> <br>

<h3>Avaliação de negócios: <b> ${{ formatMoney($data->business_value_local) }} (u$s{{ formatMoney($data->business_value) }}) </b></h3>

@else

<h2>Hemos recibido un pedido de cotización</h2> 
{{-- Spanish --}}
<p>
    <b>Nombre:</b> {{ $data->customer_firstname }} {{ $data->customer_lastname }} <br>
    <b>Teléfono:</b> {{ $data->customer_phone }} <br>
    <b>Email:</b> {{ $data->customer_email }} <br>
    <b>Ubicación:</b> 
    @if($data->country) {{ $data->country->name }} @if($data->state) - {{ $data->state->name }} @endif @endif
</p>
<br>

<b>Dominio:</b> {{ $data->domain_name }}  <br>
<b>Antigüedad del negocio: </b>{{ $data->business_age }} años <br>
<b>Horas trabajadas por día: </b>{{ $data->working_hours }} hs. <br>
<b>Ganancia neta año actual: </b>${{ formatMoney($data->net_income_actual_year) }} <br>
<b>Ganancia neta período anterior: </b>${{ formatMoney($data->net_income_last_year) }} <br>
<b>Vistas por mes: </b>{{ $data->views_per_month }} <br>
<b>Porcentaje orgánico de vistas: </b>%{{ $data->organic_views_per_month }} <br>
<b>Total de seguidores: </b>{{ $data->total_followers }} <br>
<b>Total de suscriptores: </b>{{ $data->total_subscriptors }} <br> <br>

<h3>Valuación del negocio: <b> ${{ formatMoney($data->business_value_local) }} (u$s{{ formatMoney($data->business_value) }}) </b></h3>
@endif