@component('mail::message')

@if($data->country_id == 'BR')

{{-- Portuguese --}}
Olá <b>{{ $data->customer_firstname }}</b>, percebemos que você começou a preencher seus dados para cotar o seu negócio
online, mas não finalizou todo o processo.

Queremos garantir que todas as informações que são compartilhadas conosco são confidenciais e são protegidas com toda segurança.
Caso haja alguma informação que você não se lembre apenas digite 0 para continuar.

A plataforma está programada para enviar um e-mail imediatamente com o valor da venda
sugerido assim que você enviar o formulário final.
Isso não significa que você precise anunciar a venda do seu do seu negócio. O objetivo desta ferramenta avaliar um valor aproximado
do seu negócio.

@if($data->id) 
Compartilhamos este link com você para que possa  <a href="{{ Request::root() }}/venda-seu-negocio/{{ $data->id }}/{{ $data->customer_email }}"><b>continuar seu upload</b></a>
@endif

Aproveitamos para convidar você a ler um dos nossos <a href="{{ Request::root() }}/noticias?c=1&searchByTag=BLOG"><b>blog</b></a> que com certeza será de
lucro, sobre por que é importante não adiar a venda do seu negócio. Esperamos que todas essas informações sejam valiosas para você.

Estamos a sua disposição,

<div style="height: 10px"></div>
@include('emails.includes.mail-sign-br')

@else

{{-- Spanish --}}
Hola <b>{{ $data->customer_firstname }}</b>, hemos notado que iniciaste la carga de tus datos para valuar tu negocio online pero no has terminado con ella.

Queremos darte la tranquilidad, que toda la información que nos compartas es confidencial y se encuentra resguardada de manera segura. También contarte que si al completar el formulario no recuerdas algún dato puedes completar con 0 y continuar.
La plataforma está programada para enviarte un mail de inmediato con el valor de venta sugerido una vez que envíes el formulario final.
Esto no te obliga de ningún manera a vender tu negocio, sino que simplemente orienta hacia el posible valor de venta.

@if($data->id) 
Te compartimos este enlace para que puedas <a href="{{ Request::root() }}/cotiza-tu-negocio/{{ $data->id }}/{{ $data->customer_email }}"><b>continuar tu carga!</b></a>
@endif

Aprovechamos la oportunidad para invitarte a leer un <a href="{{ Request::root() }}/noticias?c=1&searchByTag=BLOG"><b>blog</b></a> que seguramente te serán de utilidad, acerca de por qué es importante no postergar la venta de tu negocio.


Esperamos que toda esta información te sea de valor
¡Seguimos en contacto!


<br>
@include('emails.includes.mail-sign-ar')

@endif
@endcomponent