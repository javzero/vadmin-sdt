@component('mail::message')

{{-- Portuguese --}}
@if($data['lang'] == 'br')


Olá <b>{{ $data['name'] }}</b>, <br>
Bem vindo ao primeiro Marketplace da América Latina de compra e venda de negócios digitais.

Recebemos o seu interesse pelo negócio digital e teremos prazer em agendar uma conversa para responder todas as suas dúvidas e perguntas.
Segue <a href="https://meetings.hubspot.com/seuproximonegocio/contato">O LINK</a> para selecionar o dia e horário que for mais conveniente para você.

Estou à disposição.

Atenciosamente,

@include('emails.includes.mail-sign-br')

@else 

{{-- Spanish --}}
Hola <b>{{ $data['name'] }}</b>, <br>
¿Cómo estas? ¡Espero que muy bien!

Recibimos tu interés por el negocio digital y con gusto podemos evacuar todas tus dudas acerca del mismo.

Te dejo <a href="https://meetings.hubspot.com/maximiliano-sadaba-agueero">ESTE ENLACE</a> por si quieres marcar una llamada, allí podrás elegir el mejor día y horario para que conversemos.

Seguimos en contacto!
<br>

@include('emails.includes.mail-sign-ar')

@endif

@endcomponent


