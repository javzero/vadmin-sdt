@component('mail::message')

@if($data->lang == 'br')

{{-- Portuguese --}}

<h1>Olá  <b>{{ $data->customer_firstname }}</b>. <br>
Obrigado por cotar o seu negócio digital no nosso site.</h1>

<p>
    Com base nas informações compartilhadas, o valor estimado do seu negócio se encontra entre
    <b> R${{ formatMoney(($data->business_value_local - calcPercent($data->business_value_local, 25 ))) }} e 
    R${{ formatMoney($data->business_value_local) }}  </b>
    (Entre u$s {{ formatMoney(($data->business_value - calcPercent($data->business_value, 25 ))) }} e 
    u$s {{ formatMoney($data->business_value) }}). <br>
    <br>
    O total pode sofrer pequenas variações dependendo de aspectos qualitativos que não foram considerados na cotação.
    Caso tenha interesse em vender, agende uma chamada conosco usando  
    <a href="https://meetings.hubspot.com/seuproximonegocio/contato">ESTE LINK </a> para confirmarmos os detalhes.
    <div style="height: 10px"></div>
    Logo após a chamada, subiremos seu negócio em nosso Marketplace para oferece-lo para potenciais compradores.<br><br>
    Atenciosamente, <br>
    Equipe Seu Próximo Negócio 
</p>

<div style="height: 10px"></div>
@include('emails.includes.mail-sign-br')

@else

{{-- Spanish --}}
<h1>Hola  <b>{{ $data->customer_firstname }}</b>. <br>
Gracias por cotizar tu negocio en Sitio de Tiendas.</h1>

<p>
En base a los datos ingresados, el valor estimado del mismo se encuentra entre
<b> ${{ formatMoney(($data->business_value_local - calcPercent($data->business_value_local, 25 ))) }} y 
${{ formatMoney($data->business_value_local) }}  </b>
(De u$s {{ formatMoney(($data->business_value - calcPercent($data->business_value, 25 ))) }} a 
u$s {{ formatMoney($data->business_value) }}). <br>
<br>
El monto puede sufrir pequeñas variaciones dependiendo de aspectos cualitativos que no han sido considerados en esta cotización.
<br><br>
Si te interesa realizar la venta, agenda una llamada con nosotros a partir de 
@if(formatMoney($data->business_value <= 100000))
<a href="https://meetings.hubspot.com/hector-valerga">ESTE ENLACE</a>,
@else
<a href="https://meetings.hubspot.com/maximiliano-sadaba-agueero">ESTE ENLACE</a>,
@endif  

<div style="height: 10px"></div>
Luego de esa conversación, con gusto subiremos tu negocio a nuestro Marketplace para ofrecerlo públicamente.
</p>

<br>
@include('emails.includes.mail-sign-ar')

@endif
@endcomponent