@component('mail::message')

{{-- Portuguese --}}
@if($data['lang'] == 'br')

<p><b>Mensagem de contato direto</b></p>
<p>
@if(isset($data['articleTitle']))
    <b>Artigo: <b> {{ $data['articleTitle'] }}<br>
    Cod: {{ $data['articleCode'] }}
    <br><br>
@endif
<b>Nome: </b> {{ $data['name'] }}<br>
<b>Email: </b> {{ $data['email'] }}<br>
<b>Telefone: </b> {{ $data['phone'] }}<br>
<b>País: </b> {{ $data['countryName'] }}<br>
<b>Mensagem: </b> {{ $data['message'] }}
</p> 

@else 

{{-- Spanish --}}
<p><b>Mensaje de contacto directo</b></p>
<p>
@if(isset($data['articleTitle']))
    <b>Artículo: <b> {{ $data['articleTitle'] }}<br>
    Cod: {{ $data['articleCode'] }}
    <br><br>
@endif
<b>Nombre: </b> {{ $data['name'] }}<br>
<b>Email: </b> {{ $data['email'] }}<br>
<b>Teléfono: </b> {{ $data['phone'] }}<br>
<b>País: </b> {{ $data['countryName'] }}<br>
<b>Mensaje: </b> {{ $data['message'] }}
</p> 

@endif

@endcomponent