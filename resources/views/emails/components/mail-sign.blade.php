{{-- <table>
    <tr>
        <td style="padding: 10px">
            <img style="max-width: 100%; width: 200px" src="{{ $imageSrc }}"  alt=""/>
        </td>
        <td>
            
            {{ $signText }}

            @if(isset($linkedinHref))
            <a href="{{ $linkedinHref }}" target="_blank"> 
                <img src="http://vadmin-sdt.test/images/site/linkedin-logo.png" style="max-width: 100%; width: 100px" alt=""> 
            </a>
            @endif

        </td>
    </tr>
</table>
<br>
<img  src="https://sitiodetiendas.com/images/site/brand-logo.png"  alt=""> --}}

<table>
    <tr>
        <td style="padding: 10px" width="120px" ><img  style="max-width: 100%; width: 120px" src="{{ $imageSrc }}" alt="Sitio de Tiendas"></td>
        <td >
            <p>
                {{ $signText }}
                @if(isset($linkedinHref))
                <a href="{{ $linkedinHref }}" target="_blank" > 
                    <img src="{{ ENV('APP_URL') }}/images/site/linkedin-logo-small.png" style="max-width: 100%; width: 70px; margin-top: 5px" alt="Linkedin"> 
                </a>
                @endif
            </p>
        </td>
    </tr>
</table>
<br>
<img src="{{ $brandSrc }}"  alt="Sitio de Tiendas">


{{-- <table>
    <tr>
        <td style="padding: 10px"><img style="max-width: 100%; width: 200px" src="{{ ENV('APP_URL') }}/images/site/andrea-bertone-pic.png"  alt="Sitio de Tiendas"></td>
        <td >
            <p>
                <b>Andrea Bertone</b> <br>
                Founder & CEO <br>
                1er plataforma de compra-venta de <br>
                negocios online <br>
                <a href="https://www.linkedin.com/in/andrea-bertone-47155715" target="_blank"> 
                    <img src="http://vadmin-sdt.test/images/site/linkedin-logo.png" style="max-width: 100%; width: 100px" alt="Linkedin"> 
                </a>
            </p>
        </td>
    </tr>
</table>
<br>
<img  src="https://sitiodetiendas.com/images/site/brand-logo.png"  alt="Sitio de Tiendas"> --}}