@extends('vadmin.core.layouts.full')

@section('content')


{{-- Portuguese --}}
@if($data['lang'] == 'br')


Olá <b>{{ $data['name'] }}</b>, <br>
Bem vindo ao primeiro Marketplace da América Latina de compra e venda de negócios digitais.

Recebemos o seu interesse pelo negócio digital e teremos prazer em agendar uma conversa para responder todas as suas dúvidas e perguntas.
Segue <a href="https://meetings.hubspot.com/seuproximonegocio/contato">O LINK</a> para selecionar o dia e horário que for mais conveniente para você.

Estou à disposição.

Atenciosamente,
<br>
<img src="https://sitiodetiendas.com/images/site/brand-logo-br.png"  alt="Seu Proximo Negocio">

@else 

{{-- Spanish --}}
Hola <b>{{ $data['name'] }}</b>, <br>
¿Cómo estas? ¡Espero que muy bien!

Recibimos tu interés por el negocio digital y con gusto podemos evacuar todas tus dudas acerca del mismo.

Te dejo <a href="https://meetings.hubspot.com/maximiliano-sadaba-agueero">ESTE ENLACE</a> por si quieres marcar una llamada, allí podrás elegir el mejor día y horario para que conversemos.

Seguimos en contacto!
<br>
<div class="container">
    <div class="row">
        <div class="col-3">
            <img src="{{ ENV('APP_URL') }}/images/site/andrea-bertone-pic.png"  alt="Sitio de Tiendas">
        </div>
        <div class="col-9">
            <p>
                <b>Andrea Bertone</b> <br>
                Founder & CEO <br>
                1er plataforma de compra-venta de <br>
                negocios online
                <img src="http://vadmin-sdt.test/images/site/linkedin-logo.png"  alt="Sitio de Tiendas">
            </p>
        </div>
    </div>
</div>


<img src="https://sitiodetiendas.com/images/site/brand-logo.png"  alt="Sitio de Tiendas">

@endif


@endsection
