<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		{{-- Section Title --}}
		<title>@yield('title', 'Sitio de Tiendas')</title>
		
		<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="shortcut icon" type="image/png" href="{{ asset('images/vadmin/favicon.png') }}">
		<!-- Icons -->
		{{-- <link href="{{ asset('vendor/font-awesome/css/all.min.css')}}" rel="stylesheet"> --}}
		<!-- Styles -->
		<link href="{{ asset('css/site/site.css') }}" rel="stylesheet">
		@yield('styles')
		{!! $settings->google_analitycs !!}
 		{!! $settings->google_tag_manager !!}
	</head>
	<body>
        <div class="row full-centered-container">
			<div class="text-center">
				<h1>Próximamente...</h1>
				<img class="mb-2" src="{{ asset('images/site/brand-logo.png') }}" alt="">
				<p>Compartinos tu consulta <br>
				<a href="mailto:info@sitiodetiendas.com">info@sitiodetiendas.com</a></p>
					{{-- {{ env('SITE_DOWN_MESSAGE') }}  --}}
			</div>
        </div>
	</body>
</html>