@extends('site.layouts.main')

@section('content')
    <posts-section :tags="{{ $tags }}" ></posts-section>
@endsection