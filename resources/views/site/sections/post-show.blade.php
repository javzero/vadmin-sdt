@extends('site.layouts.main')

@section('content')
    <post-show-section :item="{{ $item }}"></post-show-section>
@endsection