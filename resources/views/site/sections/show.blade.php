@extends('site.layouts.main')

@section('content')
    <show-article-section :item="{{ $item }}"></show-article-section>
@endsection