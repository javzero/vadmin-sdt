@extends('site.layouts.main')

@section('content')
    {{-- {{ dd($article != null) }}  --}}
    @if($article)
        <contact-section :article="{{ json_encode($article) }}"></contact-section>
    @else
        <contact-section></contact-section>
    @endif
@endsection