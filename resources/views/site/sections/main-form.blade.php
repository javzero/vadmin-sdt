@extends('site.layouts.main')

@section('content')

    <main-form-section 
        :countries="{{ $countries }}" 
        :business_types="{{ $businessTypes }}"
        :digital_capitals="{{ $digitalCapital }}"
        :social_networks="{{ $socialNetwork }}"
        :categories="{{ $categories }}"
        :customer_data=" {{ $customerData }}">
    </main-form-section>

@endsection