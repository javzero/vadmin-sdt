{{-- Section Title --}}
@if(request()->getHost() == 'seuproximonegocio.com.br')
    <title>@yield('title', $settings->site_name_br)</title>
    <meta name="description" content="{{ $settings->site_description_br }}">
    <meta name="keywords" content="{{ $settings->site_keywords_br }}">
    <meta name="lang" content="BR Lang">
@else
    <title>@yield('title', $settings->site_name)</title>
    <meta name="description" content="{{ $settings->site_description }}">
    <meta name="keywords" content="{{ $settings->site_keywords }}">
    <meta name="lang" content="ES Lang">
@endif



@switch(request()->getHost())
        @case('vadmin-sdt.test')
                <link rel='alternate' hreflang='es-AR' href='dev.sitiodetiendas.com'/>
                @break
        @case('sitiodetiendas.com')
                <link rel='alternate' hreflang='es-AR' href='sitiodetiendas.com'/>
                @break
        @case('sitiodetiendas.com.ar')
                <link rel='alternate' hreflang='es-AR' href='sitiodetiendas.com.ar'/>
                @break
        @case('www.seuproximonegocio.com.br')
                <link rel='alternate' hreflang='pt-BR' href='seuproximonegocio.com.br'/>
                @break
        @default
@endswitch
