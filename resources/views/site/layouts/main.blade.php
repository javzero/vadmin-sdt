<!doctype html>
@if(request()->getHost() == 'seuproximonegocio.com.br')
<html lang="pt-BR">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endif
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		@include('site.components.domain')

		<meta name="author" content="Leandro Andrade | Vimana Studio | https://vimanastudio.com.ar">

		<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/vadmin/favicon.png') }}">
		<link rel="shortcut icon" type="image/png" href="{{ asset('images/vadmin/favicon.png') }}">
		<!-- Icons -->
		{{-- <link href="{{ asset('vendor/font-awesome/css/all.min.css')}}" rel="stylesheet"> --}}
		<!-- Styles -->
		<link href="{{ asset('css/site/site.css') }}" rel="stylesheet">
		{{-- Analytics --}}
		@yield('styles')

		{!! $settings->google_analitycs !!}
 		{!! $settings->google_tag_manager !!}
	</head>
	<body>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P3F5NFW"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

        <div id="app">
			<main-loader></main-loader>
			<navigation-component></navigation-component>
            @yield('content')
			<footer-component></footer-component>
			{{-- Whats App Cta--}}
			<whatsapp-cta-component></whatsapp-cta-component>
		</div>

		

		<script src="{{ asset('js/site/site.js') }}"></script>   
		@yield('scripts') 
		<script>
			
/* 			(function(){

				const currentUrl = window.location.host;

				console.log(currentUrl);
				if(currentUrl == 'vadmin-sdt.test') {

					HtmlLink link = new HtmlLink();
              
					link.Attributes.Add("rel", "alternate");
					link.Attributes.Add("href", "https://sitiodetiendas.com");
					link.Attributes.Add("hreflang", "ES");
			
					Page.Header.Controls.Add(link);

				}
					

			})(); */



		</script>	

	</body>

</html>
