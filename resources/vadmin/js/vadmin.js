require('./bootstrap')
window.Vue = require('vue')

// --------------------------------------------
// STORE
// --------------------------------------------
import store from "./store/vadminStore"

// --------------------------------------------
// HELPERS
// --------------------------------------------
import customHelpers from './helpers'

const helpers = {
    install () {
        Vue.helpers = customHelpers
        Vue.prototype.$helpers = customHelpers
    }
}

Vue.use(helpers)

// --------------------------------------------
// VENDORS
// --------------------------------------------

// Vuex
import Vuex from 'vuex'
Vue.use(Vuex)

// Bootstrap vue (https://bootstrap-vue.js.org/)
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

// Sweet alert (https://sweetalert2.github.io/)
import VueSweetalert2 from 'vue-sweetalert2'
Vue.use(VueSweetalert2)

// Vue toast (https://www.npmjs.com/package/vue-toast-notification)
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css'
import 'vue-toast-notification/dist/theme-sugar.css'
Vue.use(VueToast)

import components from "./sections"

Vue.config.productionTip = false;

// if (process.env.MIX_ENV_MODE === 'production') {
//     Vue.config.devtools = false;
//     Vue.config.debug = false;
//     Vue.config.silent = true;
// }

const app = new Vue({
    el: '#app',
    components,
    // router,
    store
});
