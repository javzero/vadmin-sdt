import Vue from "vue";
import axios from "axios";

// Vue Router
import VueRouter from "vue-router";
Vue.use(VueRouter);

Vue.component("vadmin-main", require("../sections/VadminMain").default);
import Dashboard from "../sections/DashboardSection";
import Users from "../sections/UsersSection";
import UserProfile from "../sections/UserProfileSection.vue";
import ArticleCategories from "../sections/ArticleCategoriesSection.vue";
import Articles from "../sections/ArticlesSection.vue";
import ArticlesCreate from "../sections/ArticlesCreateSection.vue";
import Ads from "../sections/AdsSection.vue";
// import Categories from "../sections/ArticleCategories.vue";
import Provs from "../sections/ProvsSection.vue";
import Locs from "../sections/LocsSection.vue";
import Roles from "../sections/RolesSection.vue";
import Permissions from "../sections/PermissionsSection.vue";
import Settings from "../sections/SettingsSection.vue";
import DevUtilities from "../sections/DevUtilitiesSection.vue";
import DevTasks from "../sections/DevTasksSection.vue";
import Menu from "../sections/Core/MenuSection.vue";

let allRoutes = [
  {
	path: "/vadmin",
	name: "dashboard",
	component: Dashboard,
	allowed: false
  },
  {
	path: "/vadmin/usuarios",
	name: "users",
	component: Users,
	allowed: false
  },
  {
	path: "/vadmin/artículos",
	name: "articles",
	component: Articles,
	allowed: false
  },
  {
    path: "/vadmin/perfil-de-usuario",
    name: "user_profile",
	component: UserProfile,
	allowed: false
  },
  {
    path: "/vadmin/categorias",
    name: "articles_categories",
	component: ArticleCategories,
	allowed: false
  },
  {
    path: "/vadmin/articulos",
    name: "articles",
	component: Articles,
	allowed: false
  },
  {
    path: "/vadmin/crear-articulo",
    name: "articles_create",
	component: ArticlesCreate,
	allowed: false
  },
  {
    path: "/vadmin/editar-articulo/:id",
    name: "article_edit",
	component: ArticlesCreate,
	allowed: false
  },
  {
    path: "/vadmin/categorias-de-articulos",
    name: "articleCategories",
	component: ArticleCategories,
	allowed: false
  },
  {
    path: "/vadmin/roles",
    name: "roles",
	component: Roles,
	allowed: false
  },
  {
    path: "/vadmin/permisos",
    name: "permissions",
	component: Permissions,
	allowed: false
  },
  {
    path: "/vadmin/anuncios",
    name: "ads",
	component: Ads,
	allowed: false
  },
  {
    path: "/vadmin/localidades",
    name: "locs",
	component: Locs,
	allowed: false
  },
  {
    path: "/vadmin/provincias",
    name: "provs",
	component: Provs,
	allowed: false
  },
  {
    path: "/vadmin/configuraciones",
    name: "settings",
	component: Settings,
	allowed: false
  },
  {
    path: "/vadmin/menu",
    name: "menus",
	component: Menu,
	allowed: false
  },
  {
    path: "/vadmin/utilidades",
    name: "dev_utilities",
	component: DevUtilities,
	allowed: false
  },
  {
    path: "/vadmin/tareas-de-desarollo",
    name: "dev_tasks",
	component: DevTasks,
	allowed: false
  }
];

let finalRoutes = [];

// Get a set of allowed routes from backend
function getAllowedRoutes() {

	axios.get('/vadmin/getMenuPermission')
	.then((res) => {
		let allowedRoutes = res.data;
		
		allRoutes.forEach(route => {
			allowedRoutes.forEach(allowed => {
				if(route.name == allowed.name) {
					route.allowed = true;
				}
			});
		});
	
		finalRoutes = allRoutes.filter(function (el) {
			return el.allowed;
		});
		
		console.log(finalRoutes);
		return finalRoutes;


	})
	.catch((error) => {
	  console.error('Error on getAllowedRoutes()');
	  console.error(error);
	}).finally(() => {
	
		// console.log(allowedRoutes);
	
	});
}
	



// Set to true the allowed routes for vue router.
// function setAllowedRoutes(allowedRoutes) {	
// 	allRoutes.forEach(route => {
// 		allowedRoutes.forEach(allowed => {
// 			if(route.name == allowed.name) {
// 				route.allowed = true;
// 			}
// 		});
// 	});

// 	finalRoutes = allRoutes.filter(function (el) {
// 		return el.allowed;
// 	});
// 	console.log("Retornando", finalRoutes);
// 	return finalRoutes;

// }

console.log(getAllowedRoutes());

const router = new VueRouter({
	mode: "history",
	linkExactActiveClass: "active",
	routes: getAllowedRoutes()
});

export default router;
