// --------------------------------------------
// COMPONENTS AND SECTIONS
// --------------------------------------------

// Components
import Header from "./components/HeaderComponent"
import Sidebar from "./components/SidebarComponent"
import Loader from "./components/LoaderComponent"

// Sections
import Dashboard from "./sections/Dashboard"
import Users from "./sections/Core/Users"
import Menu from "./sections/Core/MenuSection.vue"
import Roles from "./sections/Core/RolesSection.vue"
import Articles from "./sections/Article/ArticlesSection.vue"
import ArticlesCreate from "./sections/Article/ArticlesCreateSection.vue"
import ArticleCategories from "./sections/Article/ArticleCategories.vue"
import DigitalCapital from "./sections/DigitalCapital.vue"
import Settings from "./sections/SettingsSection.vue"
import Ads from "./sections/AdsSection.vue"
import UserProfile from "./sections/UserProfileSection.vue"
import Countries from "./sections/Core/Countries.vue"
import States from "./sections/Core/States.vue"
import Cities from "./sections/Core/Cities.vue"
import SocialNetworks from "./sections/Article/SocialNetworks"
import Currency from "./sections/Currency"
import Posts from "./sections/Post/Posts"
import PostTags from "./sections/Post/PostTags"
import PostCreate from "./sections/Post/PostCreateSection"
import AlgorithmValues from "./sections/Article/AlgorithmValues"
import Langs from "./sections/Core/Langs"
import Contacts from "./sections/Contacts"
import MfContacts from "./sections/MfContacts.vue"

const components = {
    'header-component': Header,
    'sidebar-component': Sidebar,
    'loader-component': Loader,
    'dashboard-section': Dashboard,
    'users-section': Users,
    'menu-section': Menu,
    'roles-section': Roles,
    'articles-section': Articles,
    'articles-create-section': ArticlesCreate,
    'article-categories-section': ArticleCategories,
    'social-networks-section': SocialNetworks,
    'digital-capital-section': DigitalCapital,
    'settings-section': Settings,
    'user-profile-section': UserProfile,
    'ads-section': Ads,
    'countries-section': Countries,
    'states-section': States,
    'cities-section': Cities,
    'currency-section': Currency,
    'posts-section': Posts,
    'post-create-section': PostCreate,
    'post-tags-section': PostTags,
    'algorithm-values-section': AlgorithmValues,
    'langs-section': Langs,
    'contacts-section': Contacts,
    'mfcontacts-section': MfContacts
};

export default components;