import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

// On component
// import { mapMutations, mapActions, mapState, mapGetters } from "vuex";

// SIDEBAR
// -----------------------------------------------

function getSidebarState() {
	
	if(localStorage.sidebarState != null){
		if(localStorage.sidebarState == '0' ) {
			return false
		}
	}

	return true

};

function getResultsPerPage() {

	if(localStorage.resultsPerPage) {
		return localStorage.resultsPerPage;
	}

	return 10;

}



// CONSTANTS
// ------------------------------------------------

const state = {
	sidebarState: getSidebarState(),
	modelData: [],
	paginationData: [],
	currencyData: [],
	query: {
		orderBy: 'id', 
		order: 'asc',
		searchTerm: '',
		country: ''
	},
	resultsPerPage: getResultsPerPage(),
	activeSearch: false,
	loader: false,
	skeletonList: false,
	sectionRoute: '',
	listProps: {
		updateMode: false,
		errors: [],
		batchDeleteMode: false,
		allChecked: false,
		noResults: false
	},
	authUser: {
		username: '', first_name: '', last_name: '', email: '', created_at: '', status: ''
	},
	authUserRoles: [],
	menu: {},
	allRoles: []
};

const actions = {


	async getMenu({ commit }) {
		const response = await axios.get('/vadmin/getMenu');

		try {
			commit('setMenu', response.data);
		} catch (error) {
			console.log('Error getting data in getMenu()');
			console.log(error);
		}
	},

	async getAuthUser({ commit }) {

		const response = await axios.get('/vadmin/getAuthUser');
		
		try {
			commit('setAuthUserData', response.data);
			commit('setAuthUserRoles', response.data.roles);
		} catch (error) {
			console.log('Error getting data in getAuthUser()');
			console.log(error);
		}
	},

	// Sidebar
	// ----------------------------------
	sidebarToggle({ commit }, state) {

		if(state == false){ 
			var d = document.getElementById("MainWrapper");
			d.className = "main-wrapper";
			localStorage.sidebarState = 1;
		}
		else {
			var d = document.getElementById("MainWrapper");
			d.className = "main-wrapper main-wrapper-full";
			localStorage.sidebarState = 0;
		}

		commit("setSidebar", !state);
	},

	// Get Model Data
	async fetchData({ commit }, rules ) {
		
		

		state.loader = true;
		if(state.modelData == 0)
			state.skeletonList = true;
		
		let page = 1

		if(rules)
		{
			if(rules.resultsPerPage != undefined)
				this.setResultsPerPage(resultsPerPage);
			
			if(rules.page != undefined)
				page = rules.page;
		}

		let url = '/vadmin/' + state.sectionRoute + '?page=' + page + '&orderBy=' + state.query.orderBy + '&order=' + state.query.order + "&resultsPerPage=" + state.resultsPerPage;

		// console.log(url);
		if(state.query.searchTerm != '' && state.query.searchTerm != undefined)
		{
			url += '&search=' + state.query.searchTerm;
		}
		const response = await axios.get(url);
		// console.log(response.data);
		try {
			commit('setData', response.data.items.data);		
			commit('setPagination', response.data.pagination);

			if(response.data.currency)
				commit('setCurrencyData', response.data.currency);

		} catch (error) {
			// commit('setListProperty', { property: errors, value: response.data.message });
			console.log('Error getting data in fetchData()');
			console.log(error);
			console.log("Route: " + state.sectionRoute);
			console.log("Page: " + page);
			console.log("Url: " + url);
		}
		finally {
			state.loader = false;
			state.skeletonList = false;
		}
	},
	
	setResultsPerPage( { commit }, resultsPerPage) {
		commit('setResultsPerPage', resultsPerPage);
		// console.log("SET results" + resultsPerPage);

	},

	getRoles( { commit } ){
		axios.get('/vadmin/getRoles')
			.then(res => {
				commit('setRoles', res.data);
				// this.allRoles = res.data;
			})
			.catch(error => {
				console.error("Error on getProvs()");
				console.log(error);
			});
	},

	// updateRoles( { commit }, data) {
		
	// 	axios.post("/vadmin/setRoles", data)
	// 		.then(res => {
	// 			if(res.data.status == 'success') {
	// 				this.dispatch('fetchData');
	// 				resolve(response); 
	// 			}
	// 			// $('#Error').html(res.data);
	// 		})
	// 		.catch(error => {
	// 			console.error("Error en setRoles", error);
	// 		})
	// 		.finally(() => {  });

	// },

	sendPost( { commit }, data) {
        return new Promise((resolve, reject) => {
			axios.post(data.route, data)
					.then(res => {
						if(res.data.status == 'success') {
							this.dispatch('fetchData');
							resolve(res.data.message); 
						} else {
							reject(res.data.message);
						}
					})
					.catch(error => {
						console.error("Error en setRoles", error);
					})
					.finally(() => {  });
        });
    },

	deleteRoles( { commit }, data) {
		console.log(data);
		// axios.post("/vadmin/deleteRoles", data)
		// 	.then(res => {
		// 		if(res.data.status == 'success') {
		// 			this.dispatch('fetchData');
		// 		}
		// 	})
		// 	.catch(error => {
		// 		console.error("Error en deleteRoles", error);
		// 	})
		// 	.finally(() => {});
	},
};


// GETTERS and SETTERS
// --------------------------------------------------

const getters = {
	sidebarState: state => state.sidebarState,
	searchTerm: state => state.searchTerm,
	items: state => state.modelData,
	pagination: state => state.paginationData,
	loader: state => state.loader,
	skeletonList: state => state.skeletonList,
	route: state => state.sectionRoute,
	updateMode: state => state.listProps.updateMode,
	errors: state => state.listProps.errors,
	noResults: state => state.listProps.noResults,
	allChecked: state => state.listProps.allChecked,
	batchDeleteMode: state => state.listProps.batchDeleteMode,
	resultsPerPage: state => state.resultsPerPage,
	authUser: state => state.authUser,
	authUserRoles: state => state.authUserRoles,
	menu: state => state.menu,
	allRoles: state => state.allRoles,
	currencyData: state => state.currencyData
};


const mutations = {
	setData: (state, modelData) => (state.modelData = modelData),
	setPagination: (state, paginationData) => (state.paginationData = paginationData),
	setCurrencyData: (state, currencyData) => (state.currencyData = currencyData),
	setSidebar: (state, sidebarState) => (state.sidebarState = sidebarState),
	setOrder(state, query) {
		state.query.orderBy = query;

		if(state.query.order == 'asc')
			state.query.order = 'desc';
		else if(state.query.order == 'desc')
			state.query.order = 'asc';
	},
	
	// setSearchTerm(state, searchQuery) {
	// 	state.query.searchTerm = searchQuery;
	// },

	setSearchTerm(state, searchQuery) {
		state.query.searchTerm = searchQuery;
	},
	
	setAuthUserData(state, data) {
		state.authUser = data;
	},

	setAuthUserRoles(state, data) {
		state.authUserRoles = data;
	},

	setMenu(state, data) {
		state.menu = data;
	},

	setRoute(state, route){
		state.sectionRoute = route;
	}, 

	toggleLoader(state, action)
	{
		state.loader = action;
	},

	setListProp(state, data) {
		state.listProps[data.property] = data.value;
	},

	setResultsPerPage(state, resultsPerPage) {
		localStorage.resultsPerPage = resultsPerPage 
		state.resultsPerPage = localStorage.resultsPerPage;
	},

	setRoles: (state, roles) => (state.allRoles = roles),
	
	updateRoles: (roles) => {

	},

};

export default new Vuex.Store({
	state,
	mutations,
	getters,
	actions,
	modules: {}
});
