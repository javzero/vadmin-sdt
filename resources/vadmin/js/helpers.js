export default { 

    bIcon(icon) {
        // console.log(icon);
        return '/vendor/bootstrap-icons/' + icon + '.svg';
    }, 

    iconImg(icon) {
        return "<img src='/vendor/bootstrap-icons/" + icon + ".svg'>";
    },

    siteAsset(assetFilename) {
        return '/images/site/' + assetFilename;
    },

    sitePostAssets(filename) {
        if(filename)
            return '/images/site/posts/' + filename;
        return '';
    },

    alertSmall({message: message = 'Ok', 
                type: type = 'success', 
                position: position = 'top-right', 
                duration: duration = 3000,
                dismissible: dismissible = true}) {
                
        Vue.$toast.open({
            message: message,
            type: type,
            position: position,
            dismissible: dismissible,
            pauseOnHover: true,
            duration: duration
        });
    },

    alertSmall({message: message = 'Ok', 
                type: type = 'success', 
                position: position = 'top-right', 
                duration: duration = 3000,
                dismissible: dismissible = true}) {
                
        Vue.$toast.open({
            message: message,
            type: type,
            position: position,
            dismissible: dismissible,
            pauseOnHover: true,
            duration: duration
        });
    },

    // CURRENCY
    // --------------

    formatMoney(num) {
        if(num) {
            return (
                num
                  .toFixed(2) // always two decimal digits
                  .replace('.', ',') // replace decimal point character with ,
                  .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                  .replace(/(\.|,)00$/g, '')
            ) // use . as a separator
        }
    
    },


    currencyToText(country, value) {
        if(value == '-')
            return '-'

        switch (country) {
            case 'Argentina':
                return '$ ' + value + ' AR';
            case 'Chile':
                return '$ ' + value + ' CLP';
            case 'Colombia':
                return '$ ' + value + ' COP';
            case 'Mexico':
            case 'México':
            case 'Mejico':
                return '$ ' + value + ' MEX';
            case 'Perú':
            case 'Peru':
                return '$ ' + value + ' PEN';
            case 'Uruguay':
                return '$ ' + value + ' UYU';
            case 'Brasil':
                return 'R$ ' + value;
            default:
                return '';
        }
    },


    currencyCode(country) {

        switch (country) {
            case 'Argentina':
                return 'AR';
            case 'Chile':
                return 'CLP';
            case 'Colombia':
                return 'COP';
            case 'Mexico':
            case 'México':
            case 'Mejico':
                return 'MEX';
            case 'Perú':
            case 'Peru':
                return 'PEN';
            case 'Uruguay':
                return 'UYU';
            case 'Brasil':
                return '';
            default:
                return '';
        }
    },

    //------------------
    // DATES
    //------------------

   formatDate(date, withHours = false) {

        let current_datetime = new Date(date);
        console.log();
        let formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear();
        console.log(current_datetime.getHours())
        if(withHours) {
            formatted_date += " (" + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ")";
        }

        console.log(formatted_date);
        return formatted_date;
   },


    //------------------
    
    formatInteger(num) {
        if(num)
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    },

    toInteger(num) {
        if(num)
            return this.formatInteger(Math.ceil(num));
    },
    
    twoDecimals(value) {
        if(value != undefined) {
            return Math.round(value * 100) / 100;
        }
    },

    test() {
        console.log("HOLA");
    },

    pixel(fbAction) {
        try {
            fbq('track', fbAction);
        } catch(e) {
            console.warn(e);
        }
    },

    convertMysqlDate(mysqlDate) {
        
        let onlyDate = mysqlDate.split('T')[0];
        let dateArray = onlyDate.split('-');
        return dateArray[2]+'-'+dateArray[1]+'-'+dateArray[0];

    },

    objectIsEmpty(object) {
        return Object.keys(object).length === 0;
    },

    objectLenght(object) {
        return Object.keys(object).length;
    },

    convertLocalToUssCurrency(localValue, country_id, currencyData) {
        
        let finalData = 0;
        let currencyValue = 0;

        currencyData.filter(function(elem){
            if(elem.country_id == country_id) {
                if(elem.api_value > 0) { 
                    currencyValue = elem.api_value;
                } else {
                    currencyValue = elem.manual_value;
                }
            }
            finalData = (localValue / currencyValue);
        });

        return this.twoDecimals(finalData);
    },

    setLocalstorageParam(data){
        localStorage[data[0]] = data[1];
    },

    limitedString(string, lenght) {
        if(string)
            return (string.length > lenght) ? string.substr(0, lenght-1) + ' ...' : string;
        else 
            return ''
    },

    clearLocalStorage(item) {
        localStorage.setItem(item, '')
    },

    showCategory(text, locale) {
        console.log( text, locale );

    }


};