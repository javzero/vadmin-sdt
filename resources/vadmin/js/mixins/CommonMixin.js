import { mapGetters, mapActions } from "vuex";

export default {
	data() {
		return {
			itemForRoles: [],

			infoModalTitle: '',
			infoModalContent: '',
		};
	},
	computed: {
		// ...mapGetters(['loader']),
		...mapGetters(['items']),
	},
	filters: {
		translateStatus(status) {
			switch (status) {
				case 'A':
					return 'Activo';
					break;
				case 'I':
					return 'Inactivo';
					break;
				default:
					return 'Sin estado';
					break;
			}
		},

		imageSrcFilter(image) {

			if(image != undefined)
				return '/images/articles/' + image.name;
			
			return '/images/articles/default.jpg';
		},	

		getStatusBoolean(status){
			switch (status) {
				case 'A':
					return true;
					break;
				case 'I':
					return false;
					break;
				default:
					return false;
					break;
			}
		},
		switchStatus(status) {
			switch (status) {
				case 'A':
					return 'I';
					break;
				case 'I':
					return 'A';
					break;
				default:
					return 'I';
					break;
			}
		}
	},
	methods: {
		...mapActions(['fetchData']),
		
		bIcon(icon) {
			return '/vendor/bootstrap-icons/' + icon + '.svg';
		}, 

		// Forms
		// --------------------------
		toggleActive(model, id, active) {

			console.log(model, id, active);
			
			// if(switchInactiveActive)
			// 	status = this.$options.filters.switchStatus(status);
			axios
				.post("/vadmin/toggleActive", {
					model: model,
					id: id,
					active: active
				})
				.then(res => {
					// console.log(res.data.message);
					this.alertSmall({ message: "Estado actualizado" });
				})
				.catch(error => {
					console.error("Error");
					console.error(error);
				})
				.finally(() => {});
		},

		submitForm(route, data, message = "Formulario enviado") {
			// if(this.loader != undefined) this.loader = true;

			axios.post(route, data)
				.then(res => {
					// console.log(res.data);

					if (res.data.status == "success") 
						this.alertSuccess(res.data.message);
					else 
					{
						// console.log("ERROR")
						console.log(res.data);
					}
				})
				.catch(error => {
					console.error("Error: " + error);
					console.log(error);
				})
				.finally(() => {
					// if(this.loader != undefined) this.loader = false;
				});
		},

		submitFormWithImages(route, data, message, action) {
			axios
				.post("route", data, {
					headers: {
						"Content-Type": "multipart/form-data"
					}
				})
				.then(function() {
					console.log("SUCCESS!!");
				})
				.catch(function() {
					console.log("FAILURE!!");
				});
		},

		setItemRoles(data) {
			this.itemForRoles = data;
		},
	
		
		// UI 

		// Toggle UI elements
		uiToggle(value) { 
			if(this.uiItems[value] == false) 
				this.uiItems[value] = true;
			else
				this.uiItems[value] = false;
		},

		// App Settings
		// --------------------------

		setSetting(setting, value) {
			// let data = { setting: setting, value: value };
			axios
				.post("/setSetting", {
					setting: setting,
					value: value
				})
				.then(res => {
					console.log(res.data);
					// $('#Error').html(res.data);
				})
				.catch(error => {
					console.error("Error: " + error);
					console.log(error);
				})
				.finally(() => {});
		},

		// Modal
		// --------------------------

		modal(id, action) {
			$(id).modal(action);
		},

		infoModal(title, content) {
			this.infoModalTitle = title;
			this.infoModalContent = content;
			
			this.$refs.InfoModal.openModal();
		},

		// Date Helpers
		// --------------------------

		notBeforeToday(date) {
		
			let todayDate = new Date();
			todayDate.setHours(0, 0, 0, 0);
			
			return date < todayDate;
		
		},


		// Helpers
		// --------------------------

		getUrlParamFromLast(index = 1) {
			
			const url = window.location.href.split("/");
			return url[url.length - index].replace('#', '');

		},

		getParameterFromUrlString(route) {
			const url = route.split("/");
			return url[url.length - 1].replace('#', '');
		},

	

		// Logs
		// --------------------------

		vuelog(e) {
			console.log(e);
		},

		// Alerts
		// --------------------------

		alertSuccess(message, message2 = '', message3 = '') {
			let fullMessage = message;
			
			if(message3 != '' && message3 != undefined) {
				fullMessage + "<br> <span style='font-size: .8rem'>" + message3 + "</span>";
			}

			this.$swal({
				icon: "success",
				// title: 'Oops...',
				text: fullMessage,
				footer: "<span style='text-align:center'>" + message2 + "</span>"
			});

		},

		alertError(message, message2 = '') {

			this.$swal({
				icon: "error",
				title: "Ups...",
				text: message,
				footer: message2
			});
		},

		alertConfirm(message, message2 = '', callback) {
			this.$swal({
				title: message,
				text: message2,
				icon: "warning",
				showCancelButton: true,
				cancelButtonText: "Cancelar",
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Sí, eliminar!"
			}).then(confirmed => {
				callback(confirmed && confirmed.value == true);
			});
		},

		alertConfirm2(message, message2 = '', buttonCancel = 'Cancelar', buttonAccept = 'Aceptar', action, callback) {
			this.$swal({
				title: message,
				text: message2,
				icon: "warning",
				showCancelButton: true,
				cancelButtonText: buttonCancel,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: buttonAccept
			}).then(confirmed => {
				callback(confirmed && confirmed.value == true);
			});
		},

		alertSmall({message: message, type: type = 'success', position: position = 'bottom-left', time: time = 5000} ) {
			Vue.$toast.open({
				message: message,
				type: type,
				position: position,
				dismissible: true,
				pauseOnHover: true,
				duration: time
			});
		}

		// Common Queries
		
	}
};
