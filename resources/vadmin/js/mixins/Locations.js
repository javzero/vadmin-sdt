export default {
	data() {
		return {
            countries: [],
            states: [],
            cities: [],
        }
    },
    created() {
        if(this.useCountries)
            this.getCountries();
        // if(this.useStates)
        //     this.getStates();
    },
    methods: {
        getCountries() {

            axios.get('/vadmin/getCountries')
                .then(res => {
                    this.countries = res.data;
                })
                .catch(error => {
                    console.error("Error on getCountries()");
                    console.log(error);
                });
        },
        // getCountry(id = '0') {
        //     console.log("getting countries");
        //     // Clear previous search
        //     this.state = [];
        //     this.cities = [];

        //     axios.get('/vadmin/getCountries/' + id)
        //         .then(res => {
        //             this.countries = res.data;
        //         })
        //         .catch(error => {
        //             console.error("Error on getCountries()");
        //             console.log(error);
        //         });
        // },

        getStates( countryId) {
            axios.get('/vadmin/getStates/' + countryId)
                .then(res => {
                    this.states = res.data;
                    // if(getCityById > 0) {
                    //     this.getCities(getCityById);
                    // }
                })
                .catch(error => {
                    console.error("Error on getStates()");
                    console.log(error);
                });
        },

        getCities( stateId = '0') {
            axios.get('/vadmin/getCities/' + stateId)
                .then(res => {
                    this.cities = res.data;
                })
                .catch(error => {
                    console.error("Error on getCities()");
                    console.log(error);
                });
        },

        // getCitiesFromState(id){
        //     if(id != undefined) {
        //         // this.loader = true;
        //         let url = '/vadmin/cities?whereState='+id;
    
        //         axios.get(url)
        //             .then((res) => {
        //                 this.citiesFromState = res.data;
        //             })
        //             .catch((error) => {
        //                 console.error('Error en getCities');
        //                 console.error(error)
        //             }).finally(() => {
        //                 // this.loader = false;
        //             });
        //     }

        // },

        clearCityAndState() {
            this.states = [];
            this.cities = [];
            delete this.item.state_id;
            delete this.item.city_id;
        },
    }
}
