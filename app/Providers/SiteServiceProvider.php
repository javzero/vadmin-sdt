<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class SiteServiceProvider extends ServiceProvider
{
    public function boot(View $view)
    {
        View::composer(['site.*'], 'App\Http\ViewComposes\SiteComposer');
    }
}
