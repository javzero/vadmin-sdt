<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Traits\CommonTrait;
use Carbon\Carbon;


class AppServiceProvider extends ServiceProvider
{
	use CommonTrait;
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
	  // CoreModification
	  // $this->loadMigrationsFrom(__DIR__.'/database/migrations/Core');
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
	  Schema::defaultStringLength(191);
	  Carbon::setLocale('es');

	  view()->composer('*', function ($view) {

		  $view->with('menu', $this->buildMenu());

	  });
	}


}
