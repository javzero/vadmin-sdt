<?php

namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ArticleExport implements FromView
{
    public function __construct($data) 
    {
        $this->data = $data;
    }
    // return \App\Models\Article::all();
    public function view(): View
    {
        // dd($this->data);
        return view('vadmin.articles.export-sheet', [
            'items' => $this->data
        ]);
    }
}
