<?php

namespace App\Exports;

use App\Invoice;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ContactExport implements FromView
{
    public function __construct($data) 
    {
        $this->data = $data;
    }
    
    public function view(): View
    {
        return view('vadmin.contacts.export-sheet', [
            'items' => $this->data
        ]);
    }
}
