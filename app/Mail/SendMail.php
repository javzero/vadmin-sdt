<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $template;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $template, $data)
    {
        $this->subject = $subject;
        $this->template = $template;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $from = env('MAIL_FROM_ADDRESS');

        if(isset($this->data['lang']) && $this->data['lang'] == 'br')
            $from = env('MAIL_FROM_ADDRESS_BR');

        return $this->from($from)->subject($this->subject)
        ->markdown('emails.'.$this->template, [
            'data' => $this->data
        ]);

        // return $this->subject($this->subject)->markdown('vadmin.components.mail'.$this->view)
        // ->with(['data' => $this->data]);
    }
}
