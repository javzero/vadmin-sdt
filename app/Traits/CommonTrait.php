<?php

namespace App\Traits;

use App\Models\Menu;
use Auth;

trait CommonTrait {

    
    public function buildMenu() 
    {
        if(Auth::user()) {

            $userRoles = Auth::user()->roles->pluck('id');

            $parents = Menu::where('parent_id', 0)->whereHas('roles', function ($query) use ($userRoles) {
                            return $query->whereIn('id', $userRoles);
                        })->get();  

            $menu = [ "name" => "root" ];
            $childrens = [];

            $count = 0;

            foreach($parents as $key => $parent) 
            {
              
                $menu["children"][$key] = $parent;
                $menu["children"][$key]["children"] = $this->getChildrens($parent, $userRoles);
                $childrens = $this->getChildrens($parent, $userRoles);
                
                $childrenCount = 0;
                
                foreach($childrens as $key2 => $children) 
                {
                    $menu["children"][$key]["children"][$key2]["children"] =  $this->getChildrens($children, $userRoles);
                    
                    $childrenCount += 1;
                }  
                $count += 1;
            }

            // dd($menu);
            return json_encode($menu);
        }

    }


    public function getChildrens($parent, $userRoles)
    {
        return Menu::where('parent_id', $parent['id'])->whereHas('roles', function ($query) use ($userRoles) {
            return $query->whereIn('id', $userRoles);
        })->get();  
    }


}