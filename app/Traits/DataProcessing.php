<?php

namespace App\Traits;

trait DataProcessing 
{

    public function valuateBusiness($data = null) 
    {
        // For Test
        // $data['country_id'] = 'AR';
        
        $business_type = \App\Models\BusinessType::where('id', $data['business_type_id'])->first();
        if(!$business_type)
            return [ 'error', 'Falta tipo de negocio' ];

        $algorithmValues = \App\Models\QAlgorithm::all(); // Get dynamic algorith values

        // $data['country_id'] = 'AR'; // For Testing

        $localCurrencyValue = $this->getLocalCurrencyValue($data['country_id']);
        $values = [];
        
        $values['localCurrencyValue'] = $localCurrencyValue->manual_value;

        $values['original_data'] = [];
        
        // Convert local currency values to Uss currency
        // --------------------------------------------
        $net_income_actual_year = $this->convertLocalToUssCurrency($data['net_income_actual_year'], $localCurrencyValue);
        $net_income_last_year = $this->convertLocalToUssCurrency($data['net_income_last_year'], $localCurrencyValue);
     
        $values['net_income_actual_year_uss'] = $net_income_actual_year;
        $values['net_income_last_year_uss'] = $net_income_last_year;
        
        // --------------------------------------------

        foreach($data as $key => $item) 
        {
            $values['original_data'][$key] = $item;
        }

        // --------------------------------------------
        // BUSINESS AGE
        // --------------------------------------------
        $ageValue = 0;

        if($data['business_age'] >= 3) 
        { 
            $ageValue = $net_income_actual_year * 1.1 - $net_income_actual_year;
        } 
        
        $values['ageValue'] = $ageValue;

        // -------------------------------------------- 
        // VIEWS PER MONTH / YEAR
        // ---------------------------------------------
        
        // dd(self::viewsPerMonthValue($business_type->id, $algorithmValues));
        $viewsValue = 0;
      
        if($business_type->id != 4) // Do not calc workingHours if is App
        {
            if($business_type->id == 2)  // Youtube Channel
            {
                // Youtube Channel
                $viewsValue = $data['views_per_month']
                                * self::viewsPerMonthValue($business_type->id, $algorithmValues);
            } 
            else if($business_type->id == 5) // Sitio de contenido
            {
                // organico
                $viewsValue = calcPercent($data['views_per_month'], $data['organic_views_per_month']) 
                    * $algorithmValues->find(9)->value;
            }
            else 
            {
                // If not a youtube channel calculate percent with organic views and 
                // multiply views per month per 12 and convert to views per year.
                $viewsValue = calcPercent($data['views_per_month'], $data['organic_views_per_month']) 
                                * self::viewsPerMonthValue($business_type->id, $algorithmValues) 
                                * 12;
            }
        }

        $values['viewsValue'] = $viewsValue;

         // -------------------------------------------- 
        // SUBSCRIPTORS
        // ---------------------------------------------
        $subscriptorsValue = 0;
        if($business_type->id != 4) // Do not calc workingHours if is App
        {
            $subscriptorsValue = $data['total_subscriptors'] * self::suscriptorValue($business_type->id, $algorithmValues);
            $values['subscriptorsValue'] = $subscriptorsValue;
        }

        // --------------------------------------------
        // FOLLOWERS
        // ---------------------------------------------
        $followersValue = 0;
        if($business_type->id != 4) // Do not calc workingHours if is App
        {
            $followersValue = $data['total_followers'] * self::followerValue($business_type->id, $algorithmValues);
            $values['followersValue'] = $followersValue;
        }

        // --------------------------------------------
        // ACTIVE USERS
        // ---------------------------------------------

        $activeUsersValue = 0;
        if($business_type->id == 4) // Calc Active Users only if App
        {
            $activeUsersValue = $data['active_users'] * $algorithmValues->find(8)->value;
            $values['activeUsersValue'] = $activeUsersValue;
        }

        // --------------------------------------------
        // INCOME VARIATION
        // ---------------------------------------------

        $incomeVariation = 0;
        
        $incomeVariation = $net_income_actual_year * 2;

        $values['incomeVariation'] = $incomeVariation;

        // --------------------------------------------
        // INCOME VARIATION
        // ---------------------------------------------

        $digitalCapitalsValue = 0;
        
        if(array_key_exists('digitalCapital', $data)) {
            
            $digitalCapitals = \App\Models\DigitalCapital::whereIn('id', $data['digitalCapital'])->get();
        
            foreach($digitalCapitals as $digitalCapital) 
            {
                $digitalCapitalsValue += $digitalCapital->value;
            }
        }

        $values['digitalCapitalValue'] = $digitalCapitalsValue;

        // --------------------------------------------
        // CALC SUBTOTALS
        // ---------------------------------------------
              
        $subtotal = $ageValue + $viewsValue + $followersValue + $subscriptorsValue + $incomeVariation + $activeUsersValue + $digitalCapitalsValue;
        
        $values['subTotal'] = $subtotal;

        $totalUss = round($subtotal, 2); 
        $totalLocal = $this->convertUssToLocalCurrency($totalUss, $localCurrencyValue);        
        
        $values['total'] = $totalUss;
        $values['totalLocal'] = $totalLocal;

        return [ 'uss' => $totalUss, 'local' => $totalLocal, 'raw_data' => $values ];
        
    }


    // public function valuateBusinessOLD($data = null) 
    // {
    //     // dd($data);
    //     $business_type = \App\Models\BusinessType::where('id', $data['business_type_id'])->first();
    //     if(!$business_type)
    //         return [ 'error', 'Falta tipo de negocio' ];

    //     $algorithmValues = \App\Models\QAlgorithm::all(); // Get dynamic algorith values

    //     // $data['country_id'] = 'AR'; // For Testing

    //     $localCurrencyValue = $this->getLocalCurrencyValue($data['country_id']);
       
    //     $values = [];
        
    //     $values['localCurrencyValue'] = $localCurrencyValue->manual_value;

    //     $values['original_data'] = [];
        
    //     // Convert local currency values to Uss currency
    //     // --------------------------------------------
    //     $net_income_actual_year = $this->convertLocalToUssCurrency($data['net_income_actual_year'], $localCurrencyValue);
    //     $net_income_last_year = $this->convertLocalToUssCurrency($data['net_income_last_year'], $localCurrencyValue);
     
    //     $values['net_income_actual_year_uss'] = $net_income_actual_year;
    //     $values['net_income_last_year_uss'] = $net_income_last_year;
    //     // --------------------------------------------

    //     foreach($data as $key => $item) 
    //     {
    //         $values['original_data'][$key] = $item;
    //     }

    //     // --------------------------------------------
    //     // BUSINESS AGE
    //     // --------------------------------------------
    //     $ageValue = 0;

    //     if($data['business_age'] >= 3) 
    //     { 
    //         $ageValue = ($net_income_actual_year * 1.1) - $net_income_actual_year;
    //     } 
        
    //     $values['ageValue'] = $ageValue;
    //     // dd($values);
    //     // --------------------------------------------


    //     // --------------------------------------------
    //     // WORKING HOURS
    //     // ---------------------------------------------

    //     $workingHoursValue = 0;

    //     if($business_type->id != 4) // Do not calc workingHours if is App
    //     {
    //         if($data['working_hours'] <= 4) 
    //         {
    //             $workingHoursValue = ($net_income_actual_year * 1.05) - $net_income_actual_year;
    //         } 
    //     }

    //     $values['workingHoursValue'] = $workingHoursValue;
    //     // --------------------------------------------

    //     // -------------------------------------------- 
    //     // VIEWS PER MONTH / YEAR
    //     // ---------------------------------------------
        
    //     // dd(self::viewsPerMonthValue($business_type->id, $algorithmValues));
    //     $viewsValue = 0;
      
    //     if($business_type->id != 4) // Do not calc workingHours if is App
    //     {
    //         if($business_type->id == 2)  // Youtube Channel
    //         {
    //             // Youtube Channel
    //             $viewsValue = $data['views_per_month']
    //                             * self::viewsPerMonthValue($business_type->id, $algorithmValues);
    //         } 
    //         else if($business_type->id == 5) // Sitio de contenido
    //         {
    //             $viewsValue = $data['organic_views_per_month'] * $algorithmValues->find(9)->value;
                
    //         }
    //         else 
    //         {
    //             // If not a youtube channel calculate percent with organic views and 
    //             // multiply views per month per 12 and convert to views per year.
    //             $viewsValue = calcPercent($data['views_per_month'], $data['organic_views_per_month']) 
    //                             * self::viewsPerMonthValue($business_type->id, $algorithmValues) 
    //                             * 12; 
    //         }
    //     }

    //     $values['viewsValue'] = $viewsValue;
        
     
    //     // -------------------------------------------- 
    //     // SUBSCRIPTORS
    //     // ---------------------------------------------
    //     $subscriptorsValue = 0;
    //     if($business_type->id != 4) // Do not calc workingHours if is App
    //     {
    //         $subscriptorsValue = $data['total_subscriptors'] * self::suscriptorValue($business_type->id, $algorithmValues);
    //         $values['subscriptorsValue'] = $subscriptorsValue;
    //     }

    //     // --------------------------------------------
        
    //     // --------------------------------------------
    //     // FOLLOWERS
    //     // ---------------------------------------------
    //     $followersValue = 0;
    //     if($business_type->id != 4) // Do not calc workingHours if is App
    //     {
    //         $followersValue = $data['total_followers'] * self::followerValue($business_type->id, $algorithmValues);
    //         $values['followersValue'] = $followersValue;
    //     }

    //     // --------------------------------------------

    //     // --------------------------------------------
    //     // ACTIVE USERS
    //     // ---------------------------------------------

    //     $activeUsersValue = 0;
    //     if($business_type->id == 4) // Calc Active Users only if App
    //     {
    //         $activeUsersValue = $data['active_users'] * $algorithmValues->find(8)->value;
    //         $values['activeUsersValue'] = $activeUsersValue;
    //     }

    //     // --------------------------------------------
    //     // TOTAL DOWNLOADS
    //     // ---------------------------------------------
        
    //     // No value.

    //     // --------------------------------------------
    //     // INCOME VARIATION
    //     // ---------------------------------------------

    //     $incomeVariation = 0;
        
    //     $incomeVariation = $net_income_actual_year * 2.5;

    //     $values['incomeVariation'] = $incomeVariation;


    //     // Calc subtotals
    //     // --------------------------------------------
        
    //     $subtotal = $ageValue + $workingHoursValue + $viewsValue + $followersValue + $subscriptorsValue + $incomeVariation + $activeUsersValue;
        
    //     $values['subTotal'] = $subtotal;

    //     $digitalCapitalsValue = 0;
        
    //     if(array_key_exists('digitalCapital', $data)) {
            
    //         $digitalCapitals = \App\Models\DigitalCapital::whereIn('id', $data['digitalCapital'])->get();
        
    //         foreach($digitalCapitals as $digitalCapital) 
    //         {
    //             $digitalCapitalsValue += $digitalCapital->value;
    //         }
    //     }


    //     $values['digitalCapitalValue'] = $digitalCapitalsValue;

    //     $extras = 0;

    //     // --------------------------------------------
    //     // LOGISTICS
    //     // ---------------------------------------------
    //     // 2 Externa
    //     $logisticValue = 0;
        
    //     if($business_type->id == 1) // Calc Logistics only if Tienda Online
    //     {
    //         if(array_key_exists('logistic_id', $data) && $data['logistic_id'] == 2) {
    //             $logisticValue = ($net_income_actual_year * 1.02) - $net_income_actual_year;
    //             $values['logisticValue'] = $logisticValue;
    //         }    
    //     }
    //     $extras = $digitalCapitalsValue + $logisticValue;

    //     $totalUss = round($subtotal + $extras, 2); 
    //     $totalLocal = $this->convertUssToLocalCurrency($totalUss, $localCurrencyValue);        
        
    //     $values['total'] = $totalUss;
    //     $values['totalLocal'] = $totalLocal;



    //     return [ 'uss' => $totalUss, 'local' => $totalLocal, 'raw_data' => $values ];
    // }

    
    // --------------------------------------------
    // CURRENCY
    // ---------------------------------------------

    public static function getLocalCurrencyValue($countryId) 
    {
        return  \App\Models\Currency::where('country_id', $countryId)->first();
    }

    public function convertLocalToUssCurrency($value, $currencyValue)
    {   
        // dd($value, $currencyValue->manual_value);
        
        // dd(Http::get('https://www.dolarsi.com/api/api.php?type=valoresprincipales'));
        if($value > 0) 
        {
            if($currencyValue->manual_value != null) 
            {
                return twoDecimals(floatval($value) / $currencyValue->manual_value);
            } 
        } 
        return 0;
        
    }

    public function convertUssToLocalCurrency($value, $currencyValue)
    {
        // dd(Http::get('https://www.dolarsi.com/api/api.php?type=valoresprincipales'));
        if($value > 0) 
        {
            if($currencyValue->manual_value != null) 
            {
                return twoDecimals($currencyValue->manual_value * floatval($value));
            } 
        }

        return 0;
        
    }

    
    public static function viewsPerMonthValue($business_type_id, $algorithmValues) 
    {
        switch ($business_type_id) 
        {
            case 1: // Tienda Online
                return $algorithmValues->find(1)->value; // Valor Seguidor de tienda
                break;  
            case 2: // Canal de Youtube    
                return $algorithmValues->find(3)->value; // Valor Seguidor de tienda      
                break; 
            default:
                return $algorithmValues->find(102)->value; // 0.30;
                break;
        }
    }

    public static function suscriptorValue($business_type_id, $algorithmValues) 
    {
        switch ($business_type_id) 
        {
            case 1: // Tienda Online
                return $algorithmValues->find(2)->value; // Valor Suscriptor de tienda
                break; 
            case 2: // Youtube
                return $algorithmValues->find(5)->value; // Valor Suscriptor de youtube
                break; 
            case 3: // Influencer    
                return $algorithmValues->find(7)->value; // Valor Suscriptor influencer    
                break; 
            case 5: // Sitio de contenido    
                return $algorithmValues->find(11)->value; // Valor Suscriptor influencer    
                break;          
            default:
                return $algorithmValues->find(100)->value; // 0.20;
                break;
        }

    }

    public static function followerValue($business_type_id, $algorithmValues) 
    {

        switch ($business_type_id) 
        {
            case 1: // Tienda Online
                return $algorithmValues->find(1)->value; // Valor Seguidor de tienda
                break;  
            case 2: // Canal de Youtube    
                return $algorithmValues->find(5)->value; // Valor Seguidor de tienda      
                break;   
            case 3: // Influencer    
                return $algorithmValues->find(6)->value; // Valor Seguidor influencer    
                break; 
            case 5: // Sitio de contenido    
                return $algorithmValues->find(10)->value; // Valor Suscriptor influencer    
                break;       
            default:
                return $algorithmValues->find(101)->value; // 0.15;
                break;
        }

    }

  
}