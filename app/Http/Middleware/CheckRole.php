<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {   
        // unregistered.blade.php
        if(Auth::user()) 
        {
            if(count(array_intersect(Auth::user()->roles->pluck('slug')->toArray(), explode('|', $role))) > 0) 
            {
                return $next($request);
            }
            
            return redirect('/vadmin')->withError('No tiene los roles necesarios para ingresar a la sección');
        }

        return redirect('/login');
    }
}
