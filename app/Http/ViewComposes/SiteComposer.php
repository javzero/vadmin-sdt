<?php 

namespace App\Http\ViewComposes;

use Illuminate\Contracts\View\View;
use App\Models\Setting;

class SiteComposer
{
    public function compose(View $view)
    {
        $settings = Setting::find(1);
        
        if($settings)
            $view->with('settings', $settings);
    }
}