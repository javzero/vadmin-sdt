<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LangsController extends Controller
{
    public function showLangs() 
    {
        return view('vadmin.core.langs'); 
    }
}
