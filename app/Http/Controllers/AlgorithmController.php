<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QAlgorithm;
// use Illuminate\Support\Facades\Validator;

class AlgorithmController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
	}


    public function getValues()
    {   
        try
        {
            $values = QAlgorithm::all();
            return $values;
        }
        catch (\Exception $e) 
        {
            return H_Response(200, 'error', 'Falla todo: '. $e->getMessage());
        }
    }

    public function updateValues(Request $request)
    {
        try
        {
            foreach ($request->all() as $newValue) {
                
                $item = QAlgorithm::find($newValue['id']);
                $item->value = $newValue['value'];
                $item->save();

            }

            return H_Response(200, 'success', 'Valores del algoritmo actualizados', '');
        }
        catch (\Exception $e) 
        {
            return H_Response(200, 'error', 'Falla guardado de valores del algoritmo: '. $e->getMessage());
        }
    }

}
