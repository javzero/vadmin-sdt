<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Mail\Markdown;
use Mail;


class TestController extends Controller
{

	public function TestProcessMainFormForManager($id) 
    {
        
        $data = \App\Models\Article::find($id);
        if($data)
            return view('emails.tests.business-value-manager', compact('data'));
        else
            return 'No hay ningún artículo con el ID: ' . $id;
    }

    public function TestProcessMainFormForClient($id) 
    {
        
        $data = \App\Models\Article::find($id);
        if($data)
            return view('emails.tests.main-form-for-client', compact('data'));
        else
            return 'No hay ningún artículo con el ID: ' . $id;
    }


    public function TestEmailContactFormForClient($country = 'ar') 
    {

        $data = collect([
            'lang' => $country,
            'name' => 'Nombre del cliente',
            'email' => env('MAIL_TRAP_EMAIL'),
            'message' => 'Este es el mensaje',
            'countryName' => 'Argentina',
            'phone' => '+5491158090227',
            'articleCode' => 1,
            'articleTitle' => 'Ut ullam accusantium est dicta dolorem.',
        ]);

        $markdown = new Markdown(view(), config('mail.markdown'));
        return $markdown->render('emails.contact-main-site-for-client', compact('data'));
      
    }

    public function TestEmailProcessMainForm($articleId = 1, $country = 'ar')
    {
        $data = \App\Models\Article::find($articleId);
        $data->lang = $country;
        if($data) 
        {
            $markdown = new Markdown(view(), config('mail.markdown'));
            return $markdown->render('emails.main-form-for-client', compact('data'));
        }
        else
        {
            return 'No hay ningún artículo con el ID: ' . $articleId;
        }
    }

    public function TestEmailMFContacts($customerId = 'null') 
    {
        if(!$customerId) dd("No customer ID specified on url");

        $data = \App\Models\MfContact::where('id', $customerId)->first();
        
        $markdown = new Markdown(view(), config('mail.markdown'));
        return $markdown->render('emails.mfcontacts', compact('data'));

    }

}
