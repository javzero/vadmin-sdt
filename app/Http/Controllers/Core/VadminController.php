<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;
use Auth;

class VadminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {   
        return view('vadmin.index');
    }

    
	public function getAuthUser() 
	{	
		$roles = Auth::user()->roles;
		return [ 'data' => Auth::user(), 'roles' => $roles ];
	}


    
    public function page404()
    {
        return view('vadmin.index')->with('message', 'La página solicitada no existe');
    }

}
