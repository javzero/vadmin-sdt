<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;
use App\Traits\CrudTrait;

class CountryController extends Controller
{

	use CrudTrait;

	public function __construct()
    {
		$this->middleware('check-role:developer|admin');
	}

    public function index(Request $request)
    {	
		if($request->ajax())
		{	

			// Default
			$pagination = $this->setResultsPerPage($request->resultsPerPage);
			$order = 'asc';
			
			if(isset($request->order))
				$order = $request->order;
			
			$orderBy = $this->orderBy($request->orderBy, 'id');		
			
			$items = Country::search($request->search)->orderBy($orderBy, $order)->paginate($pagination);

			$data = $this->makeData($items);
			
			return $data;
		}
		
		return view('vadmin.core.countries');
	}
	
	public function getCountries() 
	{
        return Country::all();	
	}
	
	public function save(Request $request)
	{
		// dd($request->all());
		$successMessage = 'País guardado correctamente';

		$item = Country::firstOrNew(['id' => $request->id]);

		// // If we are creating a new Country we have to set 
		// if($request->id == null) {
		// 	$item->id = Country::orderBy('id','desc')->first()->id + 1;
		// }

		$item->fill($request->all());

		try {
			$validation = $this->validator($request->all(), $request->id);
			
			if($validation->fails())
				return H_Response(200, 'error', $validation->errors());
			

			$item->name = $request->name;
			$item->save();

			return H_Response(200, 'success', $successMessage);
		} 
		catch (\Exception $e) 
		{
			return H_Response(200, 'error', 'Falla creación del país: '. $e->getMessage());
		}
		
	}

	public function destroy($ids)
    {
		$op = $this->GenericDestroy($ids, 'Country');

		if($op == 'success')
			return H_Response(200, 'success', 'País eliminado');
		else
			return H_Response(200, 'error', $op);
    }

		
	public function validator(array $data, $itemId = null)
    {
	
		if(isset($itemId)) // If is updating exclude updated item
		{
			$fields = [
				'name' => 'required|max:60|unique:countries,name,'.$itemId,
				'char_id' => 'required|unique:countries,name,'.$itemId
			];
		}
		else // Creating new item
		{
			$fields = [
				'name' => 'required|max:60|unique:countries,name',
				'char_id' => 'required|unique:countries,name'
			];
		}

		$messages = [
			'name.required'    => 'Debe ingresar un nombre',
			'name.unique'      => 'El país ingresado ya existe en la base de datos',
			'char_id.required' => 'Debe ingresar un prefijo',
			'char_id.unique'   => 'El prefijo ya existe en la base de datos'
		];


		$validation = Validator::make($data, $fields, $messages);
		
		return $validation;
	}

}
