<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\State;
use App\Traits\CrudTrait;


class StateController extends Controller
{

	use CrudTrait;

	public function __construct()
    {
        $this->middleware('check-role:developer|admin');
	}


    public function index(Request $request)
    {	
		if($request->ajax())
		{		
		
			$pagination = $this->setResultsPerPage($request->resultsPerPage);
			$order = 'asc';
			
			if(isset($request->order))
				$order = $request->order;
			
			$orderBy = $this->orderBy($request->orderBy, 'id');		
			
			$items = State::search($request->search)->orderBy($orderBy, $order)->with('country')->paginate($pagination);
			
			$data = $this->makeData($items);
			
			return $data;
		} 

		return view('vadmin.core.states');
	}
	
	public function getStates($countryId = null) 
	{
		if($countryId)
			return State::where('country_id', $countryId)->get();
		
		return State::all();
	}
	
	public function save(Request $request)
	{
		// dd($request->all());
		$successMessage = 'Operación realizada correctamente';

		$item = State::firstOrNew(['id' => $request->id]);

		try {

			
			$validation = $this->validator($request->all(), $request->id);
			if($validation['status'] === 0) {
				return H_Response(200, 'error', $validation);
			}

			// if(!$validation)
			// 	return H_Response(200, 'error', $validation->errors);


			// if($validation->fails())
			// 	return H_Response(200, 'error', $validation->errors());

			$item->fill($request->all());
			
			// ID Construction
			if($request->id == null) {
				
				// States ID have as firtst number the country ID. So if we have existings stats we take the ID 
				// from the last state on the table from that country and sum 1.
				// If there is no state on that country we take the country id and use that as first ID.

				$newId = \App\Models\State::where('country_id', $request->country_id)->orderBy('id', 'DESC')->first();

				if($newId) 
					$item->id = $newId->id + 1;
				else {
					$country = \App\Models\Country::where('char_id', $request->country_id)->orderBy('id', 'DESC')->first();
					if($country) {
						// Ids of states are separated by 100000, every state ID starts with the country ID times 10000.
						$item->id = ((int)$country->id * 100000);
					}
						
				}
				
			}
			
			$item->save();

			return H_Response(200, 'success', $successMessage);
		} 
		catch (\Exception $e) 
		{
			return H_Response(200, 'error', 'Falla creación de provincia: '. $e->getMessage());
		}
		
	}

	public function show($id)
	{
		dd("SHOW ?");
	}

	public function destroy($ids)
    {
		$op = $this->GenericDestroy($ids, 'State');

		if($op == 'success')
			return H_Response(200, 'success', 'Estado eliminado');
		else
			return H_Response(200, 'error', $op);
    }

		
	public function validator(array $data, $itemId = null)
    {	
		if(isset($itemId)) {
				
			if(State::where('country_id', $data['country_id'])->where('name', '=', $data['name'])
			->where('id', '!=', $data['id'])->get()->count() === 0) 
			{
				return ['status' => 1];
			}			
		}

		$state = State::where('country_id', $data['country_id'])->where('name', '=', $data['name'])->get();

		if($state->isEmpty()) {
			return ['status' => 1];
		} else {
			return ['name' =>  [0 => 'El estado ingresado ya existe en la base de datos'], 'status' => 0];
		}
		
		



		// if(isset($itemId)) // If is updating exclude updated item
		// {
		// 	$state = all();
		// 	/*  */State::where('country_id', $data['country_id']);
		// 	dd($state);
		// 	dd($data);
		// }
		// else // Creating new item
		// {
		// 	$fields = [
		// 		'name' => 'required|max:60|unique:states,name'
		// 	];
		// }

		// if(isset($itemId)) // If is updating exclude updated item
		// {
		// 	$fields = [
		// 		'name' => 'required|max:60|different:country|unique:states,name,'.$itemId,
		// 	];
		// }
		// else // Creating new item
		// {
		// 	$fields = [
		// 		'name' => 'required|max:60|unique:states,name'
		// 	];
		// }

		// $messages = [
		// 	'name.required'  => 'Debe ingresar un nombre',
		// 	'name.unique'    => 'El estado/provincia ingresado ya existe en la base de datos',
		// ];


		// $validation = Validator::make($data, $fields, $messages);
		
		// return $validation;
	}

	// public function validateByCoutry()

}
