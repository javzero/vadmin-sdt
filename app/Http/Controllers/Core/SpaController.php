<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SpaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
	}

    public function index()
    {
        return view('vadmin.index');
    }

}
