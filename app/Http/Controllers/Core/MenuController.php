<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\CrudTrait;
use App\Models\Menu;

class MenuController extends Controller
{   
    use CrudTrait;
    
	public function __construct()
    {
		$this->middleware('check-role:developer');
	}
    
    public function index(Request $request)
    {	

        if($request->ajax())
		{
			$pagination = $this->setResultsPerPage($request->resultsPerPage);

			$items = Menu::search($request->search)
				->order($request->orderBy, $request->order)
				->with('roles')
				->paginate($pagination);

			return $this->makeData($items);
		}


        return view('vadmin.core.menus');

    }
    
    public function save(Request $request)
	{
		
		if($request->id != null) 
		{
			$item = Menu::where('id', $request->id)->first();
			$successMessage = 'Item del menú actualizado';
		}
		else
		{
			$item = new User();
			$successMessage[0] = 'Item del Menú creado';
		}

		try {
			$validation = $this->validator($request->all(), $request->id);
			
			if($validation->fails())
				return H_Response(200, 'error', $validation->errors());
			
            $item->fill($request->all());
			$item->save();

			return H_Response(200, 'success', $successMessage);
		} 
		catch (\Exception $e) 
		{
			return H_Response(200, 'error', 'Falla creación del item del menu: '. $e->getMessage());
		}
		
	}

    
	public function validator(array $data, $itemId = null)
    {
		if(isset($itemId) && $itemId != null) // If is updating exclude updated item
		{
			$fields = [
				'name' => 'required|max:30|unique:menus,name,'.$itemId,
			];
		}
		else // Creating new item
		{
			$fields = [
				'name' => 'required|max:20|unique:users,menus',
			];
		}

		$messages = [
			'username.required'  => 'Debe ingresar un nombre',
		];

		$validation = Validator::make($data, $fields, $messages);
		
		return $validation;
    }

    

}
