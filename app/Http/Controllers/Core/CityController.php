<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\City;
use App\Traits\CrudTrait;

class CityController extends Controller
{

	use CrudTrait;

	public function __construct()
    {
        $this->middleware('check-role:developer|admin');
	}

    public function index(Request $request)
    {	
		if($request->ajax())
		{	

			// Default
			$pagination = $this->setResultsPerPage($request->resultsPerPage);
			$order = 'asc';
			
			if(isset($request->order))
				$order = $request->order;
			
			$orderBy = $this->orderBy($request->orderBy, 'id');		
			
			$items = City::search($request->search)->orderBy($orderBy, $order)->with('country')->with('state')->paginate($pagination);

			$data = $this->makeData($items);
			
			return $data;
		}
		
		return view('vadmin.core.cities');
	}
	
	public function getCities($stateId = null) 
	{	
		// dd($stateId);
		if($stateId) 
			return City::where('state_id', $stateId)->get();
        return City::all();	
	}
	
	public function save(Request $request)
	{
		// dd($request->all());
		$successMessage = 'Ciudad guardada correctamente';

		$item = City::firstOrNew(['id' => $request->id]);

		$item->fill($request->all());

		try {
			$validation = $this->validator($request->all(), $request->id);
			
			if($validation->fails())
				return H_Response(200, 'error', $validation->errors());
			
			$item->save();

			return H_Response(200, 'success', $successMessage);
		} 
		catch (\Exception $e) 
		{
			return H_Response(200, 'error', 'Falla creación de la ciudad: '. $e->getMessage());
		}
		
	}

	public function destroy($ids)
    {
		$op = $this->GenericDestroy($ids, 'City');

		if($op == 'success')
			return H_Response(200, 'success', 'Ciudad eliminada');
		else
			return H_Response(200, 'error', $op);
    }

		
	public function validator(array $data, $itemId = null)
    {
	
		if(isset($itemId)) // If is updating exclude updated item
		{
			$fields = [
				'name' => 'required|max:60|unique:cities,name,'.$itemId,
				'char_id' => 'required|unique:cities,name,'.$itemId
			];
		}
		else // Creating new item
		{
			$fields = [
				'name' => 'required|max:60|unique:cities,name',
				'country_id' => 'required'
			];
		}

		$messages = [
			'name.required'    => 'Debe ingresar un nombre',
			'name.unique'      => 'El país ingresado ya existe en la base de datos',
			'country_id.required' => 'Debe ingresar un prefijo'
		];


		$validation = Validator::make($data, $fields, $messages);
		
		return $validation;
	}

}
