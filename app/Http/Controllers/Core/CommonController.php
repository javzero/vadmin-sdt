<?php

namespace App\Http\Controllers\Core;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonController extends Controller
{

	public function toggleActive(Request $request)
	{	
		// dd($request->all());
		
		$active = 1;

		if($request->active == 1) $active = 0;
		
		try {
			$model = 'App\Models\\' . $request->model;
			$item = $model::where('id', $request->id)->first();
			$item->active = $active;
			$item->save();

			return H_Response(200, 'success', 'Estado actualizado');
			
		} catch(\Exception $e) {
			return H_Response(200, 'error', 'No se pudo actualizar el estado. '. $e->getMessage() );
		}
	}
}
