<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Article;
use App\Mail\SendMail;
use Mail;
use App\Traits\DataProcessing;

class FormsController extends Controller
{
    use DataProcessing;
   
	public function mainForm($customerId = null, $customerEmail = null)
	{
        $customerData = "0";
        if($customerId != null && $customerEmail != null) 
        {
            $customerData = \App\Models\MfContact::where('id', $customerId)->where('customer_email', $customerEmail)->first();
            if(!$customerData) {
                $customerData = "0";
            }
        }

        $countries = \App\Models\Country::all();
		$businessTypes = \App\Models\BusinessType::all();
        $categories = \App\Models\ArticleCategory::all();
		$digitalCapital = \App\Models\DigitalCapital::where('name', '!=', '¿Tiene actualmente Google Analytics?')->where('name', '!=', '¿Tiene actualmente Google Ads/AdMob?')->get();
        $socialNetwork = \App\Models\Social::all()->pluck('name', 'id');

		return view('site.sections.main-form', 
            compact('countries', 'businessTypes', 'digitalCapital', 'socialNetwork', 'categories', 'customerData')); 
	}

    public function processMainForm(Request $request)
    {
        // dd($this->valuateBusiness($request->all())); // Testing
        
        $item = Article::firstOrNew(['customer_email' => $request->customer_email]);
        $item->fill($request->all());       
        $item->save();
        
        try
        {
            $businessValue = $this->valuateBusiness($request->all());

            $item->business_value = $businessValue['uss'];
            $item->business_value_local = $businessValue['local'];
            $item->value_raw_data = $businessValue;
            $item->lang = $request->lang;
            $item->save();

            if($request->digital_capital) 
            {
                $item->digitalCapital()->detach();
                foreach($request->digital_capital as $digitalCapital)
                {	
                    $item->digitalCapital()->attach($digitalCapital['id']);
                }
            }

            if($request->social)
            {
                foreach($request->social as $socialNetwork) 
                {  
                    $item->social()->detach();
                    $item->social()->attach( 
                        $socialNetwork['id'], ['account_url' => $socialNetwork['pivot']['account_url'],]
                    );
                }
            }

            try
            {
                // Delete pre contact register
                $mfContact = \App\Models\MfContact::where('customer_email', $item->customer_email)->get()
                ->each(function ($item, $key) {
                    $item->delete();
                });
            } 
            catch(\Exception $e)
            {
                // dd($e->getMessage());
            }
            
        }
        catch (\Exception $e) 
        {
            dd($e->getMessage());
        }
        
        // SENDING MAILS 
        // dd($item->business_value);
        if(env('USE_DEV_MAIL')) 
        {
            $customerEmail = env('DEV_MAIL');
            if(isset($request->lang) && $request->lang == 'br') {
                $appManagerEmail = env('DEV_MAIL_APP_MANAGER');
            }
        }
    
        try 
        {
            if(env('SEND_MAILS')) 
            {   
                $customerEmail = $item->customer_email;
                $appManagerEmail = \App\Models\Setting::find(1)->first()->email;
                $subject = 'Solicitud de cotización de negocio';

                if(isset($request->lang) && $request->lang == 'br') {
                    $subject = 'Pedido de orçamento comercial';
                    $appManagerEmail = \App\Models\Setting::find(1)->first()->email_br;
                }
                
                Mail::to($appManagerEmail)->queue(new SendMail($subject, 'business-value-manager', $item));
                Mail::to($customerEmail)->queue(new SendMail($subject, 'main-form-for-client', $item));
            }
            
            
            // Final Response
            $successMessage = 'Hemos recibido tu solicitud de cotización. <br> Nos comunicaremos a la brevedad.';
            $errorMessage = 'Error al enviar el mensaje';
            
            if(isset($request->lang) && $request->lang == 'br') {
                $successMessage = 'Recebemos sua solicitação de orçamento. Entraremos em contato em breve.';
                $errorMessage = 'Erro ao enviar mensagem';
            }
            return H_Response(200, 'success', $successMessage, null, 'redirect');
        } catch (\Exception $e) {
            $errorMessage = 'Error al enviar el mensaje';
            return H_Response(200, 'error', $errorMessage, $e->getMessage());
        }
    }
   
    public function validateMainFormStepOne(Request $request)
    {

        $fields = [
            'customer_firstname' => 'required',
            'customer_lastname' => 'required',
            'customer_phone_area' => 'required',
            'customer_phone_number' => 'required',
            'customer_email' => 'required|email:rfc',
            'customer_phone'=> 'required',
            'country_id' => 'required',
            'state_id' => 'required',
        ];

        if(isset($request->lang) && $request->lang == 'br') {
            $messages = [
                'customer_firstname.required' => 'Você deve inserir um nome',
                'customer_lastname.required' => 'Você deve inserir um sobrenome',
                'customer_phone_area.required' => 'Você deve inserir seu código de área',
                'customer_phone_number.required' => 'Você deve inserir o seu número de telefone sem espaço ou caracteres.',
                'customer_phone.required' => 'Você deve inserir um telefone',
                'customer_email.required' => 'Você deve inserir um e-mail',
                'customer_email.email' => 'Este campo deve ser um email',
                'customer_email.unique' => 'Você já solicitou um orçamento com o e-mail inserido',
                'country_id.required' => 'Você deve selecionar um país',
                'state_id.required' => 'Você deve selecionar um estado/província',
            ];
                               
        }
        else
        {
            $messages = [
                'customer_firstname.required' => 'Debe ingresar un nombre',
                'customer_lastname.required' => 'Debe ingresar un apellido',
                'customer_phone_area.required' => 'Debe ingresar su código de área',
                'customer_phone_number.required' => 'Debe ingresar su número de teléfono',
                'customer_phone.required' => 'Debe ingresar un teléfono',
                'customer_email.required' => 'Debe ingresar un email',
                'customer_email' => 'Este campo debe ser un email',
                'customer_email.unique' => 'Ya ha solicitado un presupuesto con el email ingresado',
                'country_id.required' => 'Debe seleccionar un País',
                'state_id.required' => 'Debe seleccionar un Estado/Provincia',
            ];
        }

        $validation = Validator::make($request->all(), $fields, $messages);

        if($validation->fails())
            return H_Response(200, 'error', $validation->errors());
        try {
            // Save contact in case user leaves the form.
            $this->saveMfContact($request->all());
        } catch (\Exception $e) { 
            // dd($e->getMessage());
            // Ignore error and let the user continue the process
        }

        return H_Response(200, 'success', "Correcto");

    }

    public function saveMfContact($data)
    {
        \App\Models\MfContact::saveMfContact($data);
    }

    public function validateMainFormStepTwo(Request $request)
    {

        $data = $request->all();

        if($request->business_type_id == 0)
            $data['business_type_id'] = null;
        
        if($request->category == 0)
            $data['category'] = null;

        $fields = [
            'description' => 'required',
            'category_id' => 'required',
            'business_type_id' => 'required',
            'business_age' => 'required|numeric|min:0',
            'working_hours' => 'required|numeric|min:0',
            'net_income_last_year' => 'required|numeric|min:0',
            'net_income_actual_year' => 'required|numeric|min:0',
            'views_per_month' => 'required|numeric|min:0',
            'organic_views_per_month' => 'required|numeric|min:0|max:100',
            'total_followers' => 'required|numeric|min:0',
            'total_subscriptors' => 'required|numeric|min:0',
            'active_users' => 'required|numeric|min:0',
            'total_downloads' => 'required|numeric|min:0',
        ];

        if($data['business_type_id'] == 1 || $data['business_type_id'] == 5) // Tienda online || Sitio de Contenido
        {
            $fields['domain_name'] = 'required';    
        }
        if($data['business_type_id'] == 1) // Tienda online
        {
            $fields['logistic_id'] = 'required';
        }

        if(isset($request->lang) && $request->lang == 'br') {
            $messages = [
                'description.required' => 'Campo obrigatório',
                'category_id.required' => 'Campo obrigatório',
                'business_type_id.required' => 'Campo obrigatório',
                'business_age.required' => 'Campo obrigatório',
                'working_hours.required' => 'Campo obrigatório',
                'net_income_last_year.required' => 'Campo obrigatório',
                'net_income_actual_year.required' => 'Campo obrigatório',
                'views_per_month.required' => 'Campo obrigatório',
                'organic_views_per_month.required' => 'Campo obrigatório',
                'total_followers.required' => 'Campo obrigatório',
                'total_subscriptors.required' => 'Campo obrigatório',
                'active_users.required' => 'Campo obrigatório',
                'total_downloads.required' => 'Campo obrigatório',
                'organic_views_per_month.numeric' => 'Você deve inserir um número',
                'organic_views_per_month.min' => 'O número deve ser maior que 0', 
                'organic_views_per_month.max' => 'O número deve ser igual ou inferior a 100',
            ];
        }
        else 
        {   
            $messages = [
                'description.required' => 'Completar campo',
                'category_id.required' => 'Completar campo',
                'business_type_id.required' => 'Completar campo',
                'business_age.required' => 'Completar campo',
                'working_hours.required' => 'Completar campo',
                'net_income_last_year.required' => 'Completar campo',
                'net_income_actual_year.required' => 'Completar campo',
                'views_per_month.required' => 'Completar campo',
                'organic_views_per_month.required' => 'Completar campo',
                'total_followers.required' => 'Completar campo',
                'total_subscriptors.required' => 'Completar campo',
                'active_users.required' => 'Completar campo',
                'total_downloads.required' => 'Completar campo',
                'organic_views_per_month.numeric' => 'Debe ingresar un número',
                'organic_views_per_month.min' => 'El número debe ser mayor a 0', 
                'organic_views_per_month.max' => 'El número debe ser igual o menor a 100',
            ];
        }

		$validation = Validator::make($data, $fields, $messages);

		if($validation->fails())
            return H_Response(200, 'error', $validation->errors());

        return H_Response(200, 'success', "Correcto");
    }


    public function validateBusinessValueFormStepTwo(Request $request)
    {
        $fields = [
            'business_age' => 'required|numeric|min:0',
            'working_hours' => 'required|numeric|min:0',
            'net_income_last_year' => 'required|numeric|min:0',
            'net_income_actual_year' => 'required|numeric|min:0',
            'views_per_month' => 'required|numeric|min:0',
            'organic_views_per_month' => 'required|numeric|min:0|max:100',
            'total_followers' => 'required|numeric|min:0',
            'total_subscriptors' => 'required|numeric|min:0',
        ];

		$messages = [
            'organic_views_per_month.max' => 'El porcentaje máximo es 100%',
		];

		$validation = Validator::make($request->all(), $fields, $messages);
        
        if($validation->fails())
            return H_Response(200, 'error', $validation->errors());

        return H_Response(200, 'success', "Correcto");
	}

    
    public function submitContactForm(Request $request) 
    {
        $clientEmail = $request->email;

        if($request->lang == 'br')
        {
            $targetEmail = \App\Models\Setting::find(1)->first()->email_br;
            $subject = 'Contato da web';
            $subjectForClient = "Obrigado ". $request->name ." pelo contato. Quer marcar uma conversa?";
            $successMessage = 'Formulário enviado com sucesso';
            $errorMessage = 'Erro ao enviar mensagem';
        }
        else 
        {
            $targetEmail = \App\Models\Setting::find(1)->first()->email;
            $subject = 'Contacto desde la web';
            $subjectForClient = "Gracias ". $request->name ." por el contacto. Querés marcar una llamada?";
            $successMessage = 'Formulario enviado correctamente';
            $errorMessage = 'Error al enviar el mensaje';
        }

        $validation = $this->validateConctactForm($request->all());

        if($validation->fails())      
            return H_Response(200, 'error', $errorMessage, $validation->errors());

        if(env('USE_DEV_MAIL')) {
            $targetEmail = env('DEV_MAIL');
        }

        try {
            \App\Models\Contact::saveContact($request->all());
        } catch (\Exception $e) { 
            dd($e->getMessage()); 
        }
        
        

        try {
            Mail::to($targetEmail)->send(new SendMail($subject, 'contact-main-site',  $request->all()));
            Mail::to($clientEmail)->send(new SendMail($subjectForClient, 'contact-main-site-for-client',  $request->all()));

            return H_Response(200, 'success', $successMessage);
            
        } catch (\Exception $e) {

            dd($e->getMessage());
            return H_Response(200, 'error', $errorMessage, $e->getMessage());
        }
        return H_Response(200, 'error', 'Falla creación del rubro: '. $e->getMessage());
    }

    public function validateConctactForm($data)
    {
        $fields = [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'country_id' => 'required'
        ];

        if($data['lang'] == 'br')
        {
            $messages = [
                'name.required' => 'Você deve inserir um nome',
                'email.required' => 'Você deve inserir um e-mail',
                'email.email' => 'Você deve inserir um e-mail válido',
                'phone.required' => 'Você deve inserir um telefone',
                'country_id.required' => 'Você deve inserir um país'
            ];
        }
        else 
        {
            $messages = [
                'name.required' => 'Debe ingresar un nombre',
                'email.required' => 'Debe ingresar un email',
                'email.email' => 'Debe ingresar un email válido',
                'phone.required' => 'Debe ingresar un teléfono',
                'country_id.required' => 'Debe ingresar un país'
            ];
        }

       return Validator::make($data, $fields, $messages);
    }
	
}
