<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Article;
use App\Models\ArticleCategory;

use App\Traits\CrudTrait;

use Carbon\Carbon;

class SiteController extends Controller
{
	use CrudTrait;

    public function index()
    {
		if(env('SITE_DOWN'))
			return view('site.site-down');
		
		$countries = \App\Models\Country::all()->pluck('name', 'char_id');
		return view('site.index', compact('countries'));
	}

	public function list(Request $request)
	{

		// $items = Article::select([
		// 	'id',
		// 	'name', 
		// 	'title', 
		// 	'country_id',
		// 	'state_id',
		// 	'category_id',
		// 	'business_type_id',
		// 	'domain_name',
		// 	'net_income_actual_year', 
		// 	'net_income_last_year',
		// 	'total_followers',
		// 	'total_subscriptors',
		// 	'male_traffic',
		// 	'female_traffic',
		// 	'business_age',
		// 	'business_value_manual',
		// 	'business_value_local',
		// 	'status',
		// 	'position',
		// 	'updated_at'
		// 	])
		// 	->active()
		// 	->search($request->searchWord)
		// 	->fromCountry($request->country_id)
		// 	->category($request->category_id)
		// 	->with('businessType')
		// 	->with('country')
		// 	->with('state')
		// 	->with('category')
		// 	->position()
		// 	->orderBy('updated_at', 'DESC')
		// 	// ->position()
		// 	->get();
		// 	// ->get();
		// 	// ->paginate($pagination);
		// 	//dd($items);

		// 	// dd($items);
		// 	$count = 0;
		// 	foreach ($items as $item) {
		// 	print_r($item->updated_at . "| Pos: " . $item->position ) ;
		// 			echo "<br>";
		// 			$count++;
		// 		}
		// 	echo "Count: " . $count;
		// 	die();


		if($request->ajax())
		{	
			$pagination = $this->setResultsPerPage($request->resultsPerPage);

			$items = Article::select([
				'id',
				'name', 
				'title', 
				'country_id',
				'state_id',
				'category_id',
				'business_type_id',
				'domain_name',
				'net_income_actual_year', 
				'net_income_last_year',
				'total_followers',
				'total_subscriptors',
				'male_traffic',
				'female_traffic',
				'business_age',
				'business_value_manual',
				'business_value_local',
				'status',
				'position',
				'updated_at'
				])
				->active()
				->search($request->searchWord)
				->fromCountry($request->country_id)
				->category($request->category_id)
				->with('businessType')
				->with('country')
				->with('state')
				->with('category')
				->position()
				->orderBy('updated_at', 'DESC')
				->paginate($pagination);
				//dd($items);


				$data = $this->makeData($items);	
				return $data;
		}
		
		$countries = \App\Models\Country::all()->pluck('name', 'id');
		return view('site.sections.list', compact('countries'));
		
	}

	
	public function show($id) 
	{
		$item = Article::where('id', $id)
			->with('country')
			->with('category')
			->with('state')
			->with('digitalCapital')
			->with('social')
			->first();

		return view('site.sections.show', compact('item'));
	}

	public function contactForm(Request $request) 
	{
		$article = [];

		if($request->articleId) {
			$article = Article::where('id', $request->articleId)->first();
		} 
		
		return view('site.sections.contact', compact('article'));
		// return view('site.sections.contact');
	}

	public function dev()
	{
		return view('site.index');
	}
	
	public function testList(Request $request)
	{	
		return view('site.list-test');
	}

	public function getArticles(Request $request)
	{	

		$articles = Article::age($request->age)
			->category($request->category)
			->with('category')
			->order($request->orderBy, $request->order)
			->get();

		return $articles;
    }
	
    public function getCategories()
    {
		return ArticleCategory::all();
	}

		
	public function getCategoriesByCountry($countryId) 
	{

		$categories = ArticleCategory::whereHas('articles', function ($query) use($countryId) {
			$query->where('country_id', $countryId)->active();
		})->get();
		
		return $categories;
	}
	

	public function getSocialNetworks()
    {
		return \App\Models\Social::all()->pluck('name', 'id');
	}
	
	public function getDigitalCapitals()
    {
		return \App\Models\DigitalCapital::all();
	}
	
	public function getCtesads(Request $request)
    {
        return \App\Ad::all();
	}

	public function getCountries(Request $request) 
	{
		
		if($request->ajax())
		{	

			return \App\Models\Country::all();
		}

		return \App\Models\Country::all();	
	}

	public function getStates($countryId) 
	{
		// dd($countryId);
		return \App\Models\State::where('country_id', $countryId)->get();
	}

}
