<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Post;

use App\Traits\CrudTrait;

use Carbon\Carbon;

class PostController extends Controller
{
	use CrudTrait;

    public function index(Request $request)
    {	
		if($request->ajax())
		{		
			$pagination = $this->setResultsPerPage($request->resultsPerPage);
			$items = Post::select([
				'id',
				'title', 
				'lang',
				'featured_image',
				'created_at',
				'order',
				'active'
				])
				->searchByTag($request->searchByTag)
				->lang($request->lang)
				->webSearch($request->search)
				->with('tags')
				->active()
				->order($request->orderBy, $request->order)		
				->orderBy('order', 'ASC')
				->paginate($pagination);	

			$data = $this->makeData($items);
			return $data;
		}
		
		$tags = \App\Models\PostTag::all();
		
		return view('site.sections.posts', compact('tags'));

    }

	
	public function show(Request $request, $id) 
	{
		$item = Post::with('tags')->find($id);
		// $item = Post::where('id', $id)->first()->with('tags')->get();

		return view('site.sections.post-show', compact('item'));
	}

}