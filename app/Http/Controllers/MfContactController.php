<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\CrudTrait;
use App\Models\MfContact;
use App\Exports\MfContactExport;
use Excel;
use Carbon\Carbon;

class MfContactController extends Controller
{
    use CrudTrait;

    public function __construct()
    {
        $this->middleware('auth');
	}


    public function index(Request $request)
    {	

        if($request->ajax())
		{
			$pagination = $this->setResultsPerPage($request->resultsPerPage);
			
			$items = MfContact::search($request->search)
                ->order($request->orderBy, $request->order)
                ->with('country')
                ->paginate($pagination);

			$data = $this->makeData($items);
            
			return $data;
		}

        return view('vadmin.mfcontacts');
    
    }

    /*
	|--------------------------------------------------------------------------
	| Export
	|--------------------------------------------------------------------------
	*/

	public function checkRecordsExistence(Request $request) 
	{
		if(MfContact::whereBetween('created_at',[ $request->from, $request->to ])->count() >= 1)
			return H_Response(200, 'success', 'Se han encontrado contactos');
		
		return H_Response(200, 'not-found', 'No se han encontrado contactos');	
	}

	public function	exportRecords(Request $request, $from, $to)
	{

		$items = MfContact::whereBetween('created_at', 
            [Carbon::createFromFormat('D M d Y H:i:s e+', $from), Carbon::createFromFormat('D M d Y H:i:s e+', $to)] )
			->get();

		if($items->isEmpty()) {
			return redirect('/vadmin/mfcontacts');
		} 

		Carbon::setLocale('es');
			
		$filename = 'Mensajes-recibidos';
		
		try {
			$filename = 'Listado-de-registros-'. Carbon::createFromFormat('D M d Y H:i:s e+', $from)->format('Y')
			. '-' .Carbon::createFromFormat('D M d Y H:i:s e+', $from)->format('M');
		}
		catch (\Exception $e)
		{ 
			// Use default filename
		}
		

		return Excel::download(new MfContactExport($items), $filename.'.xlsx');
	}
		

    public function destroy($ids)
    {
		$op = $this->GenericDestroy($ids, 'MfContact');

		if($op == 'success')
			return H_Response(200, 'success', 'Contacto eliminado correctamente');
		else
			return H_Response(200, 'error', 'Error al eliminar contactos: ' . $op);
    }

}
