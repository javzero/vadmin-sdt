<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use \App\Models\Post;
Use Image;
use File;
use DB;
use App\Traits\CrudTrait;

class PostController extends Controller
{
    use CrudTrait;

    public function index(Request $request)
    {
        if($request->ajax())
		{
			
			$pagination = $this->setResultsPerPage($request->resultsPerPage);

			try
			{
				$items = Post::select([
					'id',
					'title',
					'lang',
					'slug',
					'status',
					'active',
					'featured_image', 
					'publish_at', 
					'publish_end', 
					])
					->search($request->search)
					->order($request->orderBy, $request->order)
					->paginate($pagination);
					// ->toSql()

			}
			catch (\Exception $e)
			{
				return H_Response(200, 'error', 'Falla todo: '. $e->getMessage());
			}

			$data = $this->makeData($items);
			
			return $data;
		}
        return view('vadmin.posts.posts');

    }

	public function create()
	{	
		$tags = \App\Models\PostTag::all()->toArray();
		return view('vadmin.posts.post-create', compact('tags'));
	}
	

	public function edit(Request $request, $id)
	{	
		if($request->ajax())
		{
			$item = Post::where('id', $id)
				->with('tags')
				->first();

			return json_encode(['item' => $item]);
		}
		
		$tags = \App\Models\PostTag::all()->toArray();
		
		return view('vadmin.posts.post-create', compact('tags'));
	}
	
	// public function edit(Request $request, $id)
	// {	
	// 	if($request->ajax())
	// 	{
	// 		$item = Article::where('id', $id)
	// 			->with('category')
	// 			->with('social')
	// 			->with('logistic')
	// 			->with('digitalCapital')
	// 			->with('country')->with('city')->with('state')
	// 			->first();

	// 		return json_encode(['item' => $item]);
	// 	}
		
	// 	$logistics = \App\Models\Logistic::pluck('name', 'id');
	// 	$socialNetworks = \App\Models\Social::pluck('name', 'id');
	// 	$digitalCapitals = \App\Models\DigitalCapital::pluck('name', 'id');
	// 	$categories = \App\Models\ArticleCategory::pluck('name', 'id');
	// 	return view('vadmin.articles.articles-create', compact('socialNetworks', 'logistics','digitalCapitals', 'categories'));
	// }


    public function save(Request $request)
    {	
		// dd($request->all());

		try {
			DB::beginTransaction();
			
			// json_decode($request->item)->title
			$data = json_decode($request->item);
			
			
			$image = $request->image;

		
			$imgPath = public_path("images/site/posts/");

			// Check if directoy exists:
			if(!is_dir($imgPath)) 
			{
				return H_Response(200, 'error', 'No se ha creado el directorio para almacenar posts. El mismo debe se: public/images/web/posts/', ''); 
			}

			// $successMessage = [];
			// if($request->id != null) 
			// {
			// 	$item = Post::where('id', $request->id)->first();
			// 	$successMessage[0] = 'Post actualizado';
			// }
			// else
			// {
			// 	$item = new Post();
			// 	$successMessage[0] = 'Post creado';
			// }

			$successMessage = $this->successMessage('Post', $data->id);
			

			$item = Post::firstOrNew(['id' => $data->id]);

			$validation = $this->validator((array) $data, $item->id);
			
			if($validation->fails())
				return H_Response(200, 'error', $validation->errors());
	
			if(!$data->active)
				$item->active = 0;
			else
				$item->active = $data->active;

			if(!$data->order)
				$item->order = 0;
			else
				$item->order = $data->order; 


			$item->author_id = auth()->user()->id;
			$item->lang = $data->lang;
			$item->title = $data->title;
			$item->content = $data->content;
			
			$item->slug = $data->slug;
								
				
			if($item->save() && $image) 
			{
				try 
				{
					$extension = '.jpg';
					$filename = 'post-img-'.$item->id;
					$img = \Image::make($image);
					$item->featured_image = $filename.$extension;
					$img->encode('jpg', 80)->save($imgPath . $filename . $extension);    
					$item->save();
					
				}
				catch (\Exception $e) 
				{
					dd($e->getMessage()); 
				}  

			}

			$tags = [];
				
			if($data->tags) {
				$item->tags()->detach();
				
				foreach ($data->tags as $tag) {
					array_push($tags, $tag->id);
				}
			}

			$item->tags()->sync($tags);
				// $item->published_on = $this->parseDate($request->published_on);
				// $item->expire_at = $this->parseDate($item->expire_at);
				// dd($item);
			$item->save();
					
			DB::commit();
			
			return H_Response(200, 'success',  $successMessage, $item->id);

		}
		catch (\Exception $e)
		{
			// dd($e);
			return H_Response(200, 'error', 'Falla la carga del post: '. $e->getMessage());
		}

	return H_Response(200, 'success', $successMessage, $item->id);

}


	public function parseDate($date)
	{
		if($date != '' || $date != null) 
		{
			$d = explode('/', $date);
			return $d[2].'-'.$d[1].'-'.$d[0];	
		} 
		else 
		{
			return null;
		}
	}
	

	/*
	|--------------------------------------------------------------------------
	| Validator
	|--------------------------------------------------------------------------
	*/

	public function validator(array $data, $itemId = null)
	{

		$fields = [
			'title'   => 'required',
			'content' => 'required',
			'lang'	  => 'required'
		];

		// dd($data);
		// if(isset($itemId)) // If is updating exclude updated item
		// {
		// 	$fields['customer_email'] = 'required|unique:articles,customer_email,' . $itemId;
		// } else {
		// 	$fields['customer_email'] = 'required|unique:articles,customer_email';
		// }

		$messages = [
			'title.required'   => 'Debe ingresar un título',
			'content.required' => 'Debe ingresar el contenido del post',
			'lang.required'    => 'Debe seleccionar un idioma para el post'
		];

		$validation = Validator::make($data, $fields, $messages);
		
		return $validation;
	}

	public function destroy($ids)
    {		
		try 
		{
			// $imagesPath = '/images/articles/';
			$itemsIds = json_decode('[' . str_replace("'", '"', $ids) . ']', true);

			if(count($itemsIds) == 1)
				$successMessage = 'Post Eliminado';
			else
				$successMessage = 'Posts Eliminados';
			// $s = [];
			foreach ($itemsIds as $id) {
				$item = Post::find($id);
				$item->tags()->detach();
				if($item->featured_image)
					File::Delete(public_path('images/site/posts/' . $item->featured_image));
				$item->delete();

			};
				
			return H_Response(200, 'success', $successMessage);
		} 
		catch (\Exception $e) 
		{
			return H_Response(500, 'error', $e->getMessage());
		}

	}

}
