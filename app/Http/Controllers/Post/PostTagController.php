<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\PostTag;
use App\Traits\CrudTrait;

class PostTagController extends Controller
{
    use CrudTrait;
   
    public function index(Request $request)
    {
        if($request->ajax())
		{
			$pagination = $this->setResultsPerPage($request->resultsPerPage);

			try
			{
				$items = PostTag::search($request->search)
					->order($request->orderBy, $request->order)
					->paginate($pagination);             

			}
			catch (\Exception $e)
			{
				return H_Response(200, 'error', 'Falla todo: '. $e->getMessage());
			}

			$data = $this->makeData($items);
			
			return $data;
		}
        return view('vadmin.posts.post-tags');

    }


    public function save(Request $request)
	{

		$successMessage = 'Operación realizada correctamente';

		if($request->id != null) 
		{
			$item = PostTag::where('id', $request->id)->first();
			$successMessage = 'Tag actualizado';
		}
		else
		{
			$item = new PostTag();
			$successMessage = 'Tag creado';
		}

		try {
			$validation = $this->validator($request->all(), $request->id);
			
			if($validation->fails())
				return H_Response(200, 'error', $validation->errors());
			

			$item->name = $request->name;
			$item->name_br = $request->name_br;
			$item->name_display = $request->name . ' | ' . $request->name_br;
			$item->save();

			return H_Response(200, 'success', $successMessage);
		} 
		catch (\Exception $e) 
		{
			return H_Response(200, 'error', 'Falla creación del tag: '. $e->getMessage());
		}
		
	}
	
	public function validator(array $data, $itemId = null)
    {
		if(isset($itemId)) // If is updating exclude updated item
		{
			$fields = [
				'name' => 'required|unique:tags,name,'.$itemId,
			];
		}
		else // Creating new item
		{

			$fields = [
				'name' => 'required|max:60|unique:tags,name',
			];
		}

		$messages = [
			'name.required'    => 'Debe ingresar un nombre',
			'name.unique'      => 'El tag ingresado ya existe en la base de datos',
		];


		$validation = Validator::make($data, $fields, $messages);
		
		return $validation;
	}

	
	public function destroy($ids)
    {
		$itemsIds = json_decode('[' . str_replace("'", '"', $ids) . ']', true);
				
		foreach ($itemsIds as $id) {
			try 
			{
				$item = PostTag::findOrFail($id);
				// $item->posts()->detach();
				$item->delete();
				
			}
			catch (\Exception $e) 
			{
				dd($e->getMessage());
				return H_Response(200, 'error', $e->getMessage());
				// return $e->getMessage();
			} 
		}
		return H_Response(200, 'success', 'Tags eliminados');

    }

}
