<?php

namespace App\Http\Controllers;

use App\Models\Social;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Traits\CrudTrait;

class SocialNetworkController extends Controller
{   
    use CrudTrait;

    public function index(Request $request)
    {		
		if($request->ajax())
		{
			$pagination = $this->setResultsPerPage($request->resultsPerPage);

			try
			{
				$items = Social::search($request->search)
					->order($request->orderBy, $request->order)
					->paginate($pagination);
			}
			catch (\Exception $e)
			{
				return H_Response(200, 'error', 'Falla todo: '. $e->getMessage());
			}

			$data = $this->makeData($items);
			
			return $data;
		}
		else
		{
			return view('vadmin.articles.social-networks');
		}
	}

	
	public function save(Request $request)
	{
		$successMessage = 'Operación realizada correctamente';

		try {
            
            
            $validation = $this->validator($request->all(), $request->id);

			if($validation->fails())
                return H_Response(200, 'error', $validation->errors());
            
            $item = Social::firstOrNew(['id' => $request->id]);
                
			$item->name = $request->name;
			$item->save();

            return H_Response(200, 'success', $successMessage);
            
		} 
		catch (\Exception $e) 
		{
			return H_Response(200, 'error', 'Falla creación del item: '. $e->getMessage());
		}
		
	}
	
	public function validator(array $data, $itemId = null)
    {

		if(isset($itemId)) // If is updating exclude updated item
		{
			$fields = [
                'name' => 'required|unique:social,name'
			];
            // |unique:digitalcapitals,name,'.$itemId,
		}
		else // Creating new item
		{

			$fields = [
                'name' => 'required|unique:social,name'
			];
            // |max:60|unique:digitalcapitals,name',
		}

		$messages = [
			'name.required'    => 'Debe ingresar un nombre',
            'name.unique'      => 'La red social ingresada ya existe en la base de datos',
		];

        $validation = Validator::make($data, $fields, $messages);
        
		return $validation;
	}

	
	public function destroy($ids)
    {
		$op = $this->GenericDestroy($ids, 'Social');

		if($op == 'success')
			return H_Response(200, 'success', 'Item eliminado');
		else
			return H_Response(200, 'error', $op);
    }

}
