<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;
use App\Ad;
Use Image;
Use File;

class AdsController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
	}

	
    public function index(Request $request)
    {
        return view('vadmin.ads');
    }

    public function getCtesad(Request $request)
    {
        return Ad::all();
    }

    public function changeStatus(Request $request)
    {

        try 
        {
            $ad = Ad::where('position', $request->position)->first();
            $ad->status = $request->status;
            $ad->save();  

            return H_Response(200, 'success', $request->status, ''); 

        }
        catch (\Exception $e) 
        {
            return H_Response(200, 'error', 'No se pudo actualizar el estado del anuncio', $e->getMessage()); 
            dd($e->getMessage()); 
        }  
    }

    public function save(Request $request)
    {
        // dd($request->all());
        if( $request->image ) 
        {
            $imgPath = public_path("images/web/ctesad/");

            // Check if directoy exists:
            if(!is_dir($imgPath)) 
            {
                return H_Response(200, 'error', 'No se ha creado el directorio para almacenar anuncios. El mismo debe se: public/images/web/ctesad/', ''); 
            }
            
            $position = $request->position;
            $extension = '.jpg';
            $filename = 'an-' . $position;

            $ad = Ad::firstOrCreate(['position' => $position]);
            $ad->position = $position;
            $ad->image = $filename.$extension;

            if($ad->save()) 
            {
                try 
                {
                    $img = \Image::make($request->image);
                    $img->encode('jpg', 80)
                        ->save($imgPath . $filename . $extension);    

                }
                catch (\Exception $e) 
		        {
                    dd($e->getMessage()); 
                }  
            }


            return H_Response(200, 'success', 'Anuncio cargado correctamente', ''); 
        }
        else 
        {
            return H_Response(200, 'error', 'No se ha incluído una imágen', ''); 
        }
    }

    public function destroy(Request $request) 
    {
        try 
        {
            $ad = Ad::where('position', $request->position)->first(); 
            $ad->delete();
            
            return H_Response(200, 'success', 'Anuncio eliminado', ''); 
        }
        catch (\Exception $e) 
        {
            return H_Response(200, 'error', 'No se ha podido eliminar el anuncio', ''); 
            dd($e->getMessage()); 
        }  

    
    }
	


}
