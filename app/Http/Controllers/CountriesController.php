<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Country;
use App\Models\State;
use App\Traits\CrudTrait;


class CountriesController extends Controller
{

	use CrudTrait;

	public function __construct()
    {
        $this->middleware('auth');
	}


    public function index(Request $request)
    {	
		if($request->ajax())
		{
			// dd($request->all());
			if(isset($_GET['get']) && $_GET['get'] == 'all')
				return State::all();
				
			// Default
			$pagination = $this->setResultsPerPage($request->resultsPerPage);
			$order = 'asc';
			
			if(isset($request->order))
				$order = $request->order;
			
			$orderBy = $this->orderBy($request->orderBy, 'id');		
			
			$items = Country::search($request->search)->orderBy($orderBy, $order)->paginate($pagination);

			$data = $this->makeData($items);
			
			return $data;
		} else {
			dd('No ajax');
		}
	}
	

	public function getCountries($id = null) 
	{
		if($id != 0)
			return Country::where('id', $id)->first();
			
		return Country::all();
	}
	

	public function save(Request $request)
	{
        try {
            
            $validation = $this->validator($request->all(), $request->id);

			if($validation->fails())
                return H_Response(200, 'error', $validation->errors());
            
            $item = Country::firstOrNew(['id' => $request->id]);
                
			$item->name = $request->name;
			$item->save();

            return H_Response(200, 'success', 'País creado');
            
		} 
		catch (\Exception $e) 
		{
			return H_Response(200, 'error', 'Falla creación del item: '. $e->getMessage());
		}
		
	}

	
	public function destroy($ids)
    {
		$op = $this->GenericDestroy($ids, 'Countries');

		if($op == 'success')
			return H_Response(200, 'success', 'País eliminada');
		else
			return H_Response(200, 'error', $op);
    }

		
	public function validator(array $data, $itemId = null)
    {
	
		if(isset($itemId)) // If is updating exclude updated item
		{
			$fields = [
				'name' => 'required|max:60|unique:countries,name,'.$itemId,
			];
		}
		else // Creating new item
		{
			$fields = [
				'name' => 'required|max:60|unique:countries,name'
			];
		}

		$messages = [
			'name.required'  => 'Debe ingresar un nombre',
			'name.unique'    => 'El país ingresado ya existe en la base de datos',
		];

		$validation = Validator::make($data, $fields, $messages);
		
		return $validation;
	}

}
