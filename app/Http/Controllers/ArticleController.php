<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Article;
use App\Exports\ArticleExport;

use App\Traits\CrudTrait;
use App\Traits\DataProcessing;

// use App\Models\ArticleImage;
// use App\Models\ArticleCategory;
// use App\Models\DigitalCapital;
// Use Image;
// Use File;

use Carbon\Carbon;
use Excel;
use DB;

class ArticleController extends Controller
{
	use CrudTrait, DataProcessing;

	public function __construct()
    {
        $this->middleware('auth');
	}

	
    public function index(Request $request)
    {

		if($request->ajax())
		{		
			// dd($request->all());
			$pagination = $this->setResultsPerPage($request->resultsPerPage);

			try
			{

				$items = Article::select([
					'id',
					'name', 
					'title', 
					'country_id',
					'category_id',
					'customer_email',
					'domain_name',
					'value_raw_data',
					'net_income_actual_year', 
					'net_income_last_year',
					'total_followers',
					'total_subscriptors',
					'male_traffic',
					'female_traffic',
					'business_age',
					'business_value',
					'business_value_manual',
					'business_value_local',
					'status',
					'active',
					'position'
					])
					->vadminSearch($request->search)
					->fromCountry($request->country)
					->order($request->orderBy, $request->order)
					->with('social')
					->with('category')
					->with('logistic')
					->with('digitalCapital')
					->with('country')->with('city')->with('state')
					->paginate($pagination);
					
			}
			catch (\Exception $e)
			{
				return H_Response(200, 'error', 'Falla todo: '. $e->getMessage());
			}

			$data = $this->makeData($items);
			$data['currency'] = \App\Models\Currency::all();

			return $data;
		}

		// $socialNetworks = \App\Models\Social::pluck('name', 'id');
		$countries = \App\Models\Country::select(['char_id', 'name'])->get();
		return view('vadmin.articles.articles', compact('countries'));
	}
	

	public function getArticle($id)
	{
		$item = Article::where('id', $id)->with('images')->with('category')->with('social')->first();	
		return json_encode($item);
	}

	public function show($id) 
	{
		$item = Article::where('id', $id)
			->with('category')
			->with('social')
			->with('logistic')
			->with('digitalCapital')
			->with('country')->with('city')->with('state')
			->first();

		$logistics = \App\Models\Logistic::pluck('name', 'id');
		$socialNetworks = \App\Models\Social::pluck('name', 'id');
		$digitalCapitals = \App\Models\DigitalCapital::pluck('name', 'id');
		$businessTypes = \App\Models\BusinessType::pluck('name', 'id');
		$categories = \App\Models\ArticleCategory::pluck('name', 'id');
		$showMode = 1;
		return view('vadmin.articles.articles-create', 
		compact('socialNetworks', 'logistics', 'digitalCapitals', 'categories', 'showMode', 'businessTypes'));
	}  

	public function create()
	{	
		$logistics = \App\Models\Logistic::pluck('name', 'id');
		$socialNetworks = \App\Models\Social::all();
		$digitalCapitals = \App\Models\DigitalCapital::all();
		$businessTypes = \App\Models\BusinessType::pluck('name', 'id');
		$categories = \App\Models\ArticleCategory::get();
		return view('vadmin.articles.articles-create', 
		compact('socialNetworks', 'logistics', 'digitalCapitals', 'categories', 'businessTypes'));
	}
	

	public function edit(Request $request, $id)
	{	
		if($request->ajax())
		{
			$item = Article::where('id', $id)
				->with('category')
				->with('social')
				->with('logistic')
				->with('digitalCapital')
				->with('country')->with('city')->with('state')
				->first();

			return json_encode(['item' => $item]);
		}
		
		$logistics = \App\Models\Logistic::pluck('name', 'id');
		$socialNetworks = \App\Models\Social::all();
		$digitalCapitals = \App\Models\DigitalCapital::all();
		$businessTypes = \App\Models\BusinessType::pluck('name', 'id');
		$categories = \App\Models\ArticleCategory::get();
		return view('vadmin.articles.articles-create', compact('socialNetworks', 'logistics','digitalCapitals', 'categories', 'businessTypes'));
	}


    public function save(Request $request)
    {
		// dd($request->all());
		try {
			$successMessage = [];
			if($request->id != null) 
			{
				$item = Article::where('id', $request->id)->first();
				$successMessage[0] = 'Artículo actualizado';
			}
			else
			{
				$item = new Article();
				$successMessage[0] = 'Artículo creado';
			}

			$validation = $this->validator($request->all(), $item->id);

			if($validation->fails())
				return H_Response(200, 'error', $validation->errors());
			
			$digitalCapital = [];

			foreach($request->digital_capital as $dc)
			{
				array_push($digitalCapital, $dc['id']);
			}

			$businessValue = [];
			try 
			{

				$valuationData  = [
					'business_age' => $request->business_age,
					'working_hours' => $request->working_hours,
					'views_per_month' => $request->views_per_month,
					'organic_views_per_month' => $request->organic_views_per_month,
					'total_followers' => $request->total_followers,
					'total_subscriptors' => $request->total_subscriptors,
					'country_id' => $request->country_id,
					'net_income_actual_year' => $request->net_income_actual_year,
					'net_income_last_year' => $request->net_income_last_year,
					'digitalCapital' => $digitalCapital,
					'logistic_id' => $request->logistic_id,
					'business_value_manual' => $request->business_value_manual,
					'active_users' => $request->active_users,
					'total_downloads' => $request->total_downloads,
					'business_type_id' => $request->business_type_id
				];
				$businessValue = $this->valuateBusiness($valuationData);
			} 
			catch (\Exception $e) 
			{
				// If there is an error on the valuateBusiness algorithm reset the value to 0 to prevent crash on save.
				$businessValue = [
					"uss" => 0,
					"local" => 0
				];
				dd($e->getMessage());
			}

			DB::beginTransaction();
			// dd();
				$item->fill($request->all());
				if($request->position == null || $request->position == '')
					$item->position = 99999;
				$item->published_on = $this->parseDate($request->published_on);
				$item->business_value = $businessValue['uss'];
				$item->business_value_local = $businessValue['local'];
				$item->value_raw_data = json_encode($businessValue);
				$item->expire_at = $this->parseDate($item->expire_at);
				// dd($item);
				$item->save();
				
				$item->digitalCapital()->detach();
				foreach($request->digital_capital as $digitalCapital)
				{	
					$item->digitalCapital()->attach($digitalCapital['id']);
				}


				$item->social()->detach();
				foreach($request->social as $socialNetwork) 
				{
					$item->social()->attach(
						$socialNetwork['id'], 
						[ 
							// 'followers' => $socialNetwork['pivot']['followers'],
							// 'subscriptors' => $socialNetwork['pivot']['subscriptors'],
						  	'account_url' => $socialNetwork['pivot']['account_url'],
						]);
				}
				
			DB::commit();


		}
		catch (\Exception $e)
		{
			// dd($e);
			return H_Response(200, 'error', 'Falla la carga del articulo: '. $e->getMessage());
		}
		
		if($item->active == 1)
			$successMessage[1] = "El artículo tiene estado activo y está saliendo en los listados.";
		else
			$successMessage[1] = "El artículo está inactivo, para que salga en los listados debe activarlo.";

		return H_Response(200, 'success', $successMessage, $item->id);

	}

	public function updatePosition(Request $request)
	{
		try {
			$item = Article::findOrFail($request->id);
			
			$item->position = 99999;
			if($request->position != null &&  $request->position != '')
				$item->position = $request->position;

			$item->save();
			return H_Response(200, 'success', 'Posición actualizada !', $item->id);
			
		} catch (\Exception $e) {
			return H_Response(200, 'error', 'Hubo un error al intentar actualizar la posición. <br><br> (Error: ' . $e->getMessage() . ')');
		}
	}

	public function parseDate($date)
	{
		if($date != '' || $date != null) 
		{
			$d = explode('/', $date);
			return $d[2].'-'.$d[1].'-'.$d[0];	
		} 
		else 
		{
			return null;
		}
	}
	

	/*
	|--------------------------------------------------------------------------
	| Validator
	|--------------------------------------------------------------------------
	*/
   
	public function validator(array $data, $itemId = null)
    {
		$fields = [
			'customer_firstname'              => 'required',
			'customer_lastname'               => 'required',
			'customer_phone'                  => 'required',
			'country_id' 					  => 'required',
			'total_followers|nullable'        => 'integer',
			'total_subscriptors|nullable'     => 'integer',
			'views_per_month'                 => 'integer',
			'female_traffic|nullable'         => 'integer',
			'male_traffic|nullable'           => 'integer',
			'business_age|nullable'           => 'integer',
			'working_hours|nullable'          => 'integer',
			'fc_actual_year|nullable'         => 'integer',
			'fc_last_year|nullable'           => 'integer',
			'net_income_actual_year|nullable' => 'integer',
			'net_income_last_year|nullable'   => 'integer',
		];

		// dd($data);

		$fields['customer_email'] = 'required';
		
		// if(isset($itemId)) // If is updating exclude updated item
		// {
		// 	$fields['customer_email'] = 'required|unique:articles,customer_email,' . $itemId;
		// } else {
		// 	$fields['customer_email'] = 'required|unique:articles,customer_email';
		// }

		$messages = [
			'customer_email.unique'          => 'El email ya está en la base de datos',
			'customer_email.required'        => 'Debe ingresar un email',
			'customer_firstname.required'    => 'Debe ingresar el nombre del cliente',
			'customer_lastname.required'     => 'Debe ingresar el apellido del cliente',
			'customer_phone.required'        => 'Debe ingresar un teléfono de contacto',
			'total_subscriptors.integer'     => 'Solo se aceptan números',
			'total_followers.integer'        => 'Solo se aceptan números',
			'views_per_month.integer'        => 'Solo se aceptan números',		
			'female_traffic.integer'         => 'Solo se aceptan números',
			'male_traffic.integer'           => 'Solo se aceptan números',
			'business_age.integer'           => 'Solo se aceptan números',
			'working_hours.integer'          => 'Solo se aceptan números',
			'fc_actual_year.integer'         => 'Solo se aceptan números',
			'fc_last_year.integer'           => 'Solo se aceptan números',
			'net_income_actual_year.integer' => 'Solo se aceptan números',
			'net_income_last_year.integer'   => 'Solo se aceptan números',
			'country_id.required'            => 'Debe ingresar un país'
		];

		$validation = Validator::make($data, $fields, $messages);

		return $validation;
	}
	
	/*
	|--------------------------------------------------------------------------
	| Export
	|--------------------------------------------------------------------------
	*/

	public function checkArticlesExistence(Request $request) 
	{
		if(Article::whereBetween('created_at',[ $request->from, $request->to ])->count() >= 1)
			return H_Response(200, 'success', 'Se han encontrado artículos');
		
		return H_Response(200, 'not-found', 'No se han encontrado artículos');	
	}

	public function	exportArticles(Request $request, $from, $to)
	{

		$items = Article::select([
			'id',
			'customer_firstname',
			'customer_lastname',
			'customer_phone',
			'customer_email',
			'country_id',
			'state_id',
			'city_id',
			'category_id',
			'domain_name',
			'title',
			'name' ,          
			'views_per_month',
			'organic_views_per_month',
			'total_followers',
			'total_subscriptors',
			'montly_subscriptors',
			'male_traffic',
			'female_traffic',
			'digital_capital_id',
			'working_hours',
			'business_age',
			'logistic_id',
			'fc_actual_year',
			'fc_last_year',
			'net_income_actual_year',
			'business_value',
			'business_value_manual',
			'business_value_local',
			'net_income_actual_year',
			'net_income_last_year',
			'status',
			'active',
			'created_at'
			])->whereBetween('created_at',[ Carbon::createFromFormat('D M d Y H:i:s e+', $from), Carbon::createFromFormat('D M d Y H:i:s e+', $to)])
			->get();

		if($items->isEmpty()) {
			return redirect('/vadmin/articulos');
		} 


		Carbon::setLocale('es');
			
		$filename = 'Listado-de-articulos';
		
		try {
			$filename = 'Listado-de-articulos-'. Carbon::createFromFormat('D M d Y H:i:s e+', $from)->format('Y')
			. '-' .Carbon::createFromFormat('D M d Y H:i:s e+', $from)->format('M');
		}
		catch (\Exception $e)
		{ 
			// Use default filename
		}
		

		return Excel::download(new ArticleExport($items), $filename.'.xlsx');
	}
		
	/*
	|--------------------------------------------------------------------------
	| Destroy
	|--------------------------------------------------------------------------
	*/

	public function destroy($ids)
    {		
		try 
		{
			// $imagesPath = '/images/articles/';
			$itemsIds = json_decode('[' . str_replace("'", '"', $ids) . ']', true);

			if(count($itemsIds) == 1)
				$successMessage = 'Artículo Eliminado';
			else
				$successMessage = 'Artículos Eliminados';
			// $s = [];
			foreach ($itemsIds as $id) {
				$item = Article::find($id);
				
			// 		if($item->images)
			// 		{
			// 			foreach($item->images as $image)
			// 			{
			// 				$image->delete();
			// 				File::Delete(public_path($imagesPath . $image->name));
			// 				File::Delete(public_path($imagesPath . $image->thumb));
			// 			}
			// 		}

				$item->delete();

			};
				
			return H_Response(200, 'success', $successMessage);
		} 
		catch (\Exception $e) 
		{
			return H_Response(500, 'error', $e->getMessage());
		}

	}
	


}
