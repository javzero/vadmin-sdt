<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Article;
use App\Models\ArticleCategory;

use App\Traits\CrudTrait;

class ArticleController extends Controller
{
	use CrudTrait;

	public function getArticles(Request $request)
	{		
		// dd($request->all());
		// if($request->ajax())
		// {		
			// $pagination = $this->setResultsPerPage($request->resultsPerPage);

			// $items = Article::search($request->search)
			// 	->order($request->orderBy, $request->order)
			// 	->with('social')
			// 	->with('category')
			// 	->with('logistic')
			// 	->with('digitalCapital')
			// 	->with('country')->with('state')->with('city')
			// 	->paginate(10);

			$items = Article::search($request->search)
					->fromCountry($request->country_id)
					->order($request->orderBy, $request->order)
					->with('country')
					->with('category')
					->with('state')
					->paginate(10);
			$items = Article::all();
			dd($items);
			// try
			// {
			// }
			// catch (\Exception $e)
			// {
			// 	return H_Response(200, 'error', 'Falla todo: '. $e->getMessage());
			// }

			$data = $this->makeData($items);
			// dd($data);
			return $data;
		// }

	}


	// public function getArticles(Request $request, $query)
	// {	

	// 	switch ($query) {
	// 		case 'latest':
	// 			return Article::latest()->with('country')->with('state')->take(4)->get();
	// 			break;
			
	// 		default:
	// 		return Article::all();
	// 			break;
	// 	};
	// }


	public function getLastArticles()
	{
		return $this->Article::get()->take(4);
	}




}