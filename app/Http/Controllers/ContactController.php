<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\CrudTrait;
use Carbon\Carbon;

use App\Models\Contact;
use App\Exports\ContactExport;

use Excel;

class ContactController extends Controller
{

    use CrudTrait;

    public function __construct()
    {
        $this->middleware('auth');
	}

    public function index(Request $request)
    {	
        if($request->ajax())
		{
			$pagination = $this->setResultsPerPage($request->resultsPerPage);
			
			$items = Contact::search($request->search)->order($request->orderBy, $request->order)->with('country')->paginate($pagination);

			$data = $this->makeData($items);
			
			return $data;
		}

        return view('vadmin.contacts');
    }


    /*
	|--------------------------------------------------------------------------
	| Export
	|--------------------------------------------------------------------------
	*/

	public function checkContactsExistence(Request $request) 
	{
		if(Contact::whereBetween('created_at',[ $request->from, $request->to ])->count() >= 1)
			return H_Response(200, 'success', 'Se han encontrado mensajes');
		
		return H_Response(200, 'not-found', 'No se han encontrado mensajes');	
	}

	public function	exportContacts(Request $request, $from, $to)
	{

		$items = Contact::whereBetween('created_at', 
            [Carbon::createFromFormat('D M d Y H:i:s e+', $from), Carbon::createFromFormat('D M d Y H:i:s e+', $to)] )
			->get();

		if($items->isEmpty()) {
			return redirect('/vadmin/contacts');
		} 

		Carbon::setLocale('es');
			
		$filename = 'Mensajes-recibidos';
		
		try {
			$filename = 'Listado-de-mensajes-'. Carbon::createFromFormat('D M d Y H:i:s e+', $from)->format('Y')
			. '-' .Carbon::createFromFormat('D M d Y H:i:s e+', $from)->format('M');
		}
		catch (\Exception $e)
		{ 
			// Use default filename
		}
		

		return Excel::download(new ContactExport($items), $filename.'.xlsx');
	}
		

    public function destroy($ids)
    {
		$op = $this->GenericDestroy($ids, 'Contact');

		if($op == 'success')
			return H_Response(200, 'success', 'Mensaje eliminado correctamente');
		else
			return H_Response(200, 'error', 'Error al eliminar mensajes: ' . $op);
    }

}
