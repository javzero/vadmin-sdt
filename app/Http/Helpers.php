<?php
/*
|--------------------------------------------------------------------------
| HELPERS
|--------------------------------------------------------------------------
*/

/**
 * Return a Json Response
 *
 * @return resoponse
 */
function H_Response($code = 200, $status = null, $message = null, $data = null, $action = null) 
{
	return response()->json([
		'status'  => $status,
		'message' => $message,
		'data'    => $data,
		'action'  => $action
	], $code);
}

function H_Jsondd($data) 
{
	return response()->json([
		'status' => 'error',
		'type' => 'Json Dump',
		'data' => $data,
	], 200);
}

function twoDecimals($number, $precision = 2) {

    // Zero causes issues, and no need to truncate
    if (0 == (int)$number) {
        return $number;
    }

    // Are we negative?
    $negative = $number / abs($number);

    // Cast the number to a positive to solve rounding
    $number = abs($number);

    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);

    // Run the math, re-applying the negative value to ensure
    // returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}

function calcPercent($value, $percent) {
    return $value * $percent / 100;
}

function formatMoney($value) {
    return number_format($value, 0, '', '.');
    
    // return clean_num(number_format(floatval($value), 0, ',', '.' ));
    
}


function clean_num( $num ){
    $pos = strpos($num, '.');
    if($pos === false) { // it is integer number
        return $num;
    }else{ // it is decimal number
        return rtrim(rtrim($num, '0'), ',');
    }
}