<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Mail\SendMail;

class TestEmailProcessMainForm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:TestEmailProcessMainForm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test email processMainForm()';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $subject = "TestEmailProcessMainForm";
        $item = \App\Models\Article::first();

        Mail::to('landrade-f33108@inbox.mailtrap.io')->queue(new SendMail($subject, 'business-value-manager', $item));
        Mail::to('landrade-f33108@inbox.mailtrap.io')->queue(new SendMail($subject, 'main-form-for-client', $item));
        
        return 0;
    }
}
