<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Mail\SendMail;

class TestEmailMainContactForm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:TestEmailMainContactForm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test del formulario principal del sitio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $req = collect([
            'lang' => 'ar',
            'name' => 'Nombre del cliente',
            'email' => env('MAIL_TRAP_EMAIL'),
            'message' => 'Este es el mensaje',
            'countryName' => 'Argentina',
            'phone' => '+5491158090227',
            'articleCode' => 1,
            'articleTitle' => 'Ut ullam accusantium est dicta dolorem.',
        ]);

        if($req['lang'] == 'br') {
            $subjectForClient = "Obrigado ". $req['name'] ." pelo seu contato. Você gostaria que agendássemos uma ligação?";
        } else {
            $subjectForClient = "Gracias ". $req['name'] ." por tu contacto. ¿Te gustaría que agendemos una llamada?";
        }

        $subject = "TestEmailProcessMainForm";
        
        Mail::to(env('MAIL_TRAP_EMAIL'))->send(new SendMail($subject, 'contact-main-site',  $req));
        Mail::to(env('MAIL_TRAP_EMAIL'))->send(new SendMail($subjectForClient, 'contact-main-site-for-client',  $req));


        
        return 0;
    }
}
