<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use Carbon\Carbon;
use App\Mail\SendMail;
use Log;

class SendMailMfContacs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendMailMfContacts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $mfContacts = \App\Models\MfContact::canBeContacted()
            ->where('created_at', '<', Carbon::parse('-3 hours'))->get();

        // dd($mfContacts);

        if($mfContacts->count() == 0)
            return;
        
        Log::info("-----------------------------------");
        Log::info("Sending Mails to MfContacts" . " Date: " . date("d-m-Y H:i:s"));
        Log::info("-----------------------------------");

        foreach($mfContacts as $contact) 
        {
            // For Test
            if(env('USE_DEV_MAIL'))  
                $customerEmail = env('MAIL_TRAP_EMAIL');
            else
                $customerEmail = $contact->customer_email;

            if($contact->country_id == 'BR') 
            {
                $subject = $contact->customer_firstname . " obrigado por nos contatar!";
            } 
            else 
            {
                $subject = $contact->customer_firstname . " gracias por contactarte con nosotros!";
            }
            
            try {
                Mail::to($customerEmail)->queue(new SendMail($subject, 'mfcontacts', $contact));
                $contact->contacted = 1;
                $contact->save();
                Log::info("Mail to: " . $contact->customer_email . " - OK!");
            } 
            catch (\Exception $e) 
            {
                Log::info("Error sending mail MFContact");
                Log::info("ID: " . $contact->id. " | Email: " . $contact->customer_email);
                Log::info("Error: ");
                Log::info($e->getMessage());
            }

        }
        Log::info("-----------------------------------");
        
        return 0;
    }
}
