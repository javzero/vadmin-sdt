<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "cities";

    protected $primaryKey = 'id';

    protected $fillable = [ 'state_id', 'country_id', 'name' ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'char_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }


    public function scopeSearch($query, $search = '')
    {
        return $query->where('name', 'LIKE', '%' . $search . '%')
            ->orWhereHas('state', function ($query) use($search){
                $query->where('name', 'LIKE', '%' . $search . '%');
            })
            ->orWhereHas('country', function ($query) use($search){
                $query->where('name', 'LIKE', '%' . $search . '%');
            });
    }

}
