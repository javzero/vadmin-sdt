<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DigitalCapital extends Model
{
    
    protected $table = "digitalcapitals";

    protected $fillable = [ 'name', 'name_br', 'value' ];

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    public function scopeSearch($query, $term)
    {
        if($term != null)
            return $query->where('name', 'LIKE', '%'. $term .'%');
            
    }

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }

}
