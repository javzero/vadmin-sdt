<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    protected $table = "tags";

    protected $fillable = [ 'name', 'name_br', 'name_display'];

	public function posts(){
		return $this->belongsToMany(Post::class);
        // 'post_tag', 'post_id', 'tag_id'  
	}

	public function scopeSearch($query, $name)
	{
		return $query->where('name', 'LIKE', "%$name%")
					 ->orWhere('id', 'LIKE', "%$name%");
	}

	public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }
    
	public function scopeSearchTag($query, $name)
    {
    	return $query->where('name','=', $name);
    }	

	

}	
