<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'country_id', 'manual_value', 'api_value' ];

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'char_id', 'country_id');
    }

    public function scopeSearch($query, $searchTerm)
    {
        return $query->where('name', 'LIKE','%'.$searchTerm.'%');
    }
}
