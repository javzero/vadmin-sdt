<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $table = "article_categories";

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'name_br' ];

    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id');
    }
}
