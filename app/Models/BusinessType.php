<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    protected $table = "business_types";

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'code' ];

    public function articles()
    {
        return $this->hasMany( Article::class );
    }

}