<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CrudTrait;

class Contact extends Model
{

    use CrudTrait;

    protected $table = 'contacts';

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'phone', 'email', 'message', 'article_id', 'country_id' ];

    public static function saveContact($data)
    {
        // dd($data);
        $item = new \App\Models\Contact();
        $item->fill($data);

        if(isset($data['articleCode']))
            $item->article_id = $data['articleCode'];
        else
            $item->article_id = null;
        
        try {
            $item->save();
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        
    }

    public function scopeSearch($query, $term)
    {
        $query->where(function ($query) use ($term) {
            $query->orWhere('name', 'LIKE', '%'. $term .'%');
            $query->orWhere('phone', 'LIKE', '%'. $term .'%');
            $query->orWhere('email', 'LIKE', '%'. $term .'%');
            $query->orWhere('message', 'LIKE', '%'. $term .'%');
        });
    }

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }


}
