<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QAlgorithm extends Model
{      
    protected $table = "qalgorithm";

    protected $fillable = [
        'name', 'db_name', 'integer'
    ];

}
