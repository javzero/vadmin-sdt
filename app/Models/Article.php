<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";

    protected $fillable = [
        'lang', 'customer_firstname', 'customer_lastname', 'customer_phone', 'customer_email', 'country_id', 'state_id', 'city_id', 
        'business_type_id', 'category_id', 'title', 'name', 'description', 'domain_name', 'views_per_month', 'organic_views_per_month', 
        'total_followers', 'total_subscriptors', 'montly_subscriptors', 'male_traffic', 'female_traffic', 'digital_capital_id',
        'working_hours', 'business_age', 'logistic_id', 'fc_actual_year', 'fc_last_year', 'business_value', 'business_value_local',
        'business_value_manual', 'net_income_actual_year', 'net_income_last_year', 'active_users', 'total_downloads', 
        'published_on', 'expire_at', 'status', 'active', 'confirmed', 'position', 
    ];
    
    

    // ---------------------------------------
    // SCOPES / SEARCH
    // ---------------------------------------

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeConfirmed($query)
    {
        return $query->where('confirmed', 1);
    }
    
    public function scopeLatest($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }


    public function scopeSearch($query, $term)
    {
        $query->where(function ($query) use ($term) {
            $query->orWhere('title', 'LIKE', '%'. $term .'%');
            $query->orWhere('id', 'LIKE', '%'. $term .'%');
            $query->orWhere('name', 'LIKE', '%'. $term .'%');
            $query->orWhere('domain_name', 'LIKE', '%'. $term .'%');
            $query->orWhere('description', 'LIKE', '%'. $term .'%');
        });
    }

    public function scopeVadminSearch($query, $term)
    {
        $query->where(function ($query) use ($term) {
            $query->orWhere('title', 'LIKE', '%'. $term .'%');
            $query->orWhere('id', 'LIKE', '%'. $term .'%');
            $query->orWhere('name', 'LIKE', '%'. $term .'%');
            $query->orWhere('domain_name', 'LIKE', '%'. $term .'%');
            $query->orWhere('customer_email', 'LIKE', '%'. $term .'%');
            $query->orWhere('description', 'LIKE', '%'. $term .'%');
            $query->orWhereHas('Country', function($q) use($term){
                $q->where('name', 'LIKE', '%'. $term .'%'); 
            });
        });
    }

    public function scopeFromCountry($query, $char_id) 
    {   
        // dd($country_id);
        if($char_id == 'ALL')
            return;
        if($char_id) 
            return $query->where('country_id', $char_id);
    }

    public function scopeFromCountryByCharId($query, $char_id)
    {
        if($char_id) 
            return $query->where('country_id', $char_id);
    }

    public function scopeCategory($query, $category)
    {   
        if($category != null)
        {
            return $query->where('category_id', $category);
        }
    }

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }

    public function scopePosition($query)
    {
        return $query->orderBy('position', 'ASC');
    }

    public function currencyLocalToUss($country_id, $value = null)
    {

        $localCurrency = \App\Traits\DataProcessing::getLocalCurrencyValue($country_id);
        
        if(!$value || $localCurrency->manual_value == null &&  $localCurrency->api_value == null) 
            return 'No hay datos';

        if($localCurrency->api_value > 0)
            return twoDecimals(floatval($value) / $localCurrency->api_value);
        else
            return twoDecimals(floatval($value) / $localCurrency->manual_value);

        return 'No hay datos';
    }
    
    // ---------------------------------------
    // RELATIONS
    // ---------------------------------------

    public function social()
    {
        return $this->belongsToMany(Social::class)
            ->withPivot('followers', 'subscriptors', 'account_url');
    }

    public function digitalCapital()
    {
        return $this->belongsToMany(DigitalCapital::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'char_id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function logistic()
    {
    	return $this->belongsTo(Logistic::class);
    }

    public function category()
    {
    	return $this->belongsTo(ArticleCategory::class, 'category_id');
    }

    public function images()
    {
    	return $this->hasMany('App\Models\ArticleImage');
    }

    public function businessType()
    {
    	return $this->belongsTo('App\Models\BusinessType');
    }
    // BusinessType::class
 

}
