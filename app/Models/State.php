<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'country_id' ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'char_id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function scopeSearch($query, $search = '')
    {
        return $query->where('name', 'LIKE', '%' . $search . '%')
            ->orWhereHas('country', function ($query) use($search){
                $query->where('name', 'LIKE', '%' . $search . '%');
            });
    }

    // public function scopeSearch($query, $searchTerm)
    // {
    //     return $query->where('name', 'LIKE','%'.$searchTerm.'%');
    // }
}
