<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{   
    protected $fillable = [
        'name', 'icon'
    ];

    public function roles()
    {
        return $this->belongsToMany( Role::class );
    }

    public function scopeSearch($query, $term)
    {
        if($term != null)
            return $query->where('name', 'LIKE', '%'. $term .'%');
    }

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);
    }


}
