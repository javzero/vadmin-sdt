<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'social';

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'url' ];

    public function articles()
    {
        return $this->belongsToMany('App\Models\Article')
            ->withPivot('followers', 'subscriptors', 'account_url');
    }

    public function scopeSearch($query, $term)
    {
        if($term != null)
            return $query->where('name', 'LIKE', '%'. $term .'%');
            
    }

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }
}
