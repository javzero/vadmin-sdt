<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $primaryKey = 'id';

    protected $fillable = [ 'name', 'char_id' ];

    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function mfcontacts()
    {
        return $this->hasMany(MfContact::class);
    }

    public function scopeSearch($query, $searchTerm)
    {
        return $query->where('name', 'LIKE','%'.$searchTerm.'%');
    }
}
