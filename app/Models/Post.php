<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $primaryKey = 'id';

    protected $fillable = [ 'author_id', 'slug', 'title', 'lang', 'content', 'featured_image', 'publish_at', 'publish_end', 'status', 'active' ];

    public function tags()
    {
        return $this->belongsToMany(PostTag::class);
        // 'post_tag', 'tag_id', 'post_id'/
    }

    public function scopeSearch($query, $search = '')
    {
        return $query->where('title', 'LIKE', '%' . $search . '%')
                     ->where('id', 'LIKE', '%' . $search . '%');
    }

    public function scopeWebSearch($query, $search = '')
    {
        return $query->where('title', 'LIKE', '%' . $search . '%')
                    ->orWhere('content', 'LIKE', '%' . $search . '%')
                    ->orWhere('slug', 'LIKE', '%' . $search . '%');
    }

    public function scopeSearchByTag($query, $tagName = '')
    {
        // dd($tagName);
        if($tagName == '' ) return $query;
        
        return $query->whereHas('tags', function ($query) use($tagName){
            $query->where('name', 'LIKE', '%' . $tagName . '%');
        });
    }

    public function scopeLang($query, $lang) {
        return $query->where('lang', $lang);
    }

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
    

}
