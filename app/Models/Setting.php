<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    
    protected $fillable = [ 'id', 'email', 'email_br', 'google_analitycs', 'google_tag_manager', 
                            'site_name', 'site_description', 'site_keywords', 'site_name_br', 
                            'site_description_br', 'site_keywords_br', 'site_data' ];
}
