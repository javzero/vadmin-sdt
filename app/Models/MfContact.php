<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MfContact extends Model
{
    protected $table = 'mfcontacts';

    protected $primaryKey = 'id';

    protected $fillable = [ 'customer_firstname', 'customer_lastname', 'customer_email', 'country_id', 'state_id', 'customer_phone', 'customer_phone_area', 'customer_phone_number', 'contacted',  'user_allows_contact' ];

    public static function saveMfContact($data)
    {
        $item = new \App\Models\MfContact();
        $item->fill($data);
        $item->save();
    }
    
    public function scopeCanBeContacted($query) 
    {
        return $query->where('user_allows_contact', '1')->where('contacted', '0');
    }

    public function scopeSearch($query, $term)
    {
        $query->where(function ($query) use ($term) {
            $query->orWhere('customer_firstname', 'LIKE', '%'. $term .'%');
            $query->orWhere('customer_lastname', 'LIKE', '%'. $term .'%');
            $query->orWhere('customer_email', 'LIKE', '%'. $term .'%');
            $query->orWhere('customer_phone', 'LIKE', '%'. $term .'%');
            $query->orWhereHas('Country', function($q) use($term){
                $q->where('name', 'LIKE', '%'. $term .'%'); 
            });
        });
    }

    public function scopeOrder($query, $orderBy, $order)
    {
        if($order == null)
            $order = 'ASC';

        if($orderBy == null)
            $orderBy = 'created_at';
        
        return $query->orderBy($orderBy, $order);

    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'char_id');
    }

}
