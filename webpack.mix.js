const mix = require('laravel-mix');
require('laravel-mix-bundle-analyzer');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const compileVadmin = true;

if(compileVadmin) {
	mix
		.js('resources/site/js/site.js', 'public/js/site')
		.js('resources/vadmin/js/vadmin.js', 'public/js/vadmin')
		.disableNotifications();
	
	mix
		.sass('resources/site/sass/site.sass', 'public/css/site')
		.sass('resources/vadmin/sass/vadmin.sass', 'public/css/vadmin')
		.sass('resources/vadmin/sass/skin-dark.sass', 'public/css/vadmin')
		.disableNotifications()
		.sourceMaps()
		.options
		({
			processCssUrls: false,
			outputStyle: 'compressed'
		});
} else {
	

}


// JS ANALIZER
// |--------------------------------------------------------------------------
//Uncomment this if you want to analize app.js
// if (!mix.inProduction()) {
// 	mix.bundleAnalyzer();
// }

