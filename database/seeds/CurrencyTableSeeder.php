<?php

use Illuminate\Database\Seeder;

use App\Models\Logistic;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('currency')->delete();

        $currencies = [
                           ['Peso', 'AR', 138, 0],
                           ['Peso', 'CO', 3824, 0],
                           ['Sol', 'PE', 3.60, 0],
                           ['Peso', 'MX', 21.10, 0],
                           ['Peso', 'CL', 783.10, 0],
                           ['Peso', 'UY', 42.80, 0],
                           
                        ];       

		foreach($currencies as $currency)
		{
			try {
				$item = new \App\Models\Currency();
				$item->name = $currency[0];
                $item->country_id = $currency[1];
                $item->manual_value = $currency[2];
                $item->api_value = $currency[3];
				$item->save();

                
				$this->command->info('Moneda '.$item->name. ' agregada.');
			} catch (Exception $e) {
				dd($e->getMessage());
			}
		}
    }
}
