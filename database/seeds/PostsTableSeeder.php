<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\PostTag;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('posts')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $posts = [
            ['Post 1 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 2 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 3 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 4 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 5 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 6 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 7 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 8 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 9 Prensa', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','5'],
            ['Post 1 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 2 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 3 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 4 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 5 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 6 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 7 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 8 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?','6'],
            ['Post 9 Blog', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur, corrupti? Veritatis laudantium laboriosam voluptate. Sint, dolorum asperiores error quisquam cumque porro? Nesciunt enim explicabo porro eligendi neque, molestiae amet ea?', '6']
         ]; 
         
		foreach($posts as $post)
		{ 
            $tag = PostTag::find($post['2']);
            
            $item = new Post();
            $item->title = $post['0'];
            $item->content = $post['1'];
            $item->save();
            $item->tags()->sync($tag);
            
		}
	
    }
}
