<?php

use Illuminate\Database\Seeder;
use App\Models\QAlgorithm;

class QAlgorithmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('qalgorithm')->delete();

        $values = [
            ['1', 'Seguidor de tienda', 0.15],
            ['2', 'Suscriptor de tienda', 0.20],
            ['3', 'Visualizaciones Youtube', 0.0002],
            ['4', 'Seguidor en redes', 0.15],
            ['5', 'Suscriptor Youtube', 0.001],
            ['6', 'Seguidores influencer', 0.08],
            ['7', 'Suscriptor influencer', 0.08],
            ['8', 'Usuarios activos', 0.053],
            ['9', 'Trafico blog', 0.02],
            ['10', 'Seguidores blog', 0.08],
            ['11', 'Suscriptores blog', 0.08],
            ['100', 'Valor de suscriptor por defecto', 0.20],
            ['101', 'Valor de seguidor por defecto', 0.15],
            ['102', 'Vistas por mes', 0.15],

         ]; 

		foreach($values as $value)
		{
            $item = new QAlgorithm();
            $item->id = $value[0];
            $item->name = $value[1];
            $item->value = $value[2];
            $item->save();      
		}
	
    }
}
