<?php

use Illuminate\Database\Seeder;

use App\Models\Logistic;

class LogisticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logistics')->delete();

        Logistic::insert([
            [
                "id"   => 1,
                "name" => "Interna"
            ],
            [
                "id"   => 2,
                "name" => "Externa",
            ],
            [
                "id"   => 3,
                "name" => "Ambas",
            ]
        ]);
    }
}
