<?php

use Illuminate\Database\Seeder;

use App\Models\DigitalCapital;

class DigitalCapitalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('digitalcapitals')->delete();

        $items = [  [ 'Registro de Marca', 'Registro de Marca BR', 1000 ],
                    [ 'Dominio', 'Dominio BR', 100 ],
                    [ 'Apps','Apps BR', 1500 ],
                    [ '¿Tiene actualmente Google Analytics?', '¿Tiene actualmente Google Analytics? BR', 0 ],
                    [ '¿Tiene actualmente Google Ads/AdMob?', '¿Tiene actualmente Google Ads/AdMob? BR', 0 ]
                ];

		foreach($items as $item)
		{
			try {
				$record = new DigitalCapital();
                $record->name = $item[0];
                $record->name_br = $item[1];
                $record->value = $item[2];
				$record->save();

				$this->command->info('Capital Digital '.$record->name. ' creado.');
			} catch (Exception $e) {
				dd($e->getMessage());
			}
		}
    }
}
