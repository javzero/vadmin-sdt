<?php

use Illuminate\Database\Seeder;

use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        Role::insert([
            [
                "id"   => 1,
                "name" => "Desarrollador",
                "slug" => "developer"
            ],
            [
                "id"   => 2,
                "name" => "Admin",
                "slug" => "admin"
            ],
            [
                "id"   => 3,
                "name" => "Usuari@",
                "slug" => "user"
            ]
        ]);
    }
}
