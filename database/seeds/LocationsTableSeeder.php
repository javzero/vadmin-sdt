<?php

use Illuminate\Database\Seeder;
use App\Models\Country;
use App\Models\State;
use App\Models\City;


class LocationsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// DB::table('countries')->truncate();
        DB::table('countries')->truncate();
        DB::table('states')->truncate();
        DB::table('cities')->truncate();

        $json = File::get(public_path("data/locations.json"));
        $data = json_decode($json);
        
        foreach($data->countries as $item)
        {
            try 
            {
                $record = new Country();
                $record->id = $item->id;
                $record->char_id = $item->char_id;
                $record->name = $item->name;
                $record->save();

                $this->command->info('País '.$item->name. ' agregado.');
            } catch (Exception $e) 
            {
                dd($e->getMessage());
            }
        }

        foreach($data->states as $item)
        {
            try 
            {
                $record = new State();
                $record->id = $item->id;
                $record->country_id = $item->country_id;
                $record->name = $item->name;
                $record->save();

                $this->command->info('Estado '.$item->name. ' agregado.');
            } catch (Exception $e) 
            {
                dd($e->getMessage());
            }
        }

        foreach($data->cities as $item)
        {
            try 
            {
                $record = new City();
                $record->state_id = $item->state_id;
                $record->country_id = $item->country_id;
                $record->name = $item->name;
                $record->save();

                $this->command->info('Ciudad/Localidad '.$item->name. ' agregada.');
            } catch (Exception $e) 
            {
                dd($e->getMessage());
            }
        }        

    }
}

