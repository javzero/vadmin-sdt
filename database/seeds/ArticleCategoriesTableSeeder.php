<?php

use Illuminate\Database\Seeder;
use App\Models\ArticleCategory;

class ArticleCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('article_categories')->truncate();
        
        $categories = [ 
            'Rubro 1', 'Rubro 2', 'Rubro 3', 'Rubro 4'
        ];

        foreach ($categories as $category) {
            $item = new ArticleCategory();
            $item->name = $category;
            $item->save();
            $this->command->info('Rubro '.$item->name. ' agregado.');
        }
    }
}
