<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();

        $item = new Setting();
        $item->id = 1;
        $item->email = 'javzero1@gmail.com';
        $item->email_br = 'javzero1@gmail.com';
        $item->google_analitycs = 
            "<script async src='https://www.googletagmanager.com/gtag/js?id=UA-180452821-1'>
            </script>
            <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-180452821-1');
            </script>";
        $item->google_tag_manager = '';
        $item->site_description = '';
        $item->site_keywords = '';
        $item->save();        
    }
}
