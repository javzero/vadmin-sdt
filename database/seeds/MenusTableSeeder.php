<?php

use Illuminate\Database\Seeder;

use App\Models\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('menus')->truncate();
        DB::table('menu_role')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // Parent Menu Ids 
        $home = 1;
        $users = 2;
        $art = 10;
        $posts = 20;
        $mfContacts = 28;
        $contacts = 29;
        $artProps = 30;
        $settings = 90;
        $localiz = 110;
        $public = 150;
        
        // Menu
        $menus = [
            ['id' => $home,         'name' => 'Inicio',             'route' => '/vadmin',                           'icon' => 'house',                'parent_id' => 0,          'roles' => [1,2,3] ],
            ['id' => $users,        'name' => 'Usuarios',           'route' => '/vadmin/usuarios',                  'icon' => 'person',               'parent_id' => 0,          'roles' => [1,2]],
            
            ['id' => $art,          'name' => 'Artículos',          'route' => '',                                  'icon' => 'columns-gap',          'parent_id' => 0,          'roles' => [1,2,3]],
            ['id' => $art + 1,      'name' => 'Listado',            'route' => '/vadmin/articulos',                 'icon' => 'list',                 'parent_id' => $art,       'roles' => [1,2,3]],
            ['id' => $art + 2,      'name' => 'Crear',              'route' => '/vadmin/crear-articulo',            'icon' => 'plus',                 'parent_id' => $art,       'roles' => [1,2,3]],
            ['id' => $artProps,     'name' => 'Propiedades',        'route' => '',                                  'icon' => 'shuffle',              'parent_id' => $art,       'roles' => [1,2,3]],
            ['id' => $artProps + 1, 'name' => 'Rubros',             'route' => '/vadmin/rubros',                    'icon' => 'tag-fill',             'parent_id' => $artProps,  'roles' => [1,2,3]],
            ['id' => $artProps + 2, 'name' => 'Capital Digital',    'route' => '/vadmin/capital-digital',           'icon' => 'view-list',            'parent_id' => $artProps,  'roles' => [1,2,3]], 
            ['id' => $artProps + 3, 'name' => 'Redes Sociales',     'route' => '/vadmin/redes-sociales',            'icon' => 'view-list',            'parent_id' => $artProps,  'roles' => [1,2,3]],       
            ['id' => $art + 3,      'name' => 'Algoritmo',          'route' => '/vadmin/config-algoritmo',          'icon' => 'option',               'parent_id' => $art,       'roles' => [1,2]],       

            ['id' => $posts,         'name' => 'Blog',               'route' => '',                                 'icon' => 'newspaper',            'parent_id' => 0,          'roles' => [1,2,3]],       
            ['id' => $posts + 1,     'name' => 'Posts',              'route' => '/vadmin/posts',                    'icon' => 'list',                 'parent_id' => $posts,     'roles' => [1,2,3]],
            ['id' => $posts + 2,     'name' => 'Tags',               'route' => '/vadmin/tags',                     'icon' => 'tag',                  'parent_id' => $posts,     'roles' => [1,2,3]],
            
            ['id' => $contacts,      'name' => 'Mensajes',           'route' => '/vadmin/contacts',                 'icon' => 'envelope',             'parent_id' => 0,          'roles' => [1,2,3]],       
            ['id' => $mfContacts,    'name' => 'Registro',           'route' => '/vadmin/mfcontacts',               'icon' => 'gem',                  'parent_id' => 0,          'roles' => [1,2,3]],       

            // ['id' => $ads,           'name' => 'Anuncios',           'route' => '/vadmin/anuncios',                  'icon' => 'view-list',           'parent_id' => 0,         'roles' => [1,2,3]],  
            
            ['id' => $settings,     'name' => 'Configuración',      'route' => '',                                  'icon' => 'gear-wide',            'parent_id' => 0,          'roles' => [1,2]],
            ['id' => $settings + 1, 'name' => 'Generales',          'route' => '/vadmin/configuraciones-generales', 'icon' => 'gear',                 'parent_id' => $settings,  'roles' => [1,2]],
            ['id' => $settings + 2, 'name' => 'Menu',               'route' => '/vadmin/menus',                     'icon' => 'layout-sidebar-inset', 'parent_id' => $settings,  'roles' => [1]],
            ['id' => $settings + 3, 'name' => 'Roles',              'route' => '/vadmin/roles',                     'icon' => 'lock',                 'parent_id' => $settings,  'roles' => [1]],
            ['id' => $settings + 4, 'name' => 'Monedas',            'route' => '/vadmin/monedas',                   'icon' => 'puzzle',               'parent_id' => $settings, 'roles' => [1,2]],
            ['id' => $localiz,      'name' => 'Localización',       'route' => '',                                  'icon' => 'geo-alt',              'parent_id' => $settings,  'roles' => [1,2]],
            ['id' => $localiz + 1,  'name' => 'Países',             'route' => '/vadmin/paises',                    'icon' => 'geo',                  'parent_id' => $localiz,   'roles' => [1,2]],
            ['id' => $localiz + 2,  'name' => 'Estados/Provincias', 'route' => '/vadmin/estados',                   'icon' => 'geo',                  'parent_id' => $localiz,   'roles' => [1,2]],
            ['id' => $localiz + 3,  'name' => 'Ciudades',           'route' => '/vadmin/ciudades',                  'icon' => 'geo',                  'parent_id' => $localiz,   'roles' => [1,2]],

            ['id' => $public,       'name' => 'Ir a la Web',        'route' => '/',                                 'icon' => 'tv',                   'parent_id' => 0,         'roles' => [1,2,3]],
        ];

        echo '--------------' . "\n"; 
        echo 'Recreando menu' . "\n";
        echo '--------------' . "\n";
 
        foreach($menus as $menu)
        {
            try 
            {

                $item = new Menu();
                $item->id = $menu['id'];
                $item->name = $menu['name'];
                $item->route = $menu['route'];
                $item->icon = $menu['icon'];
                $item->parent_id = $menu['parent_id'];
                $item->save();
                
                $item->roles()->attach($menu['roles']);

                if( $item->route != "")
                    $this->command->info('Item de Menu "'.$item->name. '" con ruta "'. $item->route .'" creado.');
                else
                $this->command->info('Item padre de Menu "'.$item->name. '" creado.');

            } 
            catch (Exception $e) 
            {
                dd($e->getMessage());
            }
        }
    }
}
