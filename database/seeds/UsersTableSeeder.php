<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->truncate();
        DB::table('users')->delete();

        $adminRole = App\Models\Role::where('slug', 'admin')->first()->id;
        $developerRole = App\Models\Role::where('slug', 'developer')->first()->id;
        $userRole = App\Models\Role::where('slug', 'user')->first()->id;
   
        if(!User::where('username', 'javzero')->first())
        {
            $user = new User();
            $user->id = 1;
            $user->username = 'javzero';
            $user->first_name = 'Leandro';
            $user->last_name = 'Andrade';
            $user->email = 'javzero1@gmail.com';
            $user->password = bcrypt( '12121212' );
            // $user->image_id = $image->id;
            $user->save();

            $user->roles()->detach();
            $user->roles()->attach([$adminRole, $developerRole]);
            
            // $this->command->info('Usuario '. $item->username. ' agregado.');
        }

        if(!User::where('username', 'andre')->first())
        {
            $user = new User();
            $user->id = 2;
            $user->username = 'andre';
            $user->first_name = 'Andrea';
            $user->last_name = 'Bertone';
            $user->email = 'info@sitiodetiendas.com';
            $user->password = bcrypt( 'Sitio2020' );
            // $user->image_id = $image->id;
            $user->save();
            
            $user->roles()->detach();
            $user->roles()->attach([$adminRole, $userRole]);
            
            // $this->command->info('Usuario '. $item->username. ' agregado.');
        }
        
        $roles = App\Models\Role::all();

        // Populate the pivot table
        App\Models\User::all()->each(function ($user) use ($roles) { 
            if($user->id != 1 || $user->id != 2) 
            {
                $user->roles()->attach(
                    $roles->random(rand(1, $roles->count()))->pluck('id')->toArray()
                ); 
            }
        });
    }
}
