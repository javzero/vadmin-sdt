<?php

use Illuminate\Database\Seeder;
use App\Models\Social;

class SocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('social')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $socialNetworks = [
                           [1,  'Facebook', 'https://facebook.com/'],
                           [2,  'Instagram', 'https://instagram.com/'],
                           [4,  'Youtube', 'https://youtube.com/'],
                           [5,  'Twitter', 'https://twitter.com/'],
                           [6,  'TikTok', 'https://tiktok.com/'],
                           [7,  'Pinterest', 'https://pinterest.com/'],
                           [8,  'Linkedin', 'https://linkedin.com/']
                        ];       

		foreach($socialNetworks as $socialNetwork)
		{
			try {
				$item = new Social();
				$item->id = $socialNetwork[0];
                $item->name = $socialNetwork[1];
                $item->url = $socialNetwork[2];
				$item->save();

                
				$this->command->info('Red social '.$item->name. ' agregada.');
			} catch (Exception $e) {
				dd($e->getMessage());
			}
		}
	
    }
}
