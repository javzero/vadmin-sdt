<?php

use Illuminate\Database\Seeder;
use App\Models\PostTag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('tags')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $tags = [   
                    [ 'Tag1', 'Tag1 BR'],
                    ['Tag2', 'Tag2 BR'],
                    ['Tag3', 'Tag3 BR'],
                    ['Tag4', 'Tag4 BR']
        ];

		foreach($tags as $tag)
		{
            $item = new PostTag();
            $item->name = $tag[0];
            $item->name_br = $tag[1];
            $item->name_display = $tag[0] . ' | '. $tag[0];
            $item->save();
		}
	
    }
}
