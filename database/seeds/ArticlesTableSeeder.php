<?php

use Illuminate\Database\Seeder;
use App\Models\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $articlesAmount = 20;
        
        // $name = $this->choice('What is your name?', ['Taylor', 'Dayle'], $articlesAmmount);

        // $articlesAmmount = $this->ask("Cuántos artículos deseás crear?");
        
        // if(is_int($articlesAmmount));
        //     $articlesAmmount = $this->ask('El número ingresado no es un entero. Cuántos artículos desea crear?');

        // if(!$this->command->confirm('Deseás eliminar los artículos existentes? (yes|no)', true))
        // {
        //     DB::table('articles')->truncate();
        // }

        factory(Article::class, $articlesAmount)->create()->each(
            function ($article) {
                factory(Article::class)->make();
                //  Make images relations
                // $article->images()->save(factory(App\Models\ArticleImage::class)->make());
            
        });

        $socialNetworks = App\Models\Social::all();
        $digitalCapital = App\Models\DigitalCapital::all();
        
        // Populate the pivot table
        App\Models\Article::all()->each(function ($article) use ($socialNetworks, $digitalCapital) { 
            $rand = rand(1, 3);
            $social = $socialNetworks->random($rand)->pluck('id')->toArray();
            $digital = $digitalCapital->random($rand)->pluck('id')->toArray();

            $article->digitalCapital()->attach($digital);
            $article->social()->attach($social, 
            ['followers' => rand(100,100000), 'subscriptors' => rand(100,100000), 'account_url' => 'https://fake-social-network.com/user']); 

        });
    }
}
