<?php

use Illuminate\Database\Seeder;

class BusinessTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('business_types')->delete();

        $bts = [
                    ['1', 'Tienda Online', 'TO'],
                    ['2', 'Canal de Youtube', 'YT'],
                    ['3', 'Influencer en Redes', 'IF'],
                    ['4', 'Apps', 'APP'],
                    ['5', 'Sitio de contenido', 'SC'],
                    
                ];       

		foreach($bts as $bt)
		{
			try {
				$item = new \App\Models\BusinessType();
				$item->id = $bt[0];
                $item->name = $bt[1];
                $item->code = $bt[2];
				$item->save();

                
				$this->command->info('Tipo de negocio '.$item->name. ' agregado.');
			} catch (Exception $e) {
				dd($e->getMessage());
			}
		}
    }
}
