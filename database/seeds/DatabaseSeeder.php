<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info(">>>> Las tablas de ubicaciones contienen muchos datos y la operación puede demorar --------");

        if($this->command->confirm('Desea crear las ubicaciones? (yes|no)', true))
        {
            $this->call(LocationsTableSeeder::class);
            // $this->command->info("Proceso abortado por el usuario");
            // return;
        }
        else 
        {
            echo "No se crearán las ubicaciones \n\n";
        }

        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SocialTableSeeder::class);
        $this->call(LogisticsTableSeeder::class);
        $this->call(DigitalCapitalTableSeeder::class);
        
        // $this->call(ArticleImagesTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
        $this->call(ArticleCategoriesTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(BusinessTypeTableSeeder::class);
        $this->call(QAlgorithmTableSeeder::class);
        
    }
}
