<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Article;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(Article::class, function (Faker $faker) {
    
    $country_id =  \App\Models\Country::inRandomOrder()->first()->char_id;
    $state_id = \App\Models\State::where('country_id', $country_id)->inRandomOrder()->first()->id;
    // dd(\App\Models\City::where('state_id', $state_id)->inRandomOrder()->first()->id);
    
    // dd(\App\Models\City::where('state_id', $state_id)->first()->inRandomOrder()->first());
    
    // $city_id = \App\Models\City::where('state_id', $state_id)->inRandomOrder()->first();

    return [
        'customer_firstname' => $faker->firstName,
        'customer_lastname' => $faker->lastName,
        'customer_phone' => $faker->phoneNumber,
        'customer_email' => $faker->unique()->safeEmail,
        'country_id' => 'MX',
        'state_id' => 101,
        'city_id' => null,
        'business_type_id' => $faker->numberBetween(1,4),
        'category_id' => $faker->numberBetween(1,4),
        'name' => $faker->sentence(3),
        'title' => $faker->sentence(6),
        'description' => $faker->text($maxNbChars = 600),
        'domain_name' => $faker->domainName,
        'views_per_month' => $faker->numberBetween(1,100000),
        'organic_views_per_month' => $faker->numberBetween(1,50000),
        'total_followers' => $faker->numberBetween(1,50000),
        'total_subscriptors' => $faker->numberBetween(1,50000),
        'montly_subscriptors' => $faker->numberBetween(1,1000),
        'female_traffic' => $faker->numberBetween(0,100),
        'male_traffic' => $faker->numberBetween(0,100),
        'working_hours' => $faker->numberBetween(5, 24),
        'business_age' => $faker->numberBetween(0, 20),
        'logistic_id' => $faker->numberBetween(1,3),
        'fc_actual_year' => $faker->numberBetween(0, 1000000),
        'fc_last_year' => $faker->numberBetween(0, 1000000),
        'net_income_actual_year' => $faker->numberBetween(0, 1000000),
        'net_income_last_year' => $faker->numberBetween(0, 1000000),
        'business_value' => $faker->numberBetween(1000, 1000000),
        'status' => $faker->numberBetween(0, 2),
        'active' => 1,
        'confirmed' => 1,
    ];
});
