<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // Relations 
    // Capital digital
    // Social Networks

    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {

            $table->bigIncrements('id');
            
            $table->string('lang')->default('es');
            $table->string('customer_firstname');
            $table->string('customer_lastname');
            $table->string('customer_phone');
            $table->string('customer_email');
            $table->string('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('city_id')->nullable();

            $table->integer('category_id')->nullable();              // Rubro
            $table->integer('business_type_id');                     // Tipo de negocio

            $table->string('name')->nullable();                      // Nombre / Marca
            $table->string('title')->nullable();                     // Título
            $table->longText('description')->nullable();             // Descripción
            
            $table->string('domain_name')->nullable();               // Nombre del dominio
            $table->integer('views_per_month')->nullable();          // Cantidad de visitas por mes
            $table->integer('organic_views_per_month')->nullable();  // Cantidad de visitas orgánicas por mes
            $table->integer('total_followers')->nullable();          // Cantidad de seguidores en redes sociales
            $table->integer('total_subscriptors')->nullable();       // Cantidad de suscriptores
            $table->integer('montly_subscriptors')->nullable();      // Cantidad de suscriptores * mes
            $table->integer('male_traffic')->nullable();             // Tráfico masculino 
            $table->integer('female_traffic')->nullable();           // Tráfico femenino 
            $table->integer('digital_capital_id')->nullable();       // Tráfico femenino 

            $table->integer('working_hours')->nullable();            // Horas de trabajo por mes
            $table->integer('business_age')->nullable();            // Antigüedad en años
            
            $table->string('logistic_id')->nullable();               // Logística
                         
            $table->integer('fc_actual_year')->nullable();           // Facturacion año actual
            $table->integer('fc_last_year')->nullable();             // Facturacion año anterior
            $table->integer('net_income_actual_year')->nullable();   // Ganancia neta año actual
            $table->integer('net_income_last_year')->nullable();     // Ganancia neta último año 

            $table->integer('active_users')->nullable();             // Usuarios Activos 
            $table->integer('total_downloads')->nullable();          // Descargas totales

            $table->date('published_on')->nullable();                // Fecha de publicación del artículo en listado
            $table->date('expire_at')->nullable();                   // Fecha de expiración del artículo en listado
            
            // $table->integer('business_value')->nullable();          
            // $table->integer('business_value_local')->nullable();
            
            $table->float('business_value', 20, 2)->nullable();        // Precio de Venta en uss
            $table->float('business_value_local', 20, 2)->nullable();  // Precio de Venta en moneda local
            $table->float('business_value_manual', 20, 2)->nullable(); // Precio de Venta manual
            $table->longtext('value_raw_data')->nullable();            // Datos crudos del algoritmo

            $table->integer('status')->nullable();                   // 0: Publicado || 1: Reservado || 2: Vendido
            $table->integer('active')->default(0);                   // 1: Active || 2: Inactive
            $table->integer('confirmed')->default(1);                // Este campo puede usarse en un futuro para que el email sea validado

            $table->integer('position')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
