<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMfcontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mfcontacts', function (Blueprint $table) {
            $table->id();
            $table->string('customer_firstname')->nullable();
            $table->string('customer_lastname')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('customer_phone_area')->nullable();
            $table->string('customer_phone_number')->nullable();
            $table->integer('state_id')->nullable();
            $table->string('country_id')->nullable();
            $table->boolean('user_allows_contact')->nullable()->default(0);
            $table->boolean('contacted')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mfcontacts');
    }
}
