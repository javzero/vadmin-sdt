<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->nullable();
            $table->string('email_br')->nullable();
            $table->longText('google_analitycs')->nullable();
            $table->longText('google_tag_manager')->nullable();
            $table->string('site_name')->nullable();
            $table->longText('site_description')->nullable();
            $table->longText('site_keywords')->nullable();
            $table->string('site_name_br')->nullable();
            $table->longText('site_description_br')->nullable();
            $table->longText('site_keywords_br')->nullable();

            $table->longText('site_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
