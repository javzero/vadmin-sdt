<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_social', function (Blueprint $table) {
            $table->bigInteger('article_id')->unsigned();
            $table->bigInteger('social_id')->unsigned();
            $table->string('account_url')->nullable();
            $table->integer('followers')->nullable();
            $table->integer('subscriptors')->nullable();
        });

        Schema::table('article_social', function (Blueprint $table) {
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->foreign('social_id')->references('id')->on('social')->onDelete('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_social');
    }
}
