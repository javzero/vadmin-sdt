<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostPostTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_post_tag', function (Blueprint $table) {
            $table->bigIncrements('id');
        });
        
        Schema::table('post_post_tag', function(Blueprint $table) {
            $table->unsignedBigInteger('post_id')->unsigned();
            $table->unsignedBigInteger('post_tag_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade');
            $table->foreign('post_tag_id')->references('id')->on('tags')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_post_tag');
    }
}
