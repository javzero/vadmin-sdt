<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('author_id')->nullable();
            $table->string('slug')->nullable();
            $table->string('title')->nullable();
            $table->string('lang')->default('es')->nullable();
            $table->longText('content')->nullable();
            $table->string('featured_image')->nullable();
            $table->date('publish_at')->nullable();
            $table->date('publish_end')->nullable();
            $table->integer('order')->default(0);
            $table->string('status')->default('-');
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
