<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleDigitalcapitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_digital_capital', function (Blueprint $table) {
            $table->bigInteger('article_id')->unsigned();
            $table->bigInteger('digital_capital_id')->unsigned();
        });

        Schema::table('article_digital_capital', function (Blueprint $table) {
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->foreign('digital_capital_id')->references('id')->on('digitalcapitals')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_digitalcapital');
    }
}
